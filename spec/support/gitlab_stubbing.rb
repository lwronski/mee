# frozen_string_literal: true

require "rspec/mocks/standalone"
require "webmock/rspec"

RSpec.configure do |config|
  config.before(:suite) do
    repo_url = "https://gitlab.com/api/v4/projects/cyfronet%2F.*/repository"

    branches = WebMock::RequestPattern.new(:get, Regexp.new("#{repo_url}/branches?.*"))
    tags = WebMock::RequestPattern.new(:get, Regexp.new("#{repo_url}/tags?.*"))
    WebMock.globally_stub_request(:after_local_stubs) do |req|
      if branches.matches?(req)
        { status: 200, body: [{ name: "master", default: true }].to_json }
      elsif tags.matches?(req)
        { status: 200, body: [{ name: "v1" }].to_json }
      end
    end

    allow_any_instance_of(GitRepository::GitlabClient).to receive(:key_valid?)
  end
end
