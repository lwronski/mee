# frozen_string_literal: true

require "support/seed_db"
require "support/default_variables"

# Make sure that gitlab is mocked before db is seeded
require "support/gitlab_stubbing"


def hostname(subdomain = nil)
  # lvh.me always resolves to 127.0.0.1
  subdomain ? "#{subdomain}.mee.lvh.me" : "mee.lvh.me"
end

def in_organization(organization)
  original_host = Capybara.app_host
  Capybara.app_host = "http://#{hostname(organization.slug)}"
  yield
  Capybara.app_host = original_host
end

RSpec.configure do |config|
  config.include DefaultVariables

  config.before(:suite) {
    SeedDb.instance.seed_db
  }
  config.before(:each) {
    SeedDb.instance.clear_cache!
  }
  config.after(:suite) { SeedDb.instance.clean_db }

  config.before(:each, type: :system) do
    Capybara.app_host = "http://#{hostname(organization.slug)}"
  end

  config.before(:each, :root, type: :system) do
    Capybara.app_host = "http://#{hostname}"
    Capybara.default_host = "http://#{hostname}"
  end

  config.before(:each, type: :request) do
    host! hostname(organization.slug)
  end

  config.before(:all, :root, type: :request) do
    host! hostname
  end
end

Capybara.configure do |config|
  config.always_include_port = true
end
