# frozen_string_literal: true

module FactoryHelpers
  def default_organization
    @default_organization ||= Organization.find_by(name: "Default Org")
  end

  def default_user
    @default_user ||= User.find_by(first_name: "User")
  end

  def default_flow
    @default_flow ||= Flow.first
  end

  def default_grant_type
    @default_grant_type ||= GrantType.first
  end

  def rimrock_step
    @rimrock_step ||= RimrockStep.first
  end

  def rimrock_step2
    @rimrock_step2 ||= RimrockStep.second
  end

  def data_file_type
    @data_file_type ||= DataFileType.first
  end

  def default_patient
    @default_patient ||= Patient.first
  end

  def default_site
    @default_stie ||= Site.first
  end

  def valid_cert
    @valid_cert ||= begin
      key = OpenSSL::PKey::RSA.new(1024)
      public_key = key.public_key

      subject = "/C=BE/O=Test/OU=Test/CN=Test"

      cert = OpenSSL::X509::Certificate.new
      cert.subject = cert.issuer = OpenSSL::X509::Name.parse(subject)
      cert.not_before = Time.now
      cert.not_after = Time.now + 365 * 24 * 60 * 60
      cert.public_key = public_key
      cert.serial = 0x0
      cert.version = 2

      cert.sign key, OpenSSL::Digest::SHA1.new

      cert.to_pem
    end
  end
end

FactoryBot::SyntaxRunner.send(:include, FactoryHelpers)
