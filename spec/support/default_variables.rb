# frozen_string_literal: true

module DefaultVariables
  delegate :organization, :other_organization,
           :user, :user_membership,
           :supervisor, :supervisor_membership,
           :admin, :admin_membership,
           :stranger,
           :flow, :rimrock_step, :rimrock_step2, :data_file_type,
           :cpu_grant_type, :gpu_grant_type, :grant, :site,
        to: :defaults_from_db

  def defaults_from_db
    SeedDb.instance
  end
end
