# frozen_string_literal: true

require_relative "oauth_helper"

module AuthenticationHelper
  include OauthHelper

  def sign_in_as(user, teams: "#{organization.plgrid_team_id}(#{organization.name})")
    stub_plgrid_oauth(user, teams)

    visit "/auth/open_id"
  end

  def login_as(user, teams: "#{organization.plgrid_team_id}(#{organization.name})")
    stub_plgrid_oauth(user, teams)

    get "/auth/open_id"
    follow_redirect!
  end
end
