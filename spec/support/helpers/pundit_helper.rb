# frozen_string_literal: true

module PunditHelper
  def pundit_user
    UserContext.new(user, organization, user_membership)
  end

  def supervisor_pundit_user
    UserContext.new(supervisor, organization, supervisor_membership)
  end

  def admin_pundit_user
    UserContext.new(admin, organization, admin_membership)
  end
end
