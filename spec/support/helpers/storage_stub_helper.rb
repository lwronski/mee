# frozen_string_literal: true

module StorageStubHelper
  def stub_storage
    instance_double(Storage::Plgrid).tap do |storage|
      allow_any_instance_of(Organization)
        .to receive(:storage_for)
        .and_return(storage)
    end
  end

  def stub_storage_with_defaults
    stub_storage.tap do |storage|
      allow(storage).to receive(:valid?).and_return(true)
      allow(storage).to receive(:require_proxy?).and_return(false)
      allow(storage).to receive(:remove).and_return(true)
      allow(storage).to receive(:mkdir).and_return(true)
      allow(storage).to receive(:list).and_return({})
      allow(storage).to receive(:copy).and_return(true)
      allow(storage).to receive(:move).and_return(true)
    end
  end

  def stub_storage_failure
    stub_storage.tap do |storage|
      allow(storage).to receive(:valid?).and_return(true)
      allow(storage).to receive(:require_proxy?).and_return(false)
      allow(storage).to receive(:remove).and_return(false)
      allow(storage).to receive(:mkdir).and_return(false)
      allow(storage).to receive(:list).and_return({})
      allow(storage).to receive(:copy).and_return(false)
      allow(storage).to receive(:move).and_return(false)
    end
  end
end
