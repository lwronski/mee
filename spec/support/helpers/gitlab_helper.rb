# frozen_string_literal: true


module GitlabHelper
  def stub_repo_versions(step, *responses)
    repo_url = repository_url(step.repository)

    stub_request(:get, Regexp.new("#{repo_url}/branches?.*"))
      .to_return(to_responses(responses, key: :branches))
    stub_request(:get, Regexp.new("#{repo_url}/tags?.*"))
      .to_return(to_responses(responses, key: :tags))
  end

  def stub_get_repo_file(step, version, content)
    file_url = "#{repository_url(step.repository)}/files/#{step.file}?ref=#{version}"

    stub_request(:get, file_url)
      .to_return(status: 200,
                 body: { content:, commit_id: "rev" }.to_json)
  end

  def stub_gitlab_branches(repository:, status:, body:)
    repo_url = repository_url(repository)
    stub_request(:get, "#{repo_url}/branches")
      .to_return(status:, body: body.to_json)
  end

  def stub_gitlab_tags(repository:, status:, body:)
    repo_url = repository_url(repository)
    stub_request(:get, "#{repo_url}/tags")
      .to_return(status:, body: body.to_json)
  end

  private
    def repository_url(repository)
      repo_path = CGI.escape(repository)

      "https://gitlab.com/api/v4/projects/#{repo_path}/repository"
    end

    def to_responses(array, key:, default_value: nil)
      array.map do |record|
        {
          status: 200,
          body: to_name_json(record[key], default_value:)
        }
      end
    end

    def to_name_json(array, default_value: nil)
      (array || []).map { |v| { name: v, default: default_value == v } }.to_json
    end
end
