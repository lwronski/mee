# frozen_string_literal: true

module RimrockHelper
  def stub_rimrock_process(with_error: false)
    if with_error
      stub_request(:post, "https://rimrock.plgrid.pl/api/process")
        .to_return(status: 400, body: "")
    else
      stub_request(:post, "https://rimrock.plgrid.pl/api/process")
        .to_return(status: 200, body: "{\"status\": \"OK\", \"exit_code\": 0}")
    end
  end

  def stub_job_abort(computation, status: 200)
    stub_request(:put, "#{rimrock_url}/api/jobs/#{computation.job_id}")
      .with(body: { action: "abort" }.to_json)
      .to_return(status:)
  end

  private
    def rimrock_url
      Rails.application.config.constants.dig(:rimrock, :url)
    end
end
