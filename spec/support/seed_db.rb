# frozen_string_literal: true

class SeedDb
  include Singleton

  def initialize
    clear_cache!
  end

  [:organization, :other_organization, :user, :user_membership, :supervisor, :supervisor_membership,
   :admin, :admin_membership, :stranger, :flow, :rimrock_step, :rimrock_step2,
   :data_file_type, :cpu_grant_type, :gpu_grant_type, :grant, :site].each do |name|
    define_method(name) do
      @cache[name] ||= instance_variable_get("@#{name}").reload
    end
  end

  def clear_cache!
    @cache = {}
  end


  def seed_db
    git_config = GitConfig::Gitlab.new(host: ENV["GITLAB_HOST"] || "gitlab.com",
                                       private_token: ENV["GITLAB_API_PRIVATE_TOKEN"],
                                       download_key: SSHKey.generate.private_key)

    create_site
    storage_config = Storage::Config::Plgrid.new(host: "prometheus.cyfronet.pl",
                                                 host_key: "prometheus",
                                                 path: "org_directory")

    create_default_users

    Current.set(user: @admin) do
      @organization = Organization.create!(
        name: "Default Org",
        git_config:,
        storage_config:,
        plgrid_team_id: "plggdefaultorg",
      )

      @other_organization = Organization.create!(
        name: "Other Org",
        git_config:,
        storage_config:,
        plgrid_team_id: "plggotherorg",
      )
    end

    create_default_memberships
    create_grants
    create_flow
  end

  def clean_db
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean
  end

  private
    def create_default_users
      @user = FactoryBot.create(:user, first_name: "User", plgrid_login: "plguser")
      @supervisor = FactoryBot.create(:user, first_name: "Supervisor", plgrid_login: "plgsupervisor")
      @admin = FactoryBot.create(:user, first_name: "Admin", roles: [:admin], plgrid_login: "plgadmin")
      @stranger = FactoryBot.create(:user, first_name: "Stranger", plgrid_login: "plgstranger")
    end

    def create_default_memberships
      @user_membership = FactoryBot.create(:approved_membership,
                                user: @user, organization: @organization)
      @supervisor_membership = FactoryBot.create(:supervisor_membership,
                                      user: @supervisor, organization: @organization)
      @admin_membership = FactoryBot.create(:admin_membership,
                                 user: @admin, organization: @organization)
    end

    def create_site
      @site = FactoryBot.create(:site, id: 4)
    end

    def create_flow
      @data_file_type = FactoryBot.create(:data_file_type,
                               organization: @organization, data_type: "image",
                               pattern: /(^imaging_.*\.zip$)|(file\.zip)/)

      @rimrock_step = FactoryBot.create(:rimrock_step,
                             organization: @organization,
                             grant_type: cpu_grant_type)

      @rimrock_step2 = FactoryBot.create(:rimrock_step,
                             organization: @organization,
                             grant_type: cpu_grant_type,
                             required_file_types: [@data_file_type])

      @flow = FactoryBot.create(:flow, organization: @organization, flow_steps_attributes: [
        { step: @rimrock_step, position: 1 },
        { step: @rimrock_step2, position: 2 },
      ])
    end

    def create_grants
      @cpu_grant_type = FactoryBot.create(:grant_type, name: "cpu")
      @gpu_grant_type = FactoryBot.create(:grant_type, name: "gpu")

      @grant = FactoryBot.create(:grant, :active,
                      organization:,
                      grant_types: [@cpu_grant_type])
    end
end
