# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  config.before(:each, type: :system, js: true) do
    driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400] do |opts|
      opts.add_argument("--no-sandbox")
      opts.add_argument("--headless")
      opts.add_argument("--disable-gpu")
    end
  end
end
