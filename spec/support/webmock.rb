# frozen_string_literal: true

require "webmock/rspec"

WebMock.disable_net_connect!(allow_localhost: true,
                             allow: "chromedriver.storage.googleapis.com")

RSpec.configure do |config|
  config.before(:each, :gitlab) do
    WebMock.disable_net_connect!(allow_localhost: true,
                                 allow: ["gitlab.com", "url.invalid",
                                         "chromedriver.storage.googleapis.com"])
  end

  config.after(:each, :gitlab) do
    WebMock.disable_net_connect!(allow_localhost: true,
                                 allow: "chromedriver.storage.googleapis.com")
  end

  config.before(:each, :files) do
    WebMock.disable_net_connect!(allow_localhost: true,
                                 # https://github.com/bblimke/webmock/issues/883
                                 net_http_connect_on_start: true,
                                 allow: ["files.valve-dev.cyfronet.pl",
                                         "chromedriver.storage.googleapis.com"])
  end

  config.after(:each, :files) do
    WebMock.disable_net_connect!(allow_localhost: true,
                                 allow: "chromedriver.storage.googleapis.com")
  end
end
