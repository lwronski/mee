# frozen_string_literal: true

require "rails_helper"

RSpec.describe "/computations/show", type: :view do
  let(:pipeline) { create(:pipeline) }
  let(:computation) { create(:rimrock_computation, pipeline:) }

  context "flow is discarded" do
    before do
      def view.policy(x)
      end
    end

    it "shows warning message to the user" do
      allow(view).to receive(:policy)
        .and_return(double(update?: false, abort?: false))

      pipeline.flow.discard!

      render partial: "computations/show",
        locals: { patient: pipeline.runnable, pipeline:,
                  computation:, computations: [computation] }

      expect(rendered)
        .to have_selector("div[class='alert alert-warning']",
                          text: I18n.t("steps.readonly", step: computation.name.downcase))
    end
  end

  context "detailsComponent" do
    before do
      def view.policy(x)
      end
      allow(view).to receive(:policy)
                       .and_return(double(update?: false, abort?: false))

      render partial: "computations/show",
             locals: { patient: pipeline.runnable, pipeline:,
                       computation:, computations: [computation] }
    end

    context "static text" do
      it { expect(rendered).to have_text(I18n.t("computation.started_at")) }
      it { expect(rendered).to have_text(I18n.t("computation.site")) }
      it { expect(rendered).to have_text(I18n.t("computation.revision")) }
      it { expect(rendered).to have_text(I18n.t("computation.execution_time")) }
      it { expect(rendered).to have_text(I18n.t("computation.outputs")) }
      it { expect(rendered).to have_text(I18n.t("computation.status")) }
    end

    context "computation data" do
      it "shows start time" do
        expect(rendered).to have_text(l(computation.started_at, format: :short))
      end

      it "shows site" do
        expect(rendered).to have_text(computation.site.name.capitalize)
      end

      it "shows revision" do
        expect(rendered).to have_text(computation.revision)
      end

      it "shows status" do
        expect(rendered).to have_text(I18n.t("computation.details_component.runnable"))
      end
    end
  end
end
