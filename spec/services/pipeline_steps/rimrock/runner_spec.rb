# frozen_string_literal: true

require "rails_helper"
require "services/pipeline_steps/runner_shared_examples"

RSpec.describe PipelineSteps::Rimrock::Runner do
  include GitlabHelper

  let(:updater) { instance_double(ComputationUpdater, call: true) }
  let(:step) { create(:rimrock_step, repository: "cyfronet/mee", file: "file") }
  let(:user) { create(:user, :with_outdated_proxy) }
  let(:computation) { create(:rimrock_computation, step:, user:,
                             parameter_values_attributes: {
                              "tag-or-branch" => { version: "master" },
                              "grant" => { value: grant.name },
                             }) }

  subject do
    described_class.new(computation, updater: double(new: updater))
  end

  before do
    stub_repo_versions(step, { branches: ["master"] })
    allow(step).to receive(:template_and_revision_for).and_return("script payload", "revision")
  end

  context "inputs are available" do
    it_behaves_like "runnable step"

    it "starts a Rimrock job" do
      expect(Rimrock::StartJob).to receive(:perform_later)

      subject.call
    end

    it "creates computation with script returned by generator" do
      computation.assign_attributes(revision: "revision")

      subject.call

      expect(computation.script).to include "script payload"
    end

    it "transfers script generation errors to computation errors" do
      allow(step).to receive(:template_and_revision_for)
        .and_return("{% license_for ansys %}", "revision")

      subject.call

      expect(computation).to_not be_valid
      expect(computation.errors[:script]).to include("cannot find requested ansys license")
    end

    it "set job_id to null while restarting computation" do
      computation.update(job_id: "some_id", revision: "master")

      subject.call

      expect(computation.job_id).to be_nil
    end
  end
end
