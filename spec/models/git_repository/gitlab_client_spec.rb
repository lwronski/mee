# frozen_string_literal: true

require "rails_helper"

RSpec.describe GitRepository::GitlabClient, gitlab: true do
  GitlabResult = Struct.new(:name, :default)

  subject do
    described_class.new(host: ENV["GITLAB_HOST"] || "gitlab.com",
                        path: "eurvalve/integration-testing",
                        download_key: ENV["GITLAB_DOWNLOAD_KEY"],
                        private_token: ENV["GITLAB_API_PRIVATE_TOKEN"])
  end

  context "#versions" do
    before(:each) { Rails.cache.clear }

    it "downloads branches and tags from gitlab" do
      versions = subject.versions

      expect(versions[:branches]).to contain_exactly "some_branch", "master"
      expect(versions[:tags]).to eq ["some_tag"]
      expect(versions[:default_branch]).to eq "master"
    end

    it "returns empty list for nonexistent repository" do
      gitlab = described_class.new(host: "url.invalid",
                                   path: "eurvalve/integration-testing",
                                   key: "download key", private_token: "secret")

      versions = gitlab.versions
      expect(versions[:branches]).to eq []
      expect(versions[:tags]).to eq []
      expect(versions[:default_branch]).to be_nil
    end

    it "returns empty list on token validation failure" do
      gitlab = described_class.new(host: ENV["GITLAB_HOST"] || "gitlab.com",
                                   path: "eurvalve/integration-testing",
                                   key: ENV["GITLAB_DOWNLOAD_KEY"],
                                   private_token: "foo")

      versions = gitlab.versions
      expect(versions[:branches]).to eq []
      expect(versions[:tags]).to eq []
      expect(versions[:default_branch]).to be_nil
    end

    context "cache", gitlab: false do
      include ActiveSupport::Testing::TimeHelpers
      let(:memory_store) do
        ActiveSupport::Cache.lookup_store(:memory_store,
                                          expires_in: 10.minutes)
      end
      let(:cache) { Rails.cache }

      before do
        allow(Rails).to receive(:cache).and_return(memory_store)
        Rails.cache.clear
      end

      let(:gitlab_client) { double("gitlab client") }
      before { allow(Gitlab).to receive(:client).and_return(gitlab_client) }

      it "returns cached tags and versions" do
        expect(gitlab_client).
          to receive(:branches).and_return([GitlabResult.new("b1")]).once
        expect(gitlab_client).
          to receive(:tags).and_return([GitlabResult.new("t1")]).once

        subject.versions
        subject.versions
      end

      it "asks gitlab about versions after cache timeout" do
        expect(gitlab_client).
          to receive(:branches).and_return([GitlabResult.new("b1")]).twice
        expect(gitlab_client).
          to receive(:tags).and_return([GitlabResult.new("t1")]).twice

        subject.versions

        travel(11.minutes) do
          subject.versions
        end
      end

      it "forces tags and branches update" do
        expect(gitlab_client).
          to receive(:branches).and_return([GitlabResult.new("b1")]).twice
        expect(gitlab_client).
          to receive(:tags).and_return([GitlabResult.new("t1")]).twice

        subject.versions(force_reload: true)
        subject.versions(force_reload: true)
      end
    end
  end

  context "#get_file_and_revision" do
    it "downloads file content from gitlab" do
      content, = subject.content_and_revision_for("README.md", "some_branch")

      expect(content).to include "Prometheus"
    end
  end
end
