# frozen_string_literal: true

FactoryBot.define do
  factory :organization do
    sequence(:name) { |n| "Organization #{n}" }
    sequence(:git_config) { build(:gitlab_git_config) }
    sequence(:storage_config) do |n|
      Storage::Config::Plgrid.new(host: "prometheus.cyfronet.pl",
                                  host_key: "prometheus",
                                  path: "org_path_#{n}")
    end

    sequence(:plgrid_team_id) { |n| "plggteam#{n}" }
  end
end
