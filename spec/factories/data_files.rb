# frozen_string_literal: true

FactoryBot.define do
  factory :data_file do
    sequence(:name) { |n| "data_file_#{n}" }
    data_type { data_file_type }
    for_patient

    trait :for_patient do
      association :fileable, factory: :patient
    end

    trait :for_organization do
      association :fileable, factory: :organization
    end
  end
end
