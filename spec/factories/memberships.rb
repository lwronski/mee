# frozen_string_literal: true

FactoryBot.define do
  factory :membership do
    trait :approved do
      state { :approved }
    end

    trait :blocked do
      state { :blocked }
    end

    trait :admin do
      roles { [:admin] }
    end

    trait :supervisor do
      roles { [:supervisor] }
    end

    factory :approved_membership, traits: [:approved]
    factory :blocked_membership, traits: [:blocked]
    factory :admin_membership, traits: [:approved, :admin]
    factory :supervisor_membership, traits: [:approved, :supervisor]
  end
end
