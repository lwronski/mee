# frozen_string_literal: true

FactoryBot.define do
  factory :parameter_value do
    initialize_with { new(**attributes) }
    computation { build(:rimrock_computation) }

    factory :grant_parameter_value, class: "ParameterValue::Grant" do
      parameter { build(:grant_parameter) }
    end

    factory :model_version_parameter_value, class: "ParameterValue::ModelVersion" do
      parameter { build(:model_version_parameter) }
    end

    factory :select_parameter_value, class: "ParameterValue::Select" do
      parameter { build(:select_parameter) }
      value { parameter.values.first.value }
    end

    factory :string_parameter_value, class: "ParameterValue::String" do
      parameter { build(:string_parameter) }
    end

    factory :number_parameter_value, class: "ParameterValue::Number" do
      parameter { build(:number_parameter) }
    end

    factory :required_data_file_parameter_value, class: "ParameterValue::RequiredDataFile" do
      parameter { build(:required_data_file_parameter) }
    end
  end
end
