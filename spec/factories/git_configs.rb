# frozen_string_literal: true

FactoryBot.define do
  factory :git_config do
    host { "gitlab.com" }
    download_key { SSHKey.generate.private_key }

    factory :gitlab_git_config, class: "GitConfig::Gitlab" do
      sequence(:private_token) { |n| "token#{n}" }
    end
    factory :native_git_config, class: "GitConfig::Native"
  end
end
