# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "johndoe#{n}@email.pl" }
    sequence(:first_name) { |n| "John#{n}" }
    sequence(:last_name) { |n| "Doe#{n}" }
    terms { true }
    sequence(:plgrid_login) { |n| "plgjohndoe#{n}" }
    proxy { build(:proxy) }

    trait :with_outdated_proxy do
      proxy { build(:outdated_proxy) }
    end
  end
end
