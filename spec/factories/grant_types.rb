# frozen_string_literal: true

FactoryBot.define do
  factory :grant_type do
    sequence(:name) { |n| "grant_type_#{n}" }
  end
end
