# frozen_string_literal: true

FactoryBot.define do
  factory :artifact do
    sequence(:name) { |n| "Artifact #{n}" }
    organization { default_organization }
    file_id { create(:data_file).id }
  end
end
