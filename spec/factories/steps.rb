# frozen_string_literal: true

FactoryBot.define do
  factory :step do
    factory :rimrock_step, class: "RimrockStep" do
      sequence(:name) { |n| "rimrock step #{n}" }
      sequence(:repository) { |n| "cyfronet/step-repo#{n}" }
      sequence(:file) { |n| "script#{n}.sh.liquid" }
      grant_type { default_grant_type }
      organization { default_organization }
      user { default_user }
      site { default_site }
    end
  end
end
