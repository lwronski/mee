# frozen_string_literal: true

FactoryBot.define do
  factory :grant do
    sequence(:name) { |n| "grant_#{n}" }
    organization { default_organization }
    grant_types { [default_grant_type] }

    trait :active do
      start_at { 1.day.ago }
      end_at { 10.days.from_now }
    end

    trait :expired do
      start_at { 10.days.ago }
      end_at { 1.day.ago }
    end

    trait :future do
      start_at { 10.days.from_now }
      end_at { 15.days.from_now }
    end
  end
end
