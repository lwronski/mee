# frozen_string_literal: true

FactoryBot.define do
  factory :pipeline do
    sequence(:name) { |n| "pipeline_#{n}" }
    flow { default_flow }
    user { default_user }
    for_patient

    trait :with_computations do
      initialize_with do
        pipeline = Pipelines::Build.new(user, runnable, attributes, {}).call
        pipeline.computations.each do |computation|
          computation.parameter_values = []
        end

        pipeline
      end
    end

    trait :with_notes do
      notes { "some random notes" }
    end

    trait :for_patient do
      association :runnable, factory: :patient
    end

    trait :for_organization do
      association :runnable, factory: :organization
    end
  end
end
