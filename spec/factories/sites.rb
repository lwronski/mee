# frozen_string_literal: true

FactoryBot.define do
  factory :site do
    name { |n| "prometheus#{n}" }
    host { "prometheus.cyfronet.pl" }
    host_key { "prometheus" }
  end
end
