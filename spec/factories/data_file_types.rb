# frozen_string_literal: true

FactoryBot.define do
  factory :data_file_type do
    sequence(:name) { |n| "Dft#{n}" }
    sequence(:data_type) { |n| "dft#{n}" }
    sequence(:pattern) { |n| /^dft.#{n}$/ }
    organization { default_organization }
  end
end
