# frozen_string_literal: true

FactoryBot.define do
  factory :computation do
    sequence(:working_directory) { |n| "working_dir_#{n}" }
    started_at { Time.current }
    step { rimrock_step }

    user { default_user }
    pipeline

    factory :rimrock_computation, class: "RimrockComputation" do
      step { rimrock_step }
      script { "SCRIPT" }
      tag_or_branch { "master" }
      revision { "1234" }
    end
  end
end
