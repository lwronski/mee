# frozen_string_literal: true

FactoryBot.define do
  factory :flow do
    sequence(:name) { |n| "Flow #{n}" }
    sequence(:position) { |n| n }
    organization { default_organization }
    user { default_user }
    flow_steps_attributes { [step: rimrock_step, position: 1] }
  end
end
