# frozen_string_literal: true

FactoryBot.define do
  factory :parameter do
    sequence(:key) { |n| "parameter-#{n}" }
    sequence(:name) { |n| "Parameter name #{n}" }
    sequence(:hint) { |n| "Parameter hint #{n}" }

    factory :grant_parameter, class: "Parameter::Grant" do
      step { rimrock_step }
    end

    factory :model_version_parameter, class: "Parameter::ModelVersion" do
      step { rimrock_step }
    end

    factory :select_parameter, class: "Parameter::Select" do
      step { rimrock_step }
      values do
        [ Parameter::Select::Value.new(name: "A", value: "a"),
          Parameter::Select::Value.new(name: "B", value: "b")]
      end
    end

    factory :string_parameter, class: "Parameter::String" do
      step { rimrock_step }
    end

    factory :number_parameter, class: "Parameter::Number" do
      step { rimrock_step }
    end
  end

  factory :required_data_file_parameter, class: "Parameter::RequiredDataFile" do
    initialize_with { new(data_file_type:, data_files: nil) }
  end
end
