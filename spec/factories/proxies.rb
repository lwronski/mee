# frozen_string_literal: true

FactoryBot.define do
  factory :proxy, class: Proxy do
    initialize_with { new(valid_cert) }
  end

  factory :outdated_proxy, class: Proxy do
    initialize_with { new(File.read(Rails.root.join("spec", "support", "proxy", "outdated"))) }
  end
end
