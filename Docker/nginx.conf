upstream mee {
  server web:3000;
}

server {
  listen 80 default_server;
  server_name mee.cyfronet.pl;

  root /public;

  access_log /dev/stdout;
  error_log /dev/stdout info;

  location ~* ^.+\.(rb|log)$ {
    deny all;
  }

  location / {
    try_files $uri $uri/index.html $uri.html @mee;
  }

  location /cable {
    proxy_pass http://mee;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
  }

  location ~* ^/assets/ {
    try_files $uri @mee;
    access_log off;
    gzip_static on;

    expires 1y;
    add_header Cache-Control public;

    add_header Last-Modified "";
    add_header ETag "";
    break;
  }

  location @mee {
    proxy_redirect off;

    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $remote_addr;

    proxy_pass http://mee;
  }
}
