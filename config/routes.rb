# frozen_string_literal: true

require "routes_constraint/subdomain"
require "routes_constraint/root"
require "routes_constraint/admin"

# rubocop:disable Metrics/BlockLength
Rails.application.routes.draw do
  delete "logout", to: "sessions#destroy", as: "logout"
  match "auth/:provider/callback", to: "sessions#create", via: [:get, :post]
  get "auth/failure", to: redirect("/")

  constraints(RoutesConstraint::Subdomain) do
    root to: "welcome#index"

    resource :membership, only: [:show, :create]

    ## User profile section routes
    resource :profile, only: :show

    resources :patients, except: [:edit, :update], constraints: { id: /.+/ } do
      resources :pipelines, only: [:index, :new, :create]
      scope module: :patients do
        resource :details, only: :show
      end
    end

    resources :comparisons, only: [:index]

    resources :pipelines do
      scope module: :pipelines do
        resource :files, only: :show
      end
    end

    resources :computations, only: [:show, :update] do
      scope module: :computations do
        resource :abort, only: [:create]
        resource :stdout, only: :show
        resource :stderr, only: :show
      end
    end

    get "steps/*step_id/versions", to: "steps/versions#index", as: :step_versions

    resources :artifacts, except: [:new, :create]
    get "files/:file_id/artifacts/new", to: "artifacts#new", as: :new_artifact
    post "files/:file_id/artifacts", to: "artifacts#create", as: :create_artifact

    # Help
    get "help" => "help#index"
    get "help/:category/:file" => "help#show",
        as: :help_page,
        constraints: { category: /.*/, file: %r{[^/.]+} }

    resources :parameters, only: :show

    resources :flows, except: :show do
      scope module: :flows do
        resource :pipeline_parameters, only: :show
      end
    end
    resources :steps, except: :show do
      scope module: :steps do
        resources :git_configs, only: [:index, :show]
      end
    end

    resources :cohorts do
      scope module: :cohorts do
        resources :patients, only: [:destroy, :create]
      end
    end

    resources :campaigns, except: [:edit, :update] do
      scope module: :campaigns do
        resource :run, only: :create
      end
    end

    namespace :admin do
      resources :users
      resource :organization, only: [:show, :update, :destroy] do
        scope module: :organizations do
          resources :git_configs, only: :show
          resources :storage_configs, only: :show
        end
      end
      resources :grants, except: :show
      resources :licenses, except: :show
      resources :data_file_types, except: :show
    end

    get "files/*id",
        to: "files#show",
        constraints: { id: /.*/ },
        as: :file
    post "files/*id",
         to: "files#create",
         constraints: { id: /.*/ },
         as: :file_upload
    delete "files/*id",
           to: "files#destroy",
           constraints: { id: /.*/ }
  end

  constraints(RoutesConstraint::Root) do
    root to: "home#index", as: :home_root
    resources :home, only: [:index]
    resources :organizations, only: [:new, :create]
    namespace :organizations do
      resources :git_configs, only: :show
      resources :storage_configs, only: :show
    end

    get "organization_not_found", to: "home#not_found", as: :organization_not_found
    get "organization_not_authorized", to: "home#not_authorized", as: :organization_not_authorized

    # TODO
    # # Sidekiq monitoring
    constraints(RoutesConstraint::Admin) do
      require "sidekiq/web"
      mount Sidekiq::Web => "/sidekiq"
      namespace :admin do
        resources :jobs, only: :index
      end
    end
  end

  match "/404", to: "errors#not_found", via: :all
  match "/422", to: "errors#unprocessable", via: :all
  match "/500", to: "errors#internal_server_error", via: :all
end
# rubocop:enable Metrics/BlockLength
