# frozen_string_literal: true

# The same site policy is well described here:
# https://www.netsparker.com/blog/web-security/same-site-cookie-attribute-prevent-cross-site-request-forgery/
#
# By default rails starting from 6.1 is setting `lax` for SameSite cookies
# policy. This causes a problem with connecting the existing account into
# PLGrid account because we are using POST response to pass proxy payload
# (`lax` policy allows to send cookies only for GET requests). As a result,
# a new user account was created instead of connecting the existing one with
# PLGrid account.
Rails.application.config.action_dispatch.cookies_same_site_protection = ->(request) do
  request.path.start_with?("/users/auth/open_id") ? nil : :lax
end
