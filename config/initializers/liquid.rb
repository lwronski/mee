# frozen_string_literal: true

require "liquid/stage_in"
require "liquid/stage_out"
require "liquid/clone_repo"
require "liquid/license_for"
require "liquid/value_of"
require "liquid/stage_in_artifact"

Liquid::Template.register_tag("stage_in", Liquid::StageIn)
Liquid::Template.register_tag("stage_out", Liquid::StageOut)
Liquid::Template.register_tag("clone_repo", Liquid::CloneRepo)
Liquid::Template.register_tag("license_for", Liquid::LicenseFor)
Liquid::Template.register_tag("value_of", Liquid::ValueOf)
Liquid::Template.register_tag("stage_in_artifact", Liquid::StageInArtifact)
