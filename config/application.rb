# frozen_string_literal: true

require_relative "boot"

require "rails"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_view/railtie"
require "action_mailer/railtie"
require "active_job/railtie"
require "action_cable/engine"
# require "action_mailbox/engine"
require "action_text/engine"
require "rails/test_unit/railtie"

require File.expand_path("lib/jwt/config")

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mee
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    config.active_storage.queues.analysis = :active_storage_analysis
    config.active_storage.queues.purge = :active_storage_purge
    config.action_mailer.deliver_later_queue_name = :mailers

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Custom error pages
    config.exceptions_app = routes

    config.constants = config_for(:application)

    config.jwt = Jwt::Config.new(config.constants["jwt"])
    config.clock = Struct.new(:update).
                   new((config.constants[:clock][:update] || 30).seconds)

    config.i18n.load_path += Dir[Rails.root.join("config", "locales", "**", "*.{rb,yml}")]

    redis_url_string = config.constants["redis_url"]

    # Redis::Store does not handle Unix sockets well, so let's do it for them
    redis_config_hash = Redis::Store::Factory.
                        extract_host_options_from_uri(redis_url_string)
    redis_uri = URI.parse(redis_url_string)
    redis_config_hash[:path] = redis_uri.path if redis_uri.scheme == "unix"

    redis_config_hash[:namespace] = "cache:mee"
    redis_config_hash[:expires_in] = 90.minutes # Cache should not grow forever
    config.cache_store = :redis_cache_store, redis_config_hash

    config.generators do |g|
      g.test_framework :test_unit
      g.factory_bot false
    end
  end
end
