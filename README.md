# MEE - EurValve portal [![build status](https://gitlab.com/cyfronet/mee/badges/master/build.svg)](https://gitlab.com/cyfronet/mee/commits/master)

## Project description

MEE is a portal framework for patient cohort analysis. The aim of MEE is to provide a one-stop environment for
large-scale studies which involve multpile runs of simulation pipelines upon a range of input cases (i.e. individual
patients). Such analyses permit computational scientists to manage representative patient models, schedule HPC
simulations, download results and manage experimental pipelines.

MEE provides:

  * A consistent, Web-based GUI
  * HPC access automation, including staging of input data and retrieval of results from HPC storage
  * Creation and management of customizable execution environments for multiple organizations
  * A uniform security model, permitting authentication and authorization when accessing any of the above

MEE is intended for computational scientists and medical IT professionals.

## Using Docker Compose
MEE can be developed and deployed using Docker Compose. To do this you first have to install
Docker Engine and Docker Compose on your computer. Instructions can be found:

* [Docker Engine](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)

## Development with Docker Compose

First of all, you have to copy dev.env.template file in the Docker directory, rename it to dev.env and fill it
with necessary environmental variables such as Gitlab API token and an SSH key filepath.

To build MEE containers run
```
docker-compose build
```

To be able to use https you must create certificate and key for localhost domain
```
mkdir tmp; openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 365 -keyout tmp/localhost.key -out tmp/localhost.crt
```

When running MEE for the first time, you have to run setup using this command:
```
docker-compose run --rm web bin/setup
```

If you want to setup MEE with sample data you have to cd into Docker folder and run this command:
```
docker-compose run --rm web rake primage:setup
```

To run MEE you can use `docker-compose up` command. You can also add `-d` option
to run in detached mode. In another console `docker attach mee_web_1` can be used to
attach to container's output and debug it. `docker-compose stop` and `docker-compose start`
are used to  stop and start the cluster and `docker-compose down` is used to
remove containers. MEE volumes will persist unless manually removed.

## Deployment with Docker Compose

To get started with production deployment with Docker Compose you have to copy .env.template
file in the root of the repository, rename it to .env and set all environmental variables.
You will also need to generate a production X509 private key and store in config/jwt/prod.pem file.

You will want to build MEE production containers using
```
docker-compose -f prod.docker-compose.yml build
```

When running MEE for the first time, you have to run setup using this command:
```
docker-compose -f prod.docker-compose.yml run --rm web bin/setup
```

If you want to setup MEE with sample data you have to cd into Docker folder and run this command:
```
docker-compose -f prod.docker-compose.yml run --rm web rake primage:setup
```

To run MEE you can use `docker-compose -f prod.docker-compose.yml up` command. You can also add `-d` option
to run in detached mode. To take down the cluster use `docker-compose -f prod.docker-compose.yml down`.
If repository's version changes you only need to run `down` and `up` and the images will rebuild themselves
to the current version.

### Host machine nginx

The production cluster does not have https support and is supposed to be used with an nginx instance
running on the host machine. Port 4000 is bound to localhost which then needs to be reverse proxied to the
correct hostname. An template for the config is provided in the Docker/external-nginx.conf.template file which has to be
copied to conf.d folder of nginx configuration directory with .conf extension. Correct port of the web server,
hostnames and SSL certificate along with key paths have to be set in that file.


## Dependencies

asdf-vm can be used to install dependencies below ([installation guide](https://asdf-vm.com/#/core-manage-asdf-vm?id=install-asdf-vm))

  * MRI 2.6.x (`asdf plugin add ruby`)
  * NodeJS (`asdf plugin add nodejs`)

Then run `asdf install`

  * libvips (`sudo apt-get install libvips`)
  * PostgreSQL (`sudo apt-get install postgresql`)
  * PostgreSQL citext extension (`sudo apt-get install postgresql-contrib`)
  * PostgreSQL libpq-dev (`sudo apt-get install libpq-dev`)
  * Redis (`sudo apt-get install redis-server`)
  * Yarn ([installation guide](https://classic.yarnpkg.com/en/docs/install#debian-stable))

## DBMS Settings

You need to create user/role for your account in PostgreSQL. You can do it
using the 'createuser' command line tool. Then, make sure to alter this user
for rights to create databases.

## Installation

Run this command. It will, by default, create databases `mee_development` and `mee_test`.
```
bin/setup
```
If it does not complete successfully, do the next step.

### (optional) Manual activation of the citext extension

Skip this step if the previous one completed successfully.

As the PostgreSQL superuser, run the `CREATE EXTENSION IF NOT EXISTS citext
   WITH SCHEMA public;` on all databases (dev, test, ...) to activate the
   extension. So login to the `mee_development` database as the superuser (it is
   usually called 'postgres') and issue the CREATE EXTENSION command above. Then
   switch to `mee_test` and do the same.

When done, issue the `bin/setup` command again as a regular system user.
This time it should have no problem completing.

## Configuration

You need to:
* copy config/puma.rb.example into config/puma.rb
  and edit as required (env, location, socket/tcp). You probably want to have `application_path` set to `"."` and use tcp port 3000.
* create required directories defined in the config in tmp (such as pids). This step is not necessary if you configure `application path` as `"."` because required directories already exist in the project's repository.

## Running

To start only web application run:
```
bin/rails server
```

We are also using [sidekiq](https://github.com/mperham/sidekiq) to execute
delayed jobs and [clockwork](https://github.com/tomykaira/clockwork) for
triggering delayed jobs in defined interval. To run full application stack
perform following steps:
```
gem install foreman
foreman start
```

If you want to start development environment with https support first make sure
that `crt` and `key` are generated for localhost domain:

```
openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 365 -keyout tmp/localhost.key -out tmp/localhost.crt
```

Next you can use development foreman configuration:

```
foreman start -f Procfile.dev
```

or shortcut:
```
./bin/dev
```
to start https development environment (located at https://mee.lvh.me:3000).
`./bin/dev` script also checks if
[overmind](https://github.com/DarthSim/overmind) is present in the classpath
and uses it instead of `foreman`. `overmind` is more advanced than `foreman` and
plays nicely with e.g. `byebug`.

### Sample data

To load sample data for demo organizations you need to get an SSH key file for the project's pipelines from one of the team members and set it's location in PIPELINE_SSH_KEY environment variable.
You also need to create a private GitLab API access token and write it into GITLAB_API_PRIVATE_TOKEN environment variable.

For example:
```
export PIPELINE_SSH_KEY="./config/mee_ssh_key"
export GITLAB_API_PRIVATE_TOKEN="FAqDZyC_ap2gVyBLTL72"
```

Then run following commands:
```
./bin/rails eurvalve:setup
./bin/rails primage:setup
```
## ENV variables

We use ENV variables to keep secrets safe. To customize the application
you can set the following ENV variables:

  * `JWT_KEY_PATH` (optional) - path to key used to generate user JWT tokens
  * `REDIS_URL` (optional) - redis database url
  * `CLOCK_UPDATE` - Computations update period (in seconds)
  * `PROMETHEUS_HOST` (optional) - if present than this host will be used for all
    requests made to Rimrock otherwise default `prometheus.cyfronet.pl` will be
    used.
  * `SENTRY_DN` - if present MEE will sent error and performance information to
    Sentry
  * `SENTRY_ENVIRONMENT` - if present overrides sentry environment (default set
    to `Rails.env`)
  * `USER_OVERRIDE` (only for demo organization generation) - override system
    user (see `DemoOrganization#path` for details).
  * `ADMIN_PLGRID_LOGIN` (only in development environment) - if defined
    administrator with specified PLGrid login will be created.

## Testing

Some tests require Chrome headless installed. Please take a look at:
https://developers.google.com/web/updates/2017/04/headless-chrome for manual. To
install chrome on your debian based machine use following snippet:

```
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
```

To execute all tests run:

```
bundle exec rspec
```

To execute EurValve-specific File Storage integration tests:
1. Obtain EurValve dev file store key in the form of a pem file
2. Set path to this certificate in application.yml jwt.key value
4. Run rspec with files tag on:

```
bundle exec rspec --tag files
```

To execute Gitlab integration tests:
1. Obtain a valid Gitlab user token with access to the eurvalve/blood-flow project
2. Assign token payload to the GITLAB_API_PRIVATE_TOKEN environmental variable (e.g. by editing `.env`)
3. Run rspec with gitlab tag on:

```
bundle exec rspec --tag gitlab
```

## Internationalization by [i18n-task](https://github.com/glebm/i18n-tasks) tool

In tests automatically runs health (missing and unused) of translations using.

You can find configuration file at __*config/i18n-tasks.yml*__

To check the state of translations, just run
```rb
i18n-tasks health 
```
Remove translation from *.yml* file by command
```rb
i18n-tasks rm key-name
# or all
i18n-tasks unused -f yaml | i18n-tasks data-remove
```
Add missing translations to en.yml file
```rb
i18n-tasks add-missing -v 'TRME %{value}' en
```

### Config ignoring

In file *config/i18n-tasks.yml*, at section named: **ignore_unused** are some paths e.g. 
```rb
- 'admin.licenses.empty.*'
``` 
Tool don't recognize where translations are used. So we can configure to ignore this "unused" path as is line up.


## Using bullet to increase application performance
[Bullet](https://github.com/flyerhzm/bullet) gem is enabled in _development_ and _test_ environments.
While running application in development or running tests _bullet_ logs warnings to _log/bullet.log_ file.

## Contributing

1. Fork the project
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new pull request
6. When feature is ready add reviewers and wait for feedback (at least one
   approve should be given and all review comments should be resolved)
