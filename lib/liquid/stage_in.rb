# frozen_string_literal: true

module Liquid
  class StageIn < Liquid::Tag
    def initialize(tag_name, parameters, tokens)
      super
      @data_file_type, @filename = parameters.split
    end

    def render(context)
      filename, path = extract_file_from_context(context)
      if filename && path
        context.registers[:storage].
          stage_in(path:, target: "$SCRATCHDIR/#{filename}")
      else
        context.registers[:errors]
          .add(:script, "cannot find #{@data_file_type} data file in patient or pipeline directories")
      end
    end

    private
      def extract_file_from_context(context)
        data_file_type = context.registers[:organization].data_file_types.find_by(data_type: @data_file_type)

        extract_request_data(data_file_type:,
                             filename: @filename,
                             computation: context.registers[:computation])
      end

      def extract_request_data(data_file_type:, filename:, computation:)
        data_file = computation.data_file(data_file_type)
        target_filename = filename || data_file&.name
        [target_filename, data_file&.path]
      end
  end
end
