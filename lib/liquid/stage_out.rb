# frozen_string_literal: true

module Liquid
  class StageOut < Liquid::Tag
    def initialize(tag_name, relative_path, tokens)
      super
      @relative_path = relative_path.strip
    end

    def render(context)
      target = File.join(context.registers[:pipeline].outputs_dir)
      target.concat(File.basename(@relative_path)) if context.registers[:storage] == Storage::S3

      context.registers[:storage].stage_out(path: @relative_path, target:)
    end
  end
end
