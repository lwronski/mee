# frozen_string_literal: true

module Liquid
  class StageInArtifact < Liquid::Tag
    def initialize(tag_name, filename, tokens)
      super
      @filename = filename.strip
    end

    def render(context)
      path = context.registers[:organization].artifacts.find_by(name: @filename)&.path
      if path
        context.registers[:storage].
          stage_in(path:, target: "$SCRATCHDIR/#{@filename}")
      else
        context.registers[:errors]
               .add(:script, "cannot find #{@filename} artifact in organization directory")
      end
    end
  end
end
