# frozen_string_literal: true

namespace :site do
  desc "Setup supported sites"
  task setup: :environment do
    pro = Site.find_or_create_by!(name: "prometheus",
                            host: "prometheus.cyfronet.pl", host_key: "prometheus")
    Site.find_or_create_by!(name: "ares",
                            host: "ares.cyfronet.pl", host_key: "ares")
    Site.find_or_create_by!(name: "athena",
                            host: "athena.cyfronet.pl", host_key: "athena")

    ActiveRecord::Base.connection.execute("UPDATE steps SET site_id = #{pro.id}")
  end
end
