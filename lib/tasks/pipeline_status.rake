# frozen_string_literal: true

namespace :pipelines do
  desc "Set correct statuses for pipelines"
  task set_status: :environment do
    Pipeline.in_batches.each_record { |pipeline|
      pipeline.update(status: Pipeline::StatusCalculator.new(pipeline).calculate)
    }
  end
end
