# frozen_string_literal: true

namespace :patients do
  desc "Create slugs for existing patients' case numbers"
  task create_slugs: :environment do
    Patient.find_each(&:save)
  end
end
