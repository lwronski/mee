# frozen_string_literal: true

namespace :primage do
  desc "Setup primage organization"
  task setup: :environment do
    if ENV["S3_SECRET_KEY"].present? && ENV["S3_ACCESS_KEY"].present?
      storage_config =
        Storage::Config::S3.new(endpoint: "https://s3p.cloud.cyfronet.pl",
                                bucket: ENV["S3_BUCKET"].presence || "mee",
                                secret_key: ENV["S3_SECRET_KEY"],
                                access_key: ENV["S3_ACCESS_KEY"])
    else
      storage_config = nil
    end
    DemoOrganization.create! name: "PRIMAGE", logo_path: "db/logos/primage.png",
      ssh_key_path: ENV["PIPELINE_SSH_KEY"],
      gitlab_api_private_token: ENV["GITLAB_API_PRIVATE_TOKEN"],
      plgrid_team_id: "plggprimage",
      grants: {
        primage1: Date.parse("2019-06-24"),
        plgprimage2: Date.parse("2020-07-06"),
        plgprimage3: Date.parse("2021-07-06"),
        plgprimage4: Date.parse("2022-07-06"),
        "plgprimage4-cpu": Date.parse("2022-07-06"),
        "plgprimage4-gpu": Date.parse("2022-07-06"),
        "plgprimage4-gpu-a100": Date.parse("2022-07-06"),
      },
      storage_config:
  end
end
