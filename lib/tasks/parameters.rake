# frozen_string_literal: true

namespace :parameters do
  desc "Create parameters for model version and segmentation run mode"
  task setup: :environment do
    Computation.transaction do
      RimrockStep.all.each { |s| s.save! }

      RimrockComputation.includes(:parameter_values, step: :parameters)
        .where.not(tag_or_branch: nil).each do |c|
        c.tag_or_branch_parameter_value || create_model_version(c)
      end
    end
  end

  private
    def create_model_version(c)
      if c.tag_or_branch.present?
        pv = ParameterValue::ModelVersion.new(
          computation: c,
          parameter: c.step.parameter_for(RimrockStep::Parameters::TAG_OR_BRANCH),
          version: c.tag_or_branch
        )

        # We need to turn off validation since some of the branch used to run
        # existing computation does not exist anymore.
        pv.save!(validate: false)
      end
    end
end
