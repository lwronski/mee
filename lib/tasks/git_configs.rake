# frozen_string_literal: true

namespace :git_configs do
  desc "Migrate old gitlab config fields in Organizations and Steps to new git_config fields"
  task migrate: :environment do
    Organization.transaction do
      Organization.all.each do |organization|
        organization.git_config = GitConfig::Gitlab.new(transform_params(organization.config))
        organization.save!
      end
      RimrockStep.all.each do |step|
        step.git_config = GitConfig::Gitlab.new(transform_params(step.config))
        step.save!
      end
    end
  end

  task migrate_to_repo_config: :environment do
    Organization.transaction do
      RimrockStep.find_each do |step|
        git_config = step.git_config
        if git_config.host.blank? && git_config.download_key.blank? && git_config.try(:private_token).blank?
          git_config = nil
        end

        raw_git_config = JSON.parse(step.attributes_before_type_cast["git_config"])

        step.update!(repository: raw_git_config["repository"],
                     file: raw_git_config["file"],
                     git_config:)
      end
    end
  end

  private
    def transform_params(config)
      config = HashWithIndifferentAccess.new(config)
      { host: config[:gitlab_host] || config[:step_gitlab_host],
        private_token: config[:gitlab_private_token] || config[:step_gitlab_private_token],
        download_key: config[:gitlab_download_key] || config[:step_gitlab_download_key],
        repository: config[:repository],
        file: config[:file] }.compact
    end
end
