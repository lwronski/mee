# frozen_string_literal: true

module RoutesConstraint
  class Root
    def self.matches?(request)
      request.subdomain.blank? || request.subdomain == "www"
    end
  end
end
