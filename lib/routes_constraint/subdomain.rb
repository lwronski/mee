# frozen_string_literal: true

module RoutesConstraint
  class Subdomain
    def self.matches?(request)
      subdomain?(request) || Organization.find_by(domain: request.domain)
    end

    def self.subdomain?(request)
      request.subdomain.present? && request.subdomain != "www"
    end

    def self.organization_candidates(request)
      Organization.with_attached_logo.where(domain: request.domain).
        or(Organization.with_attached_logo.where(slug: request.subdomain))
    end
  end
end
