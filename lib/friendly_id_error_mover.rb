# frozen_string_literal: true

module FriendlyIdErrorMover
  # When included, this module adds error moving from friendly_id to base field.
  # Useful for scaffolded form error display.

  def self.included(model_class)
    model_class.class_eval do
      after_validation {
        errors.add friendly_id_config.base, *errors.delete(:friendly_id) if errors[:friendly_id].present?
      }
    end
  end
end
