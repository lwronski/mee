FROM ruby:3.0.2-alpine AS builder

RUN apk --no-cache add --virtual build-dependencies \
    build-base \
    # Nokogiri Libraries
    zlib-dev \
    libxml2-dev \
    libxslt-dev \
    # PG
    postgresql-dev \
    # JavaScript
    nodejs \
    yarn

RUN apk --no-cache add \
    # ActiveStorage file inspection
    file \
    # Time zone data
    tzdata \
    # Image Resizing
    imagemagick \
    # Allows for mimemagic gem to be installed
    shared-mime-info \
    # Allows usage of rails console
    less \
    # Allows ssh key validation
    openssh-client \
    # Nice to have
    bash \
    git \
    nano vim

# DEVELOPMENT STAGE
FROM builder AS development

# Set common ENVs
ENV BOOTSNAP_CACHE_DIR /usr/src/bootsnap
ENV YARN_CACHE_FOLDER /usr/src/yarn
ENV WEBPACKER_CACHE_FOLDER /mee/node_modules/.cache
ENV EDITOR vim
ENV LANG en_US.UTF-8
ENV BUNDLE_PATH /usr/local/bundle
ENV RAILS_LOG_TO_STDOUT enabled
ENV HISTFILE /mee/log/.bash_history

# Set build args. These let linux users not run into file permission problems
ARG USER_ID=${USER_ID:-1000}
ARG GROUP_ID=${GROUP_ID:-1000}

# Add non-root user and group with alpine first available uid, 1000
RUN addgroup -g $GROUP_ID -S mee \
      && adduser -u $USER_ID -S mee -s /bin/bash -G mee

# Install multiple gems at the same time
RUN bundle config set jobs $(nproc)

RUN mkdir -p /mee \
      && mkdir -p /mee/node_modules \
      && mkdir -p /mee/public/packs \
      && mkdir -p /mee/tmp/cache \
      && mkdir -p /mee/storage \
      && mkdir -p $YARN_CACHE_FOLDER \
      && mkdir -p $BOOTSNAP_CACHE_DIR \
      && mkdir -p $WEBPACKER_CACHE_FOLDER \
      && chown -R mee:mee /mee \
      && chown -R mee:mee $BUNDLE_PATH \
      && chown -R mee:mee $BOOTSNAP_CACHE_DIR \
      && chown -R mee:mee $YARN_CACHE_FOLDER \
      && chown -R mee:mee $WEBPACKER_CACHE_FOLDER
WORKDIR /mee

ENV PATH /mee/bin:$PATH

# Define the user running the container
USER mee

EXPOSE 3000
CMD ["/mee/bin/rails", "server", "-b", "ssl://localhost:3000?key=tmp/localhost.key&cert=tmp/localhost.crt"]

# PRODUCTION STAGE
FROM development AS production

ENV RAILS_ENV production
ENV RACK_ENV production
ENV NODE_ENV production

COPY Gemfile /mee
COPY Gemfile.lock /mee

# Install Ruby Gems
RUN bundle config set deployment 'true' \
      && bundle config set without 'development:test' \
      && bundle check || bundle install --jobs=$(nproc)

COPY package.json /mee
COPY yarn.lock /mee

# Install Yarn Libraries
RUN yarn install --check-files

# Chown files so none are root.
COPY --chown=mee:mee . /mee

# Copy dummy production key to precompile assets
COPY Docker/prod-dummy-key.pem /mee/config/jwt/prod.pem

# Precompile the assets & bootsnap
RUN RAILS_SERVE_STATIC_FILES=enabled \
      SECRET_KEY_BASE=secret-key-base \
      bundle exec rake assets:precompile \
      && bundle exec bootsnap precompile --gemfile app/ lib/

# Remove dummy production key
RUN rm /mee/config/jwt/prod.pem

# WEB SERVER STAGE
# This stage has a standard Nginx with precompiled static assets added
FROM nginx:latest as web_server
COPY --from=production /mee/public/ /public/
COPY --from=production /mee/Docker/nginx.conf /etc/nginx/conf.d/default.conf