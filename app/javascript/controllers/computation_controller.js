import { Controller } from "@hotwired/stimulus";
import consumer from "../channels/consumer";

export default class extends Controller {
  static targets = ["content", "menu"];
  static values = {
    id: String
  }

  connect() {
    const self = this;

    this.channel = consumer.subscriptions.create({
        channel: "ComputationChannel",
        id: this.idValue
      }, {
        received(data) {
          self.received(data);
        }
    });
  }

  disconnect() {
    this.channel.unsubscribe();
  }

  received(data) {
    if (data.reload_step) {
      this.reloadStep();
    } else {
      this.reloadMenu(data.menu);
    }

    if (data.reload_files) {
      this.reloadOutputs();
    }
  }

  reloadStep() {
    fetch(window.location.href,
          { headers: { "X-Requested-With": "XMLHttpRequest" }})
      .then(response => response.text())
      .then(html => this.contentTarget.outerHTML = html);
  }

  reloadMenu(menu) {
    this.menuTarget.outerHTML = menu;
  }

  reloadOutputs() {
    const browsers = document.getElementById("file-browsers");

    fetch(browsers.dataset.filesUrl + "?" + new URLSearchParams({"computation_id": this.idValue}),
          { headers: { "X-Requested-With": "XMLHttpRequest" }})
      .then(response => response.text())
      .then(html => browsers.innerHTML = html);
  }
}
