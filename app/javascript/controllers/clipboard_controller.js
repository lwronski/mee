import { Controller } from '@hotwired/stimulus'

export default class extends Controller {
  static targets = ['source']

  connect() {
    if (!document.queryCommandSupported('copy')) {
      this.element.classList.add('d-none')
    }
  }

  copy(e) {
    e.preventDefault()

    this.sourceTarget.classList.remove('d-none')
    this.sourceTarget.select()
    document.execCommand('copy')
    this.sourceTarget.classList.add('d-none')
  }
}
