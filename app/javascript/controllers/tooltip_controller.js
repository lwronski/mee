import {Controller} from "@hotwired/stimulus";
import $ from "jquery"

export default class extends Controller{
    connect(){
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    }
}