# frozen_string_literal: true

module Pipelines
  class Affected
    def initialize(data_files)
      @data_files = data_files
    end

    def call
      Pipeline.
        left_joins(:campaign).
        where(runnable_id: patients_ids).
        or(Pipeline.where(id: pipelines_ids)).
        and(Pipeline.where(campaign: nil). # do not start campaign automatically if it was not started manually
          or(Pipeline.where.not(campaign: { status: [0, 1] }))
           ).
      uniq
    end

    private
      def patients_ids
        @data_files.
          select { |df| df.fileable_type == "Patient" }.
          reject { |df| df.input_of_id || df.output_of_id }.
          map(&:fileable_id)
      end

      def pipelines_ids
        @data_files.
          map { |df| df.input_of_id || df.output_of_id }.
          reject(&:blank?)
      end
  end
end
