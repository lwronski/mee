# frozen_string_literal: true

module ComputationsHelper
  def start_or_restart_text(computation)
    computation.finished? ? "restart" : "start"
  end

  def submit_button_text(computation)
    t("computation.#{start_or_restart_text(computation)}_#{computation.mode}", step: computation.name)
  end

  def alert_computation_class(computation)
    "alert-#{alert_class_postfix(computation)}"
  end

  def source_comparison_link(from_comp, to_comp)
    repo = computation_repo(from_comp)
    link_to source_comparison_link_text(from_comp, to_comp),
            "https://#{host(from_comp)}/#{repo}/compare/#{from_comp.revision}...#{to_comp.revision}",
            target: "_blank", rel: "noopener"
  end

  def computation_run_text(computation)
    t("computation.start_#{computation.mode}", step: computation.name)
  end

  def cannot_start(computation)
    t("computation.cannot_start", step: computation.name.downcase)
  end

  def cannot_start_step(computation)
    t("computation.cannot_start_step", step: computation.name.downcase)
  end

  def computations_status(computation)
    t("computation.#{computation.status}", step: computation.name)
  end

  private
    def computation_repo(computation)
      computation.step.repository
    end

    def host(computation)
      computation.step.host
    end

    def source_comparison_link_text(from_comp, to_comp)
      I18n.t(
        "comparisons.sources.link_html",
        computation_step: from_comp.name,
        compared_revision: "#{from_comp.tag_or_branch}:#{from_comp.revision}",
        compare_to_revision: "#{to_comp.tag_or_branch}:#{to_comp.revision}"
      ).html_safe
    end

    def alert_class_postfix(computation)
      case computation.status
      when "error" then "danger"
      when "finished" then "success"
      when "aborted" then "warning"
      else "info"
      end
    end
end
