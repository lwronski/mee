# frozen_string_literal: true

module PipelineHelper
  def pipeline_title(pipeline)
    pipeline_icon(pipeline) + " " +
      pipeline.name + " " +
      content_tag(:small,
                  pipeline.flow.name +
                  " " +
                  I18n.t("pipelines.show.subtitle", mode: pipeline.mode),
                  class: "text-muted")
  end

  def pipeline_owner(pipeline)
    content_tag(:div,
                I18n.t("pipelines.show.owner", owner: pipeline.owner_name),
                class: "label label-primary owner-label")
  end

  private
    def pipeline_icon(pipeline)
      classes =
        if pipeline.automatic?
          pipeline.status == :running ? %w[fas fa-cog fa-spin] : %w[fas fa-cog]
        else
          %w[far fa-hand-paper]
        end

      tag.i(class: classes)
    end
end
