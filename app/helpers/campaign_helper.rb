# frozen_string_literal: true

module CampaignHelper
  def show_progress_bar?(campaign)
    !campaign.creating_pipelines_failed? && !campaign.initializing?
  end
end
