# frozen_string_literal: true

module GitConfigsHelper
  def git_config_errors(errors)
    content_tag(:div, errors, class: 'alert alert-danger mt-2{ role: "alert" }') unless errors == "<div></div>"
  end

  def field_required?(object, attribute)
    HashWithIndifferentAccess.new(object.attributes).include?(attribute)
  end
end
