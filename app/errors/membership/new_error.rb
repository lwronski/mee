# frozen_string_literal: true

class Membership::NewError < NameError; end
