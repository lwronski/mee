# frozen_string_literal: true

class DeleteDirectoryJob < ApplicationJob
  class DirectoryDeletionError < StandardError; end

  retry_on(DirectoryDeletionError) do |job, _|
    user, organization, path = job.arguments
    message = "[#{organization.name}] #{user.email} was unable to delete #{path} directory"
    Sentry.capture_message message
    Rails.logger.error message
  end

  def perform(user, organization, path)
    organization.storage_for(user).remove(path) || (raise DirectoryDeletionError)
  end
end
