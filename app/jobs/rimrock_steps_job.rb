# frozen_string_literal: true

class RimrockStepsJob < ApplicationJob
  queue_as :default

  def perform
    Organization.find_each do |organization|
      organization.rimrock_steps.each do |step|
        RimrockStep::ValidateRepositoryJob.perform_later(step)
      end
    end
  end
end
