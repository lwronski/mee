# frozen_string_literal: true

module Pipelines
  class CreateJob < ApplicationJob
    queue_as :computation
    after_perform :start_runnable

    def perform(current_user, pipeline)
      @pipeline = pipeline
      ::Pipelines::Create.new(current_user, pipeline).call
    end

    private
      def start_runnable
        ::Pipelines::StartRunnableJob.perform_later(@pipeline) if @pipeline.automatic?
      end
  end
end
