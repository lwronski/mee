# frozen_string_literal: true

class FlowsJob < ApplicationJob
  queue_as :default

  def perform
    Flow.cleanup
  end
end
