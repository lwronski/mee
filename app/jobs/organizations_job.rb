# frozen_string_literal: true

class OrganizationsJob < ApplicationJob
  queue_as :default

  def perform
    Organization.find_each do |organization|
      Organization::ValidateTokenJob.perform_later(organization) if organization.git_uses_private_token?
    end
  end
end
