# frozen_string_literal: true

class RimrockStep::ValidateRepositoryJob < ApplicationJob
  queue_as :default

  def perform(step)
    RimrockStep::ValidateRepository.new(step).call
  end
end
