# frozen_string_literal: true

class Storage::DestroyFileJob < ApplicationJob
  queue_as :data_files

  def perform(user, file)
    Storage::DestroyFile.new(user, file).call
  end
end
