# frozen_string_literal: true

class Notifier < ApplicationMailer
  def user_registered(user:, organization:)
    to = organization.supervisors.pluck(:email)
    subject = I18n.t("emails.approve_user.subject", name: user.name)

    @user = user
    @approve_url = admin_users_url(subdomain: organization.slug)

    mail(to:, subject:) if to.present?
  end

  def account_approved(user:, organization:)
    @organization_name = organization.name
    @login_url = root_url(subdomain: organization.slug)

    mail(to: user.email, subject: I18n.t("emails.account_approved.subject",
                                         organization: @organization_name))
  end

  def proxy_expired(user)
    @proxy = user.proxy
    mail(to: user.email, subject: I18n.t("emails.proxy_expired.subject"))
  end
end
