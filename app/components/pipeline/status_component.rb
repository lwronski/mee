# frozen_string_literal: true

class Pipeline::StatusComponent < ViewComponent::Base
  def initialize(pipeline:, title: nil)
    @pipeline = pipeline
    @title = title
  end

  def prefix
    "#{@title}:" if @title
  end

  def render?
    @pipeline.present?
  end
end
