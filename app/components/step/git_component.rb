# frozen_string_literal: true

class Step::GitComponent < ViewComponent::Base
  def initialize(step:)
    @step = step
  end

  def render?
    step.type == "RimrockStep"
  end

  attr_reader :step
end
