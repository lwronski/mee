# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user
    identified_by :current_organization

    def connect
      self.current_organization = find_organization
      self.current_user = find_verified_user_in(current_organization)
      logger.add_tags "ActionCable", current_user.email
    end

    private
      def find_verified_user_in(organization)
        verified_user = User.approved_in(organization).find_by(id: cookies.signed["user.id"])

        verified_user || reject_unauthorized_connection
      end

      def find_organization
        RoutesConstraint::Subdomain.organization_candidates(request).first
      end
  end
end
