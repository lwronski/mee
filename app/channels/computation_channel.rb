# frozen_string_literal: true

class ComputationChannel < ApplicationCable::Channel
  include Channel::Runnable::Find

  def subscribed
    stream_for computation if computation
  end

  def receive(data)
    if data["new_input"]
      computation.pipeline.patient.execute_data_sync(current_user)
      ComputationUpdater.new(computation).call
      Pipelines::StartRunnableJob.perform_later(computation.pipeline)
    end
  end

  private
    def computation
      Computation.find(params[:id])
    end
end
