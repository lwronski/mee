# frozen_string_literal: true

module Channel::Runnable::Find
  extend ActiveSupport::Concern

  private
    def runnable
      patient_policy_scope.find_by(slug: params[:runnable])
    end

    # todo generalize to runnable policy
    def patient_policy_scope
      uc = UserContext.new(current_user, current_organization)
      PatientPolicy::Scope.new(uc, Patient).resolve
    end
end
