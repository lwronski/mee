# frozen_string_literal: true

class RunnableChannel < ApplicationCable::Channel
  include Channel::Runnable::Find

  def subscribed
    stream_for runnable if runnable
  end
end
