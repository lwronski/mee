# frozen_string_literal: true

class CampaignPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      Campaign.where(organization:)
    end
  end

  def index?
    true
  end

  def new?
    storage_valid?
  end

  def create?
    in_organization? && organization_flow? && storage_valid?
  end

  def run?
    in_organization? && storage_valid?
  end

  def permitted_attributes
    [:name, :cohort, pipeline: [:flow_id, :parameters_values]]
  end

  def show?
    in_organization?
  end

  def destroy?
    admin?
  end
end
