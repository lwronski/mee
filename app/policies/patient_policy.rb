# frozen_string_literal: true

class PatientPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:)
    end
  end

  def index?
    true
  end

  def show?
    in_organization?
  end

  def new?
    storage_valid?
  end

  def create?
    in_organization? && storage_valid?
  end

  def update?
    false
  end

  def destroy?
    in_organization? && storage_valid?
  end

  def permitted_attributes
    [:case_number]
  end
end
