# frozen_string_literal: true

class GrantPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def new?
    admin?
  end

  def create?
    admin? && in_organization?
  end

  def edit?
    admin? && in_organization?
  end

  def update?
    admin? && in_organization?
  end

  def destroy?
    admin? && in_organization?
  end

  def permitted_attributes
    [:name, :start_at, :end_at, grant_type_ids: []]
  end

  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization: [organization, nil])
    end
  end
end
