# frozen_string_literal: true

class ArtifactPolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    storage_valid?
  end

  def create?
    in_organization? && storage_valid?
  end

  def edit?
    in_organization?
  end

  def update?
    in_organization? && storage_valid?
  end

  def destroy?
    in_organization? && storage_valid?
  end

  def permitted_attributes_for_new
    [:file_id]
  end

  def permitted_attributes_for_create
    [:name]
  end

  def permitted_attributes_for_update
    [:name]
  end
end
