# frozen_string_literal: true

class Storage::Config::S3Policy < ApplicationPolicy
  def permitted_attributes
    [:type, :endpoint, :bucket, :secret_key, :access_key, :region, :prefix]
  end
end
