# frozen_string_literal: true

class StepPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:).kept
    end
  end

  def index?
    user.organizations.include?(organization)
  end

  def new?
    in_organization?
  end


  def create?
    in_organization?
  end

  def edit?
    (admin? || owner?) && in_organization? && without_active_computation?
  end

  def update?
    (admin? || owner?) && in_organization? && without_active_computation?
  end

  def destroy?
    (admin? || owner?) && in_organization?
  end

  private
    def without_active_computation?
      record.computations.active.size.zero?
    end

    def owner?
      record.user == user
    end
end
