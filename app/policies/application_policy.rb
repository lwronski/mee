# frozen_string_literal: true

class ApplicationPolicy
  class ApplicationScope
    attr_reader :scope
    delegate :user, :organization, to: :@user_context
    delegate :approved?, :admin?, :supervisor?, to: :@user_context

    def initialize(user_context, scope)
      @user_context = user_context
      @scope = scope
    end
  end

  attr_reader :record, :user_context
  delegate :user, :organization, to: :user_context
  delegate :approved?, :admin?, :supervisor?, to: :user_context

  def initialize(user_context, record)
    @user_context = user_context
    @record = record
  end

  protected
    def in_organization?
      record &&
        record.organization_id.present? ?
          record.organization_id == organization.id :
          default_relation_organization == organization
    end

    def storage_valid?
      @storage_valid ||= organization.storage_for(user).valid?
    end

  private
    def default_relation_organization
      record.association(:organization).reflection.options[:default]&.call
    end
end
