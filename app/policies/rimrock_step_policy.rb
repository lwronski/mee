# frozen_string_literal: true

class RimrockStepPolicy < StepPolicy
  def versions?
    true
  end

  def permitted_attributes
    [:name, :grant_type_id, :repository, :file, :site_id, required_file_type_ids: []]
  end
end
