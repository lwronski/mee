# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      if admin? || supervisor?
        scope.all
      else
        scope.where(id: user&.id)
      end
    end
  end

  def index?
    supervisor? || admin?
  end

  def destroy?
    admin?
  end

  def update?
    supervisor? || admin?
  end

  def manage_users?
    supervisor? || admin?
  end
end
