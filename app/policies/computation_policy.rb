# frozen_string_literal: true

class ComputationPolicy < ApplicationPolicy
  def permitted_attributes
    [parameter_values_attributes: parameter_attributes]
  end

  def show?
    in_organization?
  end

  private
    def parameter_attributes
      record.form_parameters_values.to_h do |pv|
        [pv.key, pv.permitted_params]
      end
    end
end
