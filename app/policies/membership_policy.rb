# frozen_string_literal: true

class MembershipPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      if admin? || supervisor?
        scope.where(organization:)
      else
        scope.where(id: user&.id, organization:)
      end
    end
  end

  def index?
    supervisor? || admin?
  end

  def destroy?
    admin?
  end

  def update?
    supervisor? || admin?
  end

  def update_roles?
    admin?
  end

  def manage_users?
    supervisor? || admin?
  end

  def permitted_attributes
    [:state, roles: []]
  end
end
