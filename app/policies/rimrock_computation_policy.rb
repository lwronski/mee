# frozen_string_literal: true

class RimrockComputationPolicy < ComputationPolicy
  def show?
    true
  end

  def update?
    record.user == user && !record.active? && can_update_in_mode? &&
      record.step.persistent_errors_count.zero?
  end

  def abort?
    record.user == user && record.active? && record.manual?
  end

  def can_update_in_mode?
    if record.manual?
      record.runnable?
    else
      record.step.parameters.size != record.parameter_values.size
    end
  end
end
