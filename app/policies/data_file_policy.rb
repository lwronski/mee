# frozen_string_literal: true

class DataFilePolicy < ApplicationPolicy
  def upload?
    storage_valid?
  end

  def download?
    storage_valid?
  end

  def destroy?
    owner_policy.new(user_context, owner).destroy?
  end

  private
    def owner
      record.input_of || record.output_of || record.fileable
    end

    def owner_policy
      Pundit::PolicyFinder.new(owner).policy
    end
end
