# frozen_string_literal: true

class GitRepository
  attr_reader :host, :path, :download_key

  def initialize(attrs = {})
    @host = attrs[:host]
    @path = attrs[:path]
    @download_key = attrs[:download_key]
  end

  def key_valid?
    _, _, status = ls_remote

    status.success?
  end

  def versions(force_reload: false)
    raise NotImplementedError, "#{self.class} must implement this method"
  end

  def content_and_revision_for(file_path, tag_or_branch)
    raise NotImplementedError, "#{self.class} must implement this method"
  end

  protected
    def ls_remote
      git_command = "git ls-remote --heads --tags --refs git@#{@host.shellescape}:#{@path.shellescape}"
      run_git_command(git_command)
    end

    def run_git_command(command)
      Open3.capture3 <<~CONTENT
        ssh-agent bash -c '
          echo "#{@download_key.delete("\"")}" | ssh-add -
          export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no -F /dev/null -o IdentityFile=/dev/null"
          #{command}'
      CONTENT
    end
end
