# frozen_string_literal: true

class ProgressBar::Base
  def data
    {
      tooltip_data:,
      all:,
    }.merge(properties)
  end

  def all
    raise "This method should be implemented by descendent class"
  end

  def number_of_successful
    raise "This method should be implemented by descendent class"
  end

  def number_of_failed
    raise "This method should be implemented by descendent class"
  end

  private
    def properties
      methods.select { |method| method.start_with? "number_of_" }
        .flat_map {  |property| data_for(property:) }
        .to_h
    end

    def data_for(property:)
      [
        ["#{property.to_s.delete_prefix("number_of_")}_width".to_sym, progress_bar_width(send(property))],
        [property, send(property)]
      ]
    end

    def progress_bar_width(numerator)
      Percentage.new(numerator.to_f, all.to_f).to_f
    end
end
