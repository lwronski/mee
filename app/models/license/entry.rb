# frozen_string_literal: true

class License::Entry
  include ActiveModel::Model
  include ActiveModel::Attributes
  include ActiveModel::Validations

  attribute :key, :string
  attribute :value, :string

  validates :key, presence: true
  validates :value, presence: true

  validates :key, format: { with: /\A[a-zA-Z_]+[a-zA-Z0-9_]*\Z/,
      message: "has wrong environmental variable name format. Only letters,
      digits and underscores are allowed. Cannot start with digit" }

  def to_export
    "export #{key.upcase}=#{value}"
  end

  class Array
    class << self
      def load(hsh)
        hsh&.map { |k, v| License::Entry.new(key: k, value: v) }
      end

      def dump(list)
        list&.map { |obj| [obj.key, obj.value] }.to_h
      end
    end
  end
end
