# frozen_string_literal: true

class StorageConfig
  include ActiveModel::API
  include ActiveModel::Attributes

  class << self
    def load(hsh)
      if hsh.present?
        clazz = by_type(hsh.delete("type"))
        clazz&.new(hsh.slice(*clazz.attribute_names))
      end
    end

    def dump(object)
      if object
        ActiveSupport::HashWithIndifferentAccess
          .new(object.attributes
               .merge({ type: object.class.model_name.element }))
          .compact
      end
    end

    def types
      [StorageConfig::Gridftp]
    end

    private
      def by_type(type)
        types.find { |t| t.model_name.element == type }
      end
  end
end
