# frozen_string_literal: true

class Proxy < OpenSSL::X509::Certificate
  attr_reader :cert_exists

  def initialize(payload)
    @payload = payload
    super(payload)
    @cert_exists = true
  rescue OpenSSL::X509::CertificateError
    @cert_exists = false
  end

  def valid?
    @cert_exists && not_after > Time.current && not_before < Time.current
  end

  def encode
    @cert_exists && Base64.encode64(to_s).gsub!(/\s+/, "")
  end

  def to_s
    @payload
  end

  # This is a temporary hack for ruby 3.1.2 and OpenSSL 3.0. When this
  # combination is present and we don't have `==` method then the following
  # code returns true:
  # ```ruby
  #  Proxy.for(nil) == Proxy.for(File.read("/correct/proxy/path"))
  # ```
  def ==(other)
    to_s.strip == other.to_s.strip
  end

  def proxy_cert
    "-----BEGIN CERTIFICATE-----#{parts[0]}-----END CERTIFICATE-----\n"
  end

  def proxy_priv_key
    "-----BEGIN RSA PRIVATE KEY-----#{parts[1]}-----END RSA PRIVATE KEY-----\n"
  end

  def user_cert
    "-----BEGIN CERTIFICATE-----#{parts[2]}-----END CERTIFICATE-----"
  end

  class << self
    def for(value)
      return new("") if value.nil?
      value.is_a?(Proxy) ? value : new(value)
    end

    def load(value)
      Proxy.for(value)
    end

    def dump(value)
      return unless value
      proxy = Proxy.for(value)
      proxy.cert_exists ? proxy.to_s : nil
    end
  end

  private
    CERT_START = /-----.+-----/

    def parts
      @parts ||= @payload.split(CERT_START).reject(&:blank?)
    end
end
