# frozen_string_literal: true

class Pipeline < ApplicationRecord
  include ActionView::RecordIdentifier

  enum mode: [:automatic, :manual]
  has_rich_text :notes

  belongs_to :runnable, polymorphic: true

  belongs_to :user
  belongs_to :flow

  belongs_to :campaign, required: false

  # Inputs and outputs relation stores pipeline specific data files
  has_many :inputs,
           class_name: "DataFile",
           foreign_key: "input_of_id",
           inverse_of: "input_of",
           dependent: :destroy

  has_many :outputs,
           class_name: "DataFile",
           foreign_key: "output_of_id",
           inverse_of: "output_of",
           dependent: :destroy

  has_many :computations,
           # computations are created according to flow_step.position order (see pipelines/build.rb)
           # so this scope returns computations in order steps were put in the flow
           -> { order(:created_at) },
           dependent: :destroy

  validate :set_iid, on: :create
  validates :iid, presence: true, numericality: true
  validates :name, presence: true
  validates :mode, presence: true
  validates_associated :computations, on: :create

  scope :automatic, -> { where(mode: :automatic) }
  scope :latest, ->(nr = 3) { reorder(created_at: :desc).limit(nr) }

  delegate :steps, to: :flow

  after_destroy { |pipe| pipe.flow.destroy unless pipe.flow.kept? || pipe.flow.used? }
  after_destroy { |pipeline| DeleteDirectoryJob.perform_later(Current.user, Current.organization, pipeline.root_dir) }

  after_touch do
    set_status
    computation_status_changed
    update_frontend if campaign
  end


  enum status: {
    waiting: 0,
    running: 1,
    success: 2,
    error: 3
  }

  def outputs_dir
    File.join(root_dir, "outputs", "/")
  end

  def inputs_dir
    File.join(root_dir, "inputs", "/")
  end

  def root_dir
    File.join(runnable.pipelines_dir, iid.to_s, "/")
  end

  def data_file(data_type)
    data_files(data_type).first
  end

  def data_files(data_type)
    DataFile.
        where(fileable: runnable,
              output_of: [nil, self],
              input_of: [nil, self],
              data_type:)
  end

  def status
    # TODO Remove after next release (after rake task run)
    super || Pipeline::StatusCalculator.new(self).calculate
  end

  def set_status
    # TODO Remove if when we upgrade to Rails 7.1 https://github.com/rails/rails/pull/46522)
    new_status = Pipeline::StatusCalculator.new(self).calculate
    update(status: new_status) if new_status != status.to_sym
  end

  def owner_name
    user&.name || "(deleted user)"
  end

  def archived?
    flow.discarded?
  end

  def computation_status_changed
    campaign&.pipeline_status_changed
  end

  def update_frontend
    broadcast_replace target: dom_id(runnable),
    partial: "campaigns/patient_table/pipeline_row",
    locals: { pipeline: self }
  end

  private
    def set_iid
      self.iid = runnable.pipelines.maximum(:iid).to_i + 1 if iid.blank?
    end
end
