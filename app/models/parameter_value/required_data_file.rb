# frozen_string_literal: true

class ParameterValue::RequiredDataFile < ParameterValue
  store_accessor :value_store, :data_file_id, :data_file_type_id

  attribute :data_file_id, :integer
  validates :data_file_id, presence: true

  def initialize(attrs)
    attrs[:data_file_type_id] = attrs["parameter"].data_file_type.id if attrs["parameter"]

    super(attrs)
  end

  def permitted_params
    [:data_file_id]
  end

  def data_files
    [
      pipeline_inputs,
      pipeline_outputs,
      patient_inputs
    ].compact
  end

  attr_writer :parameter
  def parameter
    @parameter ||= load_parameter
  end

  def value
    DataFile.find_by(id: data_file_id.to_i)&.name
  end

  def blank?
    data_file_id.blank?
  end

  def to_s
    data_file_id
  end

  private
    def load_parameter
      dft = DataFileType.find_by(id: data_file_type_id)
      if dft
        data_files = computation.pipeline.data_files(dft)
        Parameter::RequiredDataFile.new(data_file_type: dft, data_files:)
      end
    end

    def pipeline_inputs
      data_files = parameter.data_files.select { |df| df.input_of_id.present? }
      ["Pipeline inputs", data_files] if data_files.any?
    end

    def pipeline_outputs
      data_files = parameter.data_files.select { |df| df.output_of_id.present? }
      ["Pipeline outputs", data_files] if data_files.any?
    end

    def patient_inputs
      data_files = parameter.data_files
        .select { |df| df.input_of_id.blank? && df.output_of_id.blank? }
      ["Patient inputs", data_files] if data_files.any?
    end
end
