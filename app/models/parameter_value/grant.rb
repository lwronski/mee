# frozen_string_literal: true

class ParameterValue::Grant < ParameterValue
  store_accessor :value_store, :value

  validates :value, inclusion: {
    in: ->(obj) { obj.parameter.active_grants.map(&:name) },
  }, if: :validate_grant?

  validates :value, presence: true

  def permitted_params
    [:value]
  end

  def blank?
    value.blank?
  end

  def default_value
    value || default_grant_name
  end

  def to_s
    value
  end

  private
    def default_grant_name
      active_grants = parameter.active_grants
      active_grants.first.name if active_grants.size == 1
    end

    def validate_grant?
      parameter && value.present?
    end
end
