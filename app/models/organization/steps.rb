# frozen_string_literal: true

module Organization::Steps
  extend ActiveSupport::Concern

  included do
    has_many :steps, dependent: :destroy
    has_many :rimrock_steps,
             -> { where(type: "RimrockStep") },
             class_name: "Step"
  end
end
