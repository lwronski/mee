# frozen_string_literal: true

module Organization::Storage
  extend ActiveSupport::Concern

  included do
    serialize :storage_config, Storage::Config
    validates_associated :storage_config
    validates :storage_config,
      presence: { message: "creation failed. Attributes are missing or wrong" }

    delegate :storage_for, to: :storage_config
  end

  def storage_config_attributes=(attrs)
    self.storage_config = Storage::Config.load(attrs.presence)
  end

  def pipelines_dir
    File.join(working_dir, "pipelines", "/")
  end

  def working_dir
    File.join(storage_config.path, "/", Rails.env, "/")
  end

  def artifacts_dir
    File.join(working_dir, "artifacts", "/")
  end
end
