# frozen_string_literal: true

module Organization::Git
  extend ActiveSupport::Concern
  include GitAttributes

  included do
    serialize :git_config, GitConfig
    validates_associated :git_config
    validates :git_config, presence: { message: "creation failed. Attributes are missing or wrong" }
  end

  def git_uses_private_token?
    self.git_config.attributes.include?("private_token")
  end
end
