# frozen_string_literal: true

class DataFileType < ApplicationRecord
  include OrganizationUnit

  validates :data_type, presence: true
  validates :pattern, presence: true
  validate :valid_pattern

  enum viewer: {
    text: "text",
    graphics: "graphics"
  }

  has_many :data_files,
           class_name: "DataFile",
           foreign_key: "data_type_id",
           inverse_of: "data_type",
           dependent: :nullify

  has_many :prerequisites, dependent: :destroy
  has_many :steps, through: :prerequisites, source: :step

  def match?(file_name)
    Regexp.new(pattern).match?(file_name)
  end

  def pattern_preview
    Regexp.new(pattern).inspect
  end

  private
    def valid_pattern
      Regexp.new(pattern) if pattern.present?
    rescue RegexpError => e
      errors.add(:pattern, e.message)
    end
end
