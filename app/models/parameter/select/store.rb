# frozen_string_literal: true

class Parameter::Select::Store
  class << self
    def load(json)
      if json.present?
        {
          "values" => values(json["values"]),
          "default_value" => json["default_value"]
        }
      end
    end

    def dump(obj)
      {
        "values" => obj["values"].map { |option| [option.name, option.value] },
        "default_value" => obj["default_value"]
      }
    end

    private
      def values(json)
        json&.map do |o|
          Parameter::Select::Value.new(name: o.first, value: o.last)
        end
      end
  end
end
