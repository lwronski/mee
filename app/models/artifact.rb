# frozen_string_literal: true

class Artifact < ApplicationRecord
  include OrganizationUnit

  has_rich_text :notes

  validates :name, presence: true, uniqueness: true
  broadcasts_to ->(artifact) { [artifact.organization, "artifacts"] }

  attr_accessor :file_id
  before_validation :add_name_extension, on: :create

  def file
    @file ||= DataFile.find file_id if file_id
  end

  def path
    File.join(organization.artifacts_dir, name)
  end

  private
    def add_name_extension
      self.name = ActiveStorage::Filename.new("#{name}#{file&.extension}")
    end
end
