# frozen_string_literal: true

class GitConfig::Gitlab < GitConfig
  attribute :private_token, :string

  validates :private_token, presence: true, gitlab_token: true

  def git_repository(repository_path:)
    GitRepository::GitlabClient.new(host:,
                                    path: repository_path,
                                    download_key:,
                                    private_token:)
  end
end
