# frozen_string_literal: true

class Campaign < ApplicationRecord
  include ActionView::RecordIdentifier
  belongs_to :cohort, counter_cache: :campaigns_count, optional: true
  has_many :pipelines, dependent: :destroy

  belongs_to :organization, default: -> { Current.organization }
  attr_accessor :flow

  before_create do
    self.desired_pipelines = cohort.patients.size
  end

  after_update -> {
    broadcast_update target: "run_button", partial: "campaigns/run_buttons/#{self.status}",
                                locals: { campaign: self, button_text: I18n.t("campaigns.view.run.#{status}") }
    broadcast_replace target: dom_id(self, "status"), partial: "campaigns/status", locals: { campaign: self }
  }

  enum status: {
    initializing: 0,
    created: 1,
    running: 2,
    finished: 3,
    creating_pipelines_failed: 4
  }

  validates :name, presence: true,
            uniqueness: { scope: :cohort_id, case_sensitive: true },
            format: { with: /\A[a-zA-Z0-9_]+\z/,
                      message: I18n.t("campaigns.errors.name") }
  validates_each :cohort, on: :create do |record, attr, cohort|
    record.errors.add(attr, I18n.t("campaigns.errors.cohort_doesnt_exist")) unless cohort
    record.errors.add(attr, I18n.t("campaigns.errors.empty_cohort")) unless cohort&.patients.present?
  end

  validates_each :flow, on: :create do |record, attr, flow|
    record.errors.add(attr, I18n.t("campaigns.errors.cohort_doesnt_exist")) unless Flow.find_by(id: flow)
  end

  def ready_pipelines_ratio
    (((pipelines.size.to_f + failed_pipelines) / desired_pipelines.to_f) * 100).to_i
  end

  def set_status!
    update(status: Campaign::StatusCalculator.new(self).calculate)
  end

  def result_error
    return nil if failed_pipelines == 0
    return I18n.t("campaigns.view.pipeline_loader.partial_error",
                  number: failed_pipelines, all: desired_pipelines) if pipelines.size != desired_pipelines
    I18n.t("campaigns.view.pipeline_loader.full_error",
           ratio: "#{failed_pipelines}/#{cohort.patients.size}",
           percentage: Percentage.new(failed_pipelines, cohort.patients.size).to_i)
  end

  def add_failed
    increment!(:failed_pipelines)
    set_status!
  end

  def pipeline_status_changed
    update_frontend
    set_status!
  end

  def pipeline_created(pipeline)
    broadcast_append target: dom_id(self, "patients"), partial: "campaigns/patient_table/pipeline_row",
                     locals: { pipeline: } if Pipeline.where(id: pipeline.id).exists?
    broadcast_update target: "run_button", partial: "campaigns/run_buttons/#{status}",
                     locals: { campaign: self, button_text: I18n.t("campaigns.view.run.#{status}") } unless pipelines_ready?
    update_progress_bar_card
  end

  private
    def pipelines_ready?
      pipelines.size == created_pipelines_size
    end

    def created_pipelines_size
      desired_pipelines - failed_pipelines
    end

    def update_frontend
      update_progress_bar_card
      update_progress_bar
    end

    def update_progress_bar
      broadcast_update target: "progress_bar_campaign_#{id}", partial: "progress_bar/campaign",
                       locals: ProgressBar::Campaign.new(campaign: self).data
    end

    def update_progress_bar_card
      broadcast_update target: "progress_bar_campaign_card_#{id}", partial: "campaigns/campaign_progress_bar_card",
                       locals: { campaign: self }
    end
end
