# frozen_string_literal: true

class Computation < ApplicationRecord
  include ParameterValues
  include DataFiles

  attr_accessor :internal_errors

  belongs_to :user
  belongs_to :pipeline, touch: true
  belongs_to :step

  has_one_attached :stdout
  has_one_attached :stderr

  validates :status,
            inclusion: { in: %w[created new queued running error finished aborted] }
  validate do
    errors.merge!(internal_errors) if internal_errors
  end

  scope :active, -> { where(status: %w[new queued running]) }
  scope :submitted, -> { where(status: %w[queued running]) }
  scope :unsubmitted, -> { where(status: %w[created new]) }
  scope :created, -> { where(status: "created") }
  scope :not_finished, -> { where(status: %w[created new queued running]) }
  scope :rimrock, -> { where(type: "RimrockComputation") }
  scope :submitted_rimrock, -> { submitted.rimrock }
  scope :for_patient_status, ->(status) { where(pipeline_step: status) }

  delegate :mode, :manual?, :automatic?, to: :pipeline
  delegate :name, :site, to: :step

  def active?
    %w[new queued running].include? status
  end

  def finished?
    %w[error finished aborted].include? status
  end

  def rimrock?
    type == "RimrockComputation"
  end

  def run
    site.run(self)
    pipeline.computation_status_changed
  end

  def abort!
    site.abort(self)
  end

  def runnable?
    !pipeline.archived? && step.input_present_for?(pipeline)
  end

  def configured?
    true
  end

  def success?
    status == "finished"
  end

  def error?
    status == "error"
  end

  def created?
    status == "created"
  end

  def computed_status
    if success?
      :success
    elsif error?
      :error
    elsif active?
      :running
    else
      :waiting
    end
  end

  def site_host
    site.host
  end

  def fetch_logs
    stdout.attach(io: site.fetch_logs(stdout_path, user), filename: "slurm.out")
    stderr.attach(io: site.fetch_logs(stderr_path, user), filename: "slurm.err")
  end

  def belongs_to_campaign?
    !pipeline.campaign.nil?
  end
end
