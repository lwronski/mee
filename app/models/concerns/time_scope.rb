# frozen_string_literal: true

module TimeScope
  extend ActiveSupport::Concern

  included do
    validates :start_at, presence: true
    validates :end_at, presence: true
    validate :start_at_before_end_at

    scope :active,
          -> { where("start_at <= :now AND end_at >= :now", now: Date.current) }
  end

  def active?
    now = Date.current
    start_at <= now && end_at >= now
  end

  def future?
    now = Date.current
    start_at >= now && end_at >= now
  end

  private
    def start_at_before_end_at
      if start_at && end_at && start_at > end_at
        errors.add(:start_at, "can't be greater than end at date")
        errors.add(:end_at, "can't be less than start at date")
      end
    end
end
