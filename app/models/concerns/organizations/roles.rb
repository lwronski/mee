# frozen_string_literal: true

module Organizations
  module Roles
    extend ActiveSupport::Concern

    ROLES = [:admin, :supervisor]

    included do
      include RoleModel
      roles ROLES
    end

    class_methods do
      def admins
        where(any_role(:admin))
      end

      def any_role(*roles)
        (arel_table[:roles_mask] & mask_for(roles)).gt(0)
      end
    end
  end
end
