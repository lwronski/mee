# frozen_string_literal: true

module OrganizationUnit
  extend ActiveSupport::Concern

  included do
    belongs_to :organization, default: -> { Current.organization }
  end
end
