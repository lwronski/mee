# frozen_string_literal: true

module Runnable
  extend ActiveSupport::Concern

  def execute_data_sync(user)
    DataFileSynchronizer.new(self, user).call
  end
end
