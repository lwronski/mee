# frozen_string_literal: true

module Errorable
  extend ActiveSupport::Concern

  included do
    after_save :clear_persistent_errors
    has_many :persistent_errors, as: :errorable, dependent: :destroy
  end

  def last_persistent_error_for(key:, child:)
    if child
      persistent_errors.where(key:, child:).last
    else
      persistent_errors.where(key:).last
    end
  end

  def load_persistent_errors
    persistent_errors.each do |error|
      if error.child.present?
        instance_eval(error.child).errors.add(error.key, error.message)
      else
        errors.add(error.key, error.message)
      end
    end
  end

  def clear_persistent_errors
    persistent_errors.destroy_all
  end
end
