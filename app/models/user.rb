# frozen_string_literal: true

class User < ApplicationRecord
  serialize :proxy, Proxy

  include CheckExistenceConcern
  include User::Account
  include User::Plgrid
  include User::Jwt

  include User::Organizations
  include User::Pipelines
  include User::Flows
  include User::Steps
end
