# frozen_string_literal: true

require "resolv"

class RackProxy < Rack::Proxy
  def initialize(destination:, auth_key: nil)
    super(backend: destination)
    @auth_key = auth_key
    @destination = destination
  end

  def rewrite_env(env)
    env.tap do |e|
      e[@auth_key] = auth if @auth_key && auth
      e["PATH_INFO"] = path(e)

      # Remove not needed headers
      e.delete("HTTP_X_FORWARDED_PROTO")
      e.delete("HTTP_X_REAL_IP")
      e.delete("HTTP_COOKIE")
      e.delete("HTTP_REFERER")
      e.delete("HTTP_HOST")
    end
  end

  private
    # Overide if path should be modified.
    def path(env)
      env["PATH_INFO"]
    end

    def auth
      nil
    end
end
