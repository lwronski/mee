# frozen_string_literal: true

module Computation::ParameterValues
  extend ActiveSupport::Concern

  included do
    has_many :parameter_values, autosave: true, dependent: :destroy
    validates_associated :parameter_values
    accepts_nested_attributes_for :parameter_values, allow_destroy: true
  end

  def form_parameters_values
    existing = parameter_values.index_by(&:key)
    parameters.map { |p| existing[p.key] || p.value_class.new(parameter: p)  }
  end

  def parameter_values_attributes=(attrs)
    # Make sure that we are creating all step parameters
    parameters.each do |parameter|
      attrs[parameter.key] ||= {} unless attrs[parameter.key]
    end

    # Add parameter reference and parameter value class (as type field)
    attrs = attrs.filter_map do |key, v|
      parameter = parameter_by_key[key]
      parameter_value = parameter_value_for(key)
      if parameter
        begin
          v[:parameter] = parameter
          v[:type] = parameter.value_class
          v[:id] = parameter_value.id if parameter_value
          [key, v]
        rescue NameError
        end
      end
    end.to_h

    # Remove parameter values which is not present in the step
    parameter_values.each do |pv|
      attrs[pv.key] = { id: pv.id, _destroy: "1" } unless pv.parameter
    end

    super(attrs)
  end

  def parameter_value_for(key)
    parameter_values.find { |p| p.key == key }
  end

  private
    def parameters
      @parameters ||= step.parameters + data_file_parameters
    end

    def parameter_by_key
      @parameter_by_key ||= parameters.index_by(&:key)
    end

    def data_file_parameters
      step.required_file_types.filter_map do |dft|
        data_files = pipeline.data_files(dft)
        if data_files.size > 1
          Parameter::RequiredDataFile.new(data_file_type: dft, data_files:)
        end
      end
    end
end
