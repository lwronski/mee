# frozen_string_literal: true

class Campaign::StatusCalculator
  def self.STATUSES
    [:initializing, :created, :running, :finished, :creating_pipelines_failed]
  end

  def initialize(campaign)
    @campaign = campaign
  end

  def calculate
    if no_pipelines_created?
      :creating_pipelines_failed
    elsif all_pipelines_finished?(statuses)
      :finished
    elsif statuses["running"] > 0
      :running
    elsif pipelines_ready?
      :created
    else
      :initializing
    end
  end

  private
    def statuses
      @statuses ||= @campaign.pipelines.group_by(&:status).transform_values(&:size).tap do |statuses|
        statuses.default = 0
      end
    end

    def all_pipelines_finished?(statuses)
      statuses["success"] + statuses["error"] == @campaign.desired_pipelines
    end

    def no_pipelines_created?
      @campaign.failed_pipelines == @campaign.desired_pipelines
    end

    def pipelines_ready?
      @campaign.pipelines.size == @campaign.desired_pipelines - @campaign.failed_pipelines
    end
end
