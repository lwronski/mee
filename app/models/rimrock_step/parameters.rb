# frozen_string_literal: true

module RimrockStep::Parameters
  TAG_OR_BRANCH = "tag-or-branch"
  GRANT = "grant"

  extend ActiveSupport::Concern

  included do
    before_validation :ensure_model_version_defined
    before_validation :ensure_grant_defined
  end

  private
    def ensure_model_version_defined
      parameters.find { |p| p.key == TAG_OR_BRANCH } ||
        parameters.build(key: TAG_OR_BRANCH,
                         name: "Model version",
                         type: Parameter::ModelVersion)
    end

    def ensure_grant_defined
      parameters.find { |p| p.key == GRANT } ||
          parameters.build(key: GRANT,
                           name: "Grant",
                           type: Parameter::Grant)
    end
end
