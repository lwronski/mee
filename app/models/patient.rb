# frozen_string_literal: true

require "friendly_id_error_mover"

class Patient < ApplicationRecord
  include Runnable, OrganizationUnit
  extend FriendlyId
  friendly_id :case_number, use: [:scoped, FriendlyIdErrorMover], scope: :organization

  has_many :data_files, dependent: :destroy, as: :fileable
  has_many :pipelines,
             -> { order(iid: :asc) },
             inverse_of: "runnable",
             dependent: :destroy,
             as: :runnable
  has_many :cohort_patients, dependent: :destroy

  validates :case_number, presence: true, uniqueness: { scope: :organization_id, case_sensitive: false }

  default_scope { order("case_number asc") }

  def to_param
    slug
  end

  def inputs_dir
    File.join(working_dir, "inputs", "/")
  end

  def pipelines_dir
    File.join(working_dir, "pipelines", "/")
  end

  def working_dir
    File.join(organization.working_dir, "patients", slug, "/")
  end

  def inputs
    data_files.where(input_of: nil, output_of: nil)
  end

  def status
    pipelines.last&.status
  end
end
