# frozen_string_literal: true

class RimrockStep < Step
  include Git
  include Parameters

  belongs_to :grant_type

  def computation_class
    RimrockComputation
  end
end
