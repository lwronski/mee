# frozen_string_literal: true

class Storage::Config::Plgrid < Storage::Config
  attribute :host, :string
  attribute :host_key, :string
  attribute :path, :string

  with_options presence: true do
    validates :host
    validates :host_key
    validates :path
  end

  def storage_for(user)
    Storage::Plgrid.new(user:, host:, host_key:)
  end
end
