# frozen_string_literal: true

class Storage::Config::S3 < Storage::Config
  attribute :endpoint, :string
  attribute :bucket, :string
  attribute :secret_key, :string
  attribute :access_key, :string

  with_options presence: true do
    validates :endpoint
    validates :bucket
    validates :secret_key
    validates :access_key
  end

  validate :endpoint_url

  def storage_for(_user)
    Storage::S3.new(endpoint:, bucket:, secret_key:, access_key:, region: "PL")
  end

  private
    def endpoint_url
      if endpoint.present? && !valid_url?(endpoint)
        errors.add(:endpoint, "is not a valid URL")
      end
    end

    def valid_url?(uri)
      uri = URI.parse(uri)

      uri.host.present?
    rescue URI::InvalidURIError
      false
    end
end
