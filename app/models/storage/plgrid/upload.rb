# frozen_string_literal: true

class Storage::Plgrid::Upload < RackProxy
  def initialize(proxy, path)
    super(destination: "https://data.plgrid.pl", auth_key: "HTTP_PROXY")

    @proxy = proxy
    @path = path
  end

  def path(_env)
    @path
  end

  private
    def auth
      @proxy
    end
end
