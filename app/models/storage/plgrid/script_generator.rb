# frozen_string_literal: true

class Storage::Plgrid::ScriptGenerator
  def stage_in(path:, target:)
    "cp #{path} #{target}"
  end

  def stage_out(path:, target:)
    <<~COMMAND
    if [[ $(find . -name "#{path}" 2> /dev/null | wc -l) -eq 0 ]]
    then
      echo "Cannot stage out any #{path} file, because no file matched" 1>&2
      exit 1
    fi
    cp #{path} #{target}
    COMMAND
  end
end
