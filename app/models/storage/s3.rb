# frozen_string_literal: true

class Storage::S3
  delegate :stage_in, :stage_out, to: :script_generator

  def initialize(endpoint:, bucket:, secret_key:, access_key:, region:)
    @client = Aws::S3::Resource.new(access_key_id: access_key,
                                    secret_access_key: secret_key,
                                    endpoint:, region:)

    @bucket = @client.bucket(bucket)
  end

  def list(path)
    @bucket.objects(prefix: path).map { |o| record(o) }
  end

  def mkdir(*paths)
    true
  end

  def remove(path)
    @bucket.objects(prefix: path).batch_delete!
    true
  end

  def get_proxy(path)
    object_for(path).presigned_url(:get)
  end

  def upload_proxy(path)
    Storage::S3::Upload.new(@bucket, path)
  end

  def copy(source_path, destination_path)
    object_for(source_path).copy_to destination_path
  end

  def move(source_path, destination_path)
    object_for(source_path).move_to destination_path
  end

  def require_proxy?
    false
  end

  def valid?
    @bucket.load.present?
  rescue StandardError
    false
  end

  private
    def object_for(path)
      @bucket.object path
    end

    def script_generator
      @script_generator ||= Storage::S3::ScriptGenerator.new(bucket: @bucket)
    end

    def record(file_data)
      {
        path: file_data.key,
        size: file_data.size,
        modification_date: file_data.last_modified
      }
    end
end
