# frozen_string_literal: true

class Storage::Plgrid
  delegate :stage_in, :stage_out, to: :script_generator

  def initialize(user:, host:, host_key:)
    @proxy = user.proxy
    @runner = Rimrock::Runner.new(user, host:)
    @host_key = host_key
  end

  def list(path)
    body = @runner.call(command: list_cmd(path))

    JSON.parse("[#{body['standard_output'].delete_suffix(',')}]").
      map { |file_data| record(file_data) }
  end

  def mkdir(*paths)
    @runner.call(command: mkdir_cmd(paths))
    true
  rescue StorageError => e
    Rails.logger.warn(e)
    false
  end

  def remove(*paths)
    @runner.call(command: remove_cmd(paths))
    true
  rescue StorageError => e
    Rails.logger.warn(e)
    false
  end

  def get_proxy(path)
    Storage::Plgrid::Get.new(@proxy.encode, "/download/#{@host_key}/#{path}")
  end

  def upload_proxy(path)
    Storage::Plgrid::Upload.new(@proxy.encode, "/upload/#{@host_key}/#{path}")
  end

  def copy(source_path, destination_path)
    @runner.call(command: copy_cmd(source_path, destination_path))
    true
  rescue StorageError => e
    Rails.logger.warn(e)
    false
  end

  def move(source_path, destination_path)
    @runner.call(command: move_cmd(source_path, destination_path))
    true
  rescue StorageError => e
    Rails.logger.warn(e)
    false
  end

  def require_proxy?
    true
  end

  def valid?
    @proxy.valid?
  end

  private
    def record(file_data)
      {
        path: file_data["path"],
        size: file_data["size"],
        modification_date: Time.zone.parse(file_data["modification_date"])
      }
    end

    def list_cmd(path)
      "find #{path} -type f -exec stat -c "\
        "'{\"size\": \"%s\", \"path\": \"%n\", \"modification_date\": \"%y\"},' {} \\;"
    end

    def mkdir_cmd(paths)
      paths_str = paths.join(" ")
      "mkdir -p #{paths_str}; chmod -R g+w #{paths_str}"
    end

    def remove_cmd(paths)
      "rm -rf #{paths.join(' ')}"
    end

    def copy_cmd(source_path, destination_path)
      "cp \"#{source_path}\" \"#{destination_path}\"; chmod g+w #{destination_path}"
    end

    def move_cmd(source_path, destination_path)
      "mv \"#{source_path}\" \"#{destination_path}\""
    end

    def script_generator
      @script_generator ||= Storage::Plgrid::ScriptGenerator.new
    end
end
