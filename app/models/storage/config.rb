# frozen_string_literal: true

class Storage::Config
  extend ActiveModel::Callbacks
  define_model_callbacks :validate

  include ActiveModel::API
  include ActiveModel::Attributes
  include ActiveModel::Dirty

  after_validate :test_storage_connection, if: :fields_changed_and_valid?

  def type = self.class.type
  def path = ""

  class << self
    def default
      Storage::Config::Plgrid.new
    end

    def load(hsh)
      if hsh.present?
        attrs = ActiveSupport::HashWithIndifferentAccess.new(hsh)
        clazz = by_type(attrs.delete("type"))
        clazz&.new(attrs.slice(*clazz.attribute_names)).tap { |o| o.clear_changes_information }
      end
    end

    def dump(object)
      if object
        ActiveSupport::HashWithIndifferentAccess
          .new(object.attributes.merge({ type: object.type }))
          .compact
      end
    end

    def types
      [Storage::Config::Plgrid, Storage::Config::S3]
    end

    def type
      model_name.element
    end

    def by_type(type)
      types.find { |t| t.type == type }
    end
  end

  private
    def fields_changed_and_valid?
      changed? && errors.blank?
    end

    def test_storage_connection
      unless storage_for(Current.user).valid?
        errors.add(:type, I18n.t("admin.organizations.update.storage.configuration_error"))
        false
      end
      true
    end
end
