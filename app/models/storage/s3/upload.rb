# frozen_string_literal: true

require "stringio"

class Storage::S3::Upload
  def initialize(bucket, prefix)
    @bucket = bucket
    @prefix = prefix
  end

  def upload(io)
    if io.size < MULTIPART_UPLOAD_THRESHOLD
      upload_with_single_part io
    else
      upload_with_multipart io
    end
  end

  private
    MAXIMUM_UPLOAD_PARTS_COUNT = 10000
    MINIMUM_UPLOAD_PART_SIZE   = 5.megabytes
    MULTIPART_UPLOAD_THRESHOLD = 100.megabytes

    def object_for(io)
      @bucket.object key(io)
    end

    def key(io)
      "#{@prefix}/#{io.original_filename}"
    end

    def upload_with_single_part(io)
      object_for(io).put(body: io)
    end

    def upload_with_multipart(io)
      part_size = [ io.size.fdiv(MAXIMUM_UPLOAD_PARTS_COUNT).ceil, MINIMUM_UPLOAD_PART_SIZE ].max

      object_for(io).upload_stream(part_size:) do |out|
        IO.copy_stream(io, out)
      end
    end
end
