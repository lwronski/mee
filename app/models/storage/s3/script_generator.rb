# frozen_string_literal: true

class Storage::S3::ScriptGenerator
  def initialize(bucket:)
    @bucket = bucket
  end

  def stage_out(path:, target:)
    <<~COMMAND
      for file in #{path} ; do
        if [[ -f "$file" ]]; then
          curl -T ${file} "#{presigned_url(:put, target)}"
        fi
      done
    COMMAND
  end

  def stage_in(path:, target:)
    <<~COMMAND
      curl -o "#{target}" "#{presigned_url(:get, path)}"
    COMMAND
  end

  private
    LINK_EXPIRATION_TIME = 24.hours.seconds.to_i

    def presigned_url(method, path)
      @bucket.object(path).presigned_url(method, expires_in: LINK_EXPIRATION_TIME)
    end
end
