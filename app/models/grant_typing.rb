# frozen_string_literal: true

class GrantTyping < ApplicationRecord
  belongs_to :grant
  belongs_to :grant_type

  validates :grant_type, uniqueness: { scope: :grant }
  validates :grant_type, :grant, presence: true
end
