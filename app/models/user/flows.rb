# frozen_string_literal: true

module User::Flows
  extend ActiveSupport::Concern

  included do
    has_many :flows, dependent: :nullify
  end
end
