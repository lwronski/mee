# frozen_string_literal: true

module User::Plgrid
  extend ActiveSupport::Concern

  class_methods do
    def from_plgrid_omniauth(auth, organization)
      find_or_initialize_by(plgrid_login: auth.info.nickname).tap do |user|
        set_new_user_attrs(auth, user)

        user.proxy = User.compose_proxy(auth.info)
        user.proxy_expired_notification_time = nil
        user.memberships.find_or_initialize_by(organization:) if organization
      end
    end

    def set_new_user_attrs(auth, user)
      user.email = auth.info.email
      name_elements = auth.info.name.split(" ")
      user.first_name = name_elements[0]
      user.last_name = name_elements[1..-1].join(" ")
      user.terms = true
    end

    def compose_proxy(info)
      return unless info.proxy && info.proxyPrivKey && info.userCert

      Proxy.for(info.proxy + info.proxyPrivKey + info.userCert)
    end
  end

  def manage_membership(info, organization)
    if team = organization&.plgrid_team_id
      membership = self.memberships.detect { |membership| membership.organization == organization }
      state = extract_teams_ids(info).include?(team) ? :approved : :blocked
      membership.update(state:)
    end
  end

  private
    def extract_teams_ids(info)
      info.userteams.split(",").map do |team|
        /\A(?<id>.*)\(.*\)\z/ =~ team
        id
      end
    end
end
