# frozen_string_literal: true

module User::Pipelines
  extend ActiveSupport::Concern

  included do
    has_many :computations, dependent: :nullify
  end

  class_methods do
    def with_submitted_computations(computation_type)
      condition = <<~SQL
        id IN (SELECT DISTINCT(user_id) FROM computations
                WHERE type = ? AND status IN ('queued', 'running'))
      SQL
      where(condition, computation_type)
    end
  end
end
