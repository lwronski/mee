# frozen_string_literal: true

module User::Jwt
  extend ActiveSupport::Concern

  class_methods do
    def from_token(token)
      find_by(email: JwtToken.decode(token)[0]["email"])
    end
  end

  def token(expiration_time_in_seconds = nil)
    JwtToken.new(self).generate(expiration_time_in_seconds)
  end
end
