# frozen_string_literal: true

class RimrockComputation < Computation
  validates :script, presence: true, unless: :created?
  validates :tag_or_branch, presence: true, unless: :created?

  delegate :repository, :file, :host, :download_key, to: :step
  delegate :gitlab, to: :organization

  def initialize(attrs = {})
    if attrs
      step = attrs[:step]
      pipeline = attrs[:pipeline]

      attrs[:user] ||= pipeline&.user
      attrs[:pipeline_step] = step&.name
    end

    super(attrs)
  end

  def configured?
    super && tag_or_branch.present?
  end

  def tag_or_branch
    tag_or_branch_parameter_value&.value || super
  end

  def tag_or_branch_parameter_value
    parameter_value_for(RimrockStep::Parameters::TAG_OR_BRANCH)
  end
end
