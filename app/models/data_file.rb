# frozen_string_literal: true

class DataFile < ApplicationRecord
  belongs_to :fileable, polymorphic: true, touch: true
  belongs_to :output_of,
             optional: true,
             inverse_of: :outputs,
             class_name: "Pipeline"

  belongs_to :input_of,
             optional: true,
             inverse_of: :inputs,
             class_name: "Pipeline"

  belongs_to :data_type,
             optional: true,
             inverse_of: :data_files,
             class_name: "DataFileType"

  validates :name, :fileable, presence: true

  delegate :organization, to: :fileable

  def path
    File.join(root_path, name)
  end

  def comparable?
    data_type&.viewer.present?
  end

  def similar?(other_data_file)
    name == other_data_file.name
  end

  def extension
    File.extname(name)
  end

  private
    def root_path
      output_of_id && output_of&.outputs_dir ||
        input_of_id && input_of&.inputs_dir ||
        fileable.inputs_dir
    end
end
