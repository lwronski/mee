# frozen_string_literal: true

require "friendly_id_error_mover"

class Organization < ApplicationRecord
  include Runnable, Storage
  extend FriendlyId
  friendly_id :name, use: [:slugged, FriendlyIdErrorMover]

  include Licenses
  include Steps
  include Git

  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships
  has_many :supervisors,
           -> { where(Membership.any_role(:admin, :supervisor)) },
           through: :memberships,
           source: "user"
  has_many :approved_users,
           -> { where(memberships: { state: :approved }) },
           through: :memberships,
           source: "user"

  has_many :patients, dependent: :destroy
  has_many :flows, dependent: :destroy
  has_many :grants, dependent: :destroy
  has_many :data_file_types, dependent: :destroy
  has_many :pipelines, as: :runnable
  has_many :data_files, as: :fileable
  has_many :artifacts
  has_many :cohorts, dependent: :destroy

  has_one_attached :logo

  validates :name, presence: true
  validates :plgrid_team_id, presence: true

  include Errorable

  def copy_slug_error_to_name
    if errors[:friendly_id].present?
      errors.add(:name, errors[:friendly_id].first)
    end
  end

  def organization
    self
  end

  def organization_id
    id
  end
end
