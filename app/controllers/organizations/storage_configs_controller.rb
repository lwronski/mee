# frozen_string_literal: true

class Organizations::StorageConfigsController < RootApplicationController
  layout false

  def show
    authorize(Organization, :new?)

    type = params[:id]
    @config = Storage::Config.by_type(type)

    if @config
      render partial: "storage/configs/#{@config.type}",
             layout: false,
             locals: { storage_config: @config }
    else
      render partial: "storage/configs/not_found"
    end
  end
end
