# frozen_string_literal: true

class Admin::GrantsController < ApplicationController
  before_action :load_and_authorize, only: [:edit, :update, :destroy]

  def index
    authorize(Grant)
    @pagy, @grants = pagy(policy_scope(Grant).includes(:grant_types))
  end

  def new
    @grant = Grant.new
    authorize(@grant)
  end

  def create
    @grant = Grant.new(permitted_attributes(Grant))
    authorize(@grant)

    if @grant.save
      redirect_to(admin_grants_path)
    else
      render(:new)
    end
  end

  def edit
  end

  def update
    if @grant.update(permitted_attributes(@grant))
      redirect_to(admin_grants_path)
    else
      render(:edit, status: :bad_request)
    end
  end

  def destroy
    @grant.destroy
    redirect_to(admin_grants_path)
  end

  private
    def load_and_authorize
      @grant = Current.organization.grants.find(params[:id])
      authorize(@grant)
    end
end
