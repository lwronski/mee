# frozen_string_literal: true

module Admin
  class Organizations::StorageConfigsController < ApplicationController
    before_action :find_and_authorize
    layout false

    def show
      type = params[:id]
      @config = Storage::Config.by_type(type) unless @config.type == type

      if @config
        render partial: "storage/configs/#{@config.type}",
               layout: false,
               locals: { storage_config: @config }
      else
        render partial: "storage/configs/not_found"
      end
    end

    private
      def find_and_authorize
        @config = Current.organization.storage_config
        authorize(Current.organization, :update?)
      end
  end
end
