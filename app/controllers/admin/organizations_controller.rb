# frozen_string_literal: true

module Admin
  class OrganizationsController < ApplicationController
    include Organization::Params
    before_action { @organization = Current.organization }

    def show
      @organization.load_persistent_errors
      authorize(@organization)
    end

    def update
      authorize(@organization)

      if @organization.update(organization_params(@organization))
        flash[:notice] = I18n.t("admin.organizations.show.updated")
        redirect_to(admin_organization_path)
      else
        render(:show, status: :unprocessable_entity)
      end
    end

    def destroy
      authorize(@organization)
      @organization.destroy
      redirect_to(root_url(subdomain: false), allow_other_host: true)
    end
  end
end
