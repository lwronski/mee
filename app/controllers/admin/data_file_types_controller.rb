# frozen_string_literal: true

class Admin::DataFileTypesController < ApplicationController
  before_action :load_and_authorize, only: [:edit, :update, :destroy]

  def index
    authorize(DataFileType)
    @pagy, @data_file_types = pagy(policy_scope(DataFileType))
  end

  def new
    @data_file_type = DataFileType.new
    authorize(@data_file_type)
  end

  def create
    @data_file_type = DataFileType.new(permitted_attributes(DataFileType).
                                       merge(organization: Current.organization))
    authorize(@data_file_type)

    if @data_file_type.save
      redirect_to(admin_data_file_types_path)
    else
      render(:new)
    end
  end

  def edit
  end

  def update
    if @data_file_type.update(permitted_attributes(@data_file_type))
      redirect_to(admin_data_file_types_path)
    else
      render(:edit, status: :bad_request)
    end
  end

  def destroy
    @data_file_type.destroy
    redirect_to(admin_data_file_types_path)
  end

  private
    def load_and_authorize
      @data_file_type = Current.organization.data_file_types.find(params[:id])
      authorize(@data_file_type)
    end
end
