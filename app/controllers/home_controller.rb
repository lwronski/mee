# frozen_string_literal: true

class HomeController < RootApplicationController
  before_action :load_organizations

  def index; end

  def not_found
    flash.now["error"] = I18n.t("organization.not_found")

    render :index
  end

  def not_authorized
    flash.now["error"] = I18n.t("organization.not_authorized")

    render :index
  end

  private
    def load_organizations
      @organizations = Organization.all
    end
end
