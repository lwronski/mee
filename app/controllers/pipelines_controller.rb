# frozen_string_literal: true

# NOTE; This controller's actions are also exposed via API - remember to update both sources
class PipelinesController < ApplicationController
  include PipelineParams
  before_action :load_runnable, only: [:index, :new, :create]
  before_action :find_and_authorize, only: [:show, :edit, :update, :destroy]

  def index
    authorize(Pipeline)

    redirect_to(patient_path(@runnable)) if @runnable.class == Patient

    @pipelines = @runnable.pipelines.includes(:user, :flow, computations: [:step, :pipeline])
    @pagy, @pipelines = pagy(filter(@pipelines))
  end

  def new
    @flows = policy_scope(Flow)
    @pipeline = ::Pipelines::Build.new(Current.user, @runnable,
                                       { flow: @flows.first, mode: params[:mode] }, {}).call

    authorize(@pipeline)
  end

  def create
    @pipeline = ::Pipelines::Build.new(Current.user, @runnable,
                                       permitted_attributes(Pipeline),
                                       steps_parameters_values).call
    remove_blank_parameters_values(@pipeline)

    authorize(@pipeline)

    if @pipeline.valid?
      ::Pipelines::Create.new(Current.user, @pipeline).call

      if @pipeline.errors.empty?
        @runnable.execute_data_sync(Current.user)
        ::Pipelines::StartRunnableJob.perform_later(@pipeline) if @pipeline.automatic?
        redirect_to(pipeline_path(@pipeline))
        return
      end
    end

    @flows = policy_scope(Flow)
    render(:new, status: :unprocessable_entity)
  end

  def show
    computation = @pipeline.computations.includes(:step).first

    return unless computation

    redirect_to computation_path(computation)
  end

  def edit; end

  def update
    if @pipeline.update(permitted_attributes(@pipeline))
      redirect_to(pipeline_path(@pipeline))
    else
      render(:edit)
    end
  end

  def destroy
    if @pipeline.destroy
      redirect_to helpers.runnable_path(@pipeline.runnable),
                  notice: I18n.t("pipelines.destroy.success", name: @pipeline.name),
                  status: :see_other
    else
      render :show,
             notice: I18n.t("pipelines.destroy.failure",
                            name: @pipeline.name)
    end
  end

  private
    def create_pipeline
      pipeline = Pipeline.new(permitted_attributes(Pipeline).merge(owners))
      authorize(pipeline)

      ::Pipelines::Create.new(Current.user, pipeline, steps_parameters_values).call
    end

    def steps_parameters_values
      params[:pipeline][:mode] == "automatic" ? permitted_step_attributes(params) : {}
    end

    def owners
      { runnable: @runnable, user: Current.user }
    end

    def load_runnable
      @runnable = params[:patient_id].nil? ? Current.organization : policy_scope(Patient).find_by!(slug: params[:patient_id])
      @patient = @runnable
    end

    def find_and_authorize
      @pipeline = Pipeline.find(params[:id])
      authorize(@pipeline)
    end

    def filter(scope)
      scope = scope.where("pipelines.name LIKE ?", "%#{params[:name].strip}%") if params[:name].present?
      scope = scope.where(user_id: params[:owner]) if params[:owner].present?
      scope = scope.where(status: params[:status]) if params[:status].present?

      scope
    end
end
