# frozen_string_literal: true

class Campaigns::RunsController < ApplicationController
  def create
    @campaign = Campaign.find(params[:campaign_id])
    authorize @campaign, :run?

    Campaigns::Run.new(@campaign).call

    redirect_to campaign_path(@campaign),
                notice: t("campaigns.view.run.success_flash")
  end
end
