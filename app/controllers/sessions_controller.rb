# frozen_string_literal: true

class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create
  skip_before_action :authenticate_user!, only: :create
  skip_before_action :authorize_membership!

  def create
    user = User.from_plgrid_omniauth(auth, Current.organization).tap { |u| u.save! }
    cookies.signed["user.id"] = user.id
    user.manage_membership(auth.info, Current.organization)
    start_computations(user)

    redirect_to root_path
  end

  def destroy
    Current.user&.update(proxy: nil)
    cookies.delete("user.id")

    redirect_to root_path
  end

  private
    def auth
      request.env["omniauth.auth"]
    end

    def start_computations(user)
      Pipeline.automatic.where(user:).
        each { |p| Pipelines::StartRunnableJob.perform_later(p) }
    end
end
