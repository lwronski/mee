# frozen_string_literal: true

class RootApplicationController < ActionController::Base
  include Authenticate
  include Pundit::Authorization
  include Sentryable
  include ErrorRescues

  def pundit_user
    Current.user
  end

  skip_before_action :authenticate_user!

  layout "root"
end
