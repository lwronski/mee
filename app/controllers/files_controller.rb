# frozen_string_literal: true

class FilesController < ApplicationController
  def show
    proxy = storage.get_proxy(path)
    if proxy.respond_to? :call
      proxy_response = ActionDispatch::Response.new(*proxy.call(request.env.dup))

      self.response_body = proxy_response.body
      self.status = proxy_response.status

      if proxy_response.headers["content-type"]
        response.set_header("Content-Type", proxy_response.headers["content-type"])
      end
    else
      redirect_to proxy.to_s, allow_other_host: true
    end
  end

  def create
    proxy = storage.upload_proxy(path)

    if proxy.respond_to? :upload
      proxy.upload(params[:file])
      create_new_datafile
      redirect_back fallback_location: root_path, notice: t(".success")
    else
      proxy_response = ActionDispatch::Response.new(*proxy.call(request.env.dup))

      if proxy_response.status == 200
        create_new_datafile
        redirect_back fallback_location: root_path, notice: t(".success")
      else
        redirect_back fallback_location: root_path, alert: t(".failure")
      end
    end
  end

  def destroy
    @data_file = DataFile.find(params[:id])
    if ::Storage::DestroyFileJob.perform_later(Current.user, @data_file)
      respond_to do |format|
        format.turbo_stream { flash.now[:notice] = t(".success") }
      end
    else
      redirect_back fallback_location: root_path, alert: t(".failure")
    end
  end

  private
    def path
      full_path(params[:id])
    end

    def storage
      @storage ||= Current.organization.storage_for(Current.user)
    end

    def create_new_datafile
      file_path = full_path(File.join(path.delete_prefix(full_path(Current.organization.storage_config.path)),
                            params[:file]&.original_filename))

      DataFiles::CreateJob.new.perform(Current.organization, [file_path])
    end

    def full_path(path)
      path[0] == "/" ? path : "/#{path}"
    end
end
