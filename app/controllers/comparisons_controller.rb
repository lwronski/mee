# frozen_string_literal: true

class ComparisonsController < ApplicationController
  before_action :check_pipelines, only: [:index]
  before_action :find_and_authorize, only: [:index]
  before_action :runnable, only: [:index]

  def index
    pipelines.reload

    @first = pipelines.first
    @second = pipelines.second

    @sources =
      @first.computations.select(&:revision).filter_map do |compared_comp|
        compare_to_comp =
          @second.computations.select(&:revision).
            detect { |c| c.pipeline_step == compared_comp.pipeline_step }

        [compared_comp, compare_to_comp] if compare_to_comp
      end


    @data = { compared: [], not_comparable: [] }
    @first.outputs.each do |compared|
      compare_to = @second.outputs.detect { |df| df.similar?(compared) }
      if compared.comparable? && compare_to.present?
        @data[:compared] << [compared, compare_to]
      elsif compare_to.present? && compared.data_type
        @data[:not_comparable] << compared.data_type.name
      end
    end
  end

  private
    def pipelines
      @pipelines ||= Pipeline.where(id: params[:pipeline_ids]).
                     includes(outputs: :data_type,
                              computations: [:step, :parameter_values])
    end

    def runnable
      @runnable ||= @pipelines.first.runnable
    end

    def find_and_authorize
      pipelines.each { |pipeline| authorize(pipeline, :compare?) }
    end

    def check_pipelines
      if pipelines.size != 2
        redirect_to helpers.runnable_path(@runnable), alert: I18n.t("comparisons.index.invalid")
      end
    end
end
