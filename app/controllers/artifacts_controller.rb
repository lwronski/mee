# frozen_string_literal: true

class ArtifactsController < ApplicationController
  before_action :load_and_authorize, except: [:index, :new, :create]

  def index
    authorize(Artifact)
    @pagy, @artifacts = pagy(Current.organization.artifacts)
  end

  def new
    authorize(Artifact)
    @artifact = Artifact.new(file_id: params[:file_id])
  end

  def create
    @artifact = Artifact.new(permitted_attributes(Artifact).merge(file_id: params[:file_id]))

    authorize(@artifact)

    if Artifacts::Create.new(Current.user, @artifact).call
      flash.now[:notice] = t(".success")
    else
      @artifact.name = permitted_attributes(Artifact)[:name]
      render :new, status: :unprocessable_entity
    end
  end

  def edit
  end

  def update
    if Artifacts::Update.new(Current.user, @artifact, permitted_attributes(@artifact)[:name]).call
      flash.now[:notice] = t(".success")
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if Storage::DestroyFile.new(Current.user, @artifact).call
      flash.now[:notice] = t(".success")
    else
      flash.now[:alert] = t(".failure", name: @artifact.name)
    end
  end

  private
    def load_and_authorize
      @artifact = Current.organization.artifacts.find(params[:id])
      authorize(@artifact)
    end

    def artifact_name(extension = nil)
      ActiveStorage::Filename.new("#{permitted_attributes(Artifact)[:name]}#{extension}").sanitized
    end
end
