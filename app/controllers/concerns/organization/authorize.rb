# frozen_string_literal: true

module Organization::Authorize
  extend ActiveSupport::Concern

  included do
    before_action :load_organization!
    before_action :authorize_membership!
  end

  def pundit_user
    UserContext.new(Current.user, Current.organization, Current.membership)
  end

  private
    def load_organization!
      Current.organization = subdomain? ? organization_candidates.first! : organization_candidates.first
    rescue ActiveRecord::RecordNotFound
      raise Organization::NotFoundError
    end

    def subdomain?
      RoutesConstraint::Subdomain.subdomain?(request)
    end

    def organization_candidates
      RoutesConstraint::Subdomain.organization_candidates(request)
    end

    def authorize_membership!
      raise Membership::NotFoundError unless Current.membership
      raise Membership::BlockedError if Current.membership.blocked?
      raise Membership::NewError unless Current.membership.approved?
    end
end
