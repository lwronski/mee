# frozen_string_literal: true

module Organization::Params
  extend ActiveSupport::Concern

  private
    def organization_params(organization = Organization.new)
      permitted_attributes = policy(organization).permitted_attributes +
        [
          git_config_attributes: permitted_git_config_attributes,
          storage_config_attributes: permitted_storage_config_attributes
        ]

      params.require(:organization).permit(permitted_attributes)
    end

    def permitted_git_config_attributes
      git_type = params.dig(:organization, :git_config_attributes, :git_type)
      git_config_class = GitConfig.by_type(git_type)

      policy(git_config_class).permitted_attributes if git_config_class
    end

    def permitted_storage_config_attributes
      type = params.dig(:organization, :storage_config_attributes, :type)
      clazz = Storage::Config.by_type(type)

      policy(clazz).permitted_attributes if clazz
    end
end
