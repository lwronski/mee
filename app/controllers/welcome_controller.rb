# frozen_string_literal: true

class WelcomeController < RootApplicationController
  include Organization::Authorize
  skip_before_action :authorize_membership!

  layout "welcome"

  def index
    redirect_to patients_path if Current.user
  end
end
