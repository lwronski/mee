# frozen_string_literal: true

class Computations::AbortsController < ApplicationController
  def create
    @computation = Computation.find(params[:computation_id])
    authorize(@computation, :abort?)
    @computation.abort!
    redirect_to computation_path(@computation)
  end
end
