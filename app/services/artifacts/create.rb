# frozen_string_literal: true

class Artifacts::Create
  def initialize(current_user, artifact)
    @current_user = current_user
    @artifact = artifact
  end

  def call
    Artifact.transaction { internal_call }
    @artifact.persisted?
  end

  private
    def internal_call
      if @artifact.save
        unless Current.organization.storage_for(@current_user).copy(@artifact.file.path, @artifact.path)
          @artifact.errors.
            add(:storage_error, I18n.t("artifacts.create.storage_failure"))

          raise ActiveRecord::Rollback
        end
      end
    end
end
