# frozen_string_literal: true

class Artifacts::Update
  def initialize(current_user, artifact, new_name)
    @current_user = current_user
    @artifact = artifact
    @organization = @artifact.organization
    @new_name = new_name
    @source_path = @artifact.path
    @destination_path = File.join(@organization.artifacts_dir, @new_name)
  end

  def call
    Artifact.transaction { internal_call }
    @artifact.errors.empty?
  end

  def internal_call
    if @artifact.update(name: @new_name)
      unless @organization.storage_for(@current_user).move(@source_path, @destination_path)
        @artifact.errors.
          add(:storage_error, I18n.t("artifacts.update.storage_failure", name: @artifact.name))

        raise ActiveRecord::Rollback
      end
    end
  end
end
