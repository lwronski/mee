# frozen_string_literal: true

class Step::Update
  def initialize(step, params)
    @step = step
    @params = params
  end

  def call
    @step.update(@params).tap do |saved|
      RimrockStep::ValidateRepositoryJob.perform_later(@step) if saved
    end
  end
end
