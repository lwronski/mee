# frozen_string_literal: true

class Step::Create
  def initialize(step)
    @step = step
  end

  def call
    @step.save.tap do |saved|
      RimrockStep::ValidateRepositoryJob.perform_later(@step) if saved
    end
  end
end
