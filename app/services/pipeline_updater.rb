# frozen_string_literal: true

class PipelineUpdater
  def initialize(computation)
    @computation = computation
  end

  def call
    data_sync
    start_runnable
  end

  private
    attr_reader :computation
    delegate :runnable, to: :pipeline
    delegate :user, to: :pipeline
    delegate :pipeline, to: :computation

    def data_sync
      runnable.execute_data_sync(user)
    end

    def start_runnable
      Pipelines::StartRunnableJob.perform_later(@computation.pipeline)
    end
end
