# frozen_string_literal: true

require "liquid"

class ScriptGenerator
  attr_reader :computation

  delegate :pipeline, :revision, to: :computation
  delegate :runnable, :user, :mode, to: :pipeline
  delegate :token, :email, to: :user
  delegate :slug, :organization, to: :runnable

  def initialize(computation, template, errors = ActiveModel::Errors.new(computation))
    @computation = computation
    @template = template
    @errors = errors
  end

  def call
    if @template
      parsed_template = Liquid::Template.parse(@template)

      parsed_template
        .render({ "token" => token, "email" => email, "case_number" => (runnable.try(:case_number)),
                  "revision" => revision, "grant_id" => grant_id, "mode" => mode,
                  "pipeline_identifier" => pipeline_identifier, "proxy" => user.proxy.to_s,
                  "campaign_name" => pipeline.campaign&.name, "cohort_name" => pipeline.campaign&.cohort&.name },
                registers: { pipeline:, storage:,
                             computation:, organization:,
                             errors: @errors })
        .gsub(/\r\n?/, "\n")
    end
  end

  def grant_id
    grant = computation.parameter_value_for(RimrockStep::Parameters::GRANT)&.value

    grant || @errors.add(:script, "active grant cannot be found")
  end

  def pipeline_identifier
    "#{slug}-#{pipeline.iid}"
  end

  private
    def storage
      organization.storage_for(user)
    end
end
