# frozen_string_literal: true

class Membership::Destroy < Membership::Base
  def call
    if self?
      :self
    elsif perform!
      :ok
    else
      :error
    end
  end

  private
    def perform!
      Computation.where(user: @membership.user).each(&:abort!)
      @membership.destroy
    end
end
