# frozen_string_literal: true

class Membership::Base
  attr_reader :membership

  def initialize(current_user, membership)
    @current_user = current_user
    @membership = membership
  end

  private
    def self?
      @current_user == @membership.user
    end
end
