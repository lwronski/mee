# frozen_string_literal: true

class Percentage
  def initialize(nominator, denominator)
    @value = nominator.to_f / denominator.to_f * 100

    freeze
  end

  def to_f
    @value
  end

  def to_i
    @value.to_i
  end
end
