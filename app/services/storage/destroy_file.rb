# frozen_string_literal: true

class Storage::DestroyFile
  def initialize(current_user, file_object)
    @current_user = current_user
    @file_object = file_object
    @organization = @file_object.organization
  end

  def call
    @file_object.class.transaction { internal_call }
    @file_object.destroyed?
  end

  private
    def internal_call
      if @file_object.destroy
        unless @organization.storage_for(@current_user).remove(@file_object.path)
          raise ActiveRecord::Rollback
        end
      end
    end
end
