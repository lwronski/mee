# frozen_string_literal: true

class Patients::Destroy < Patients::Base
  def call
    !super.persisted?
  end

  protected
    def internal_call
      @patient.destroy!
      remove(@patient.working_dir) || raise(ActiveRecord::Rollback)
    end
end
