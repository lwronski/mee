# frozen_string_literal: true

class Patients::Base
  def initialize(user, patient)
    @user = user
    @patient = patient
  end

  def call
    Patient.transaction { internal_call }
    @patient
  end

  private
    delegate :mkdir, :remove, to: :storage

    def storage
      @storage ||= organization.storage_for(@user)
    end

    def organization
      @organization ||= @patient.organization
    end
end
