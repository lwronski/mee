# frozen_string_literal: true

class Patients::Create < Patients::Base
  protected
    def internal_call
      if @patient.save
        unless mkdir(@patient.working_dir, @patient.inputs_dir, @patient.pipelines_dir)
          @patient.errors.
            add(:case_number,
                I18n.t("activerecord.errors.models.patient.create_dav403"))

          raise ActiveRecord::Rollback
        end
      end
    end
end
