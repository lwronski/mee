# frozen_string_literal: true

module DataFiles
  class Synchronize
    def initialize(runnable, remote_files)
      @runnable = runnable
      @remote_files = remote_files
    end

    def call
      sync_dir(@remote_files, @runnable.inputs_dir) if @runnable.class == Patient
      sync_pipelines(@runnable.pipelines.includes([:runnable]), @remote_files)
    end

    private
      def sync_dir(remote_files, prefix, input_pipeline: nil, output_pipeline: nil)
        validate_only_one_pipeline!(input_pipeline, output_pipeline)

        file_names = names(remote_files, prefix)

        file_names.each do |remote_name|
          sync_file(remote_name, input_pipeline:, output_pipeline:)
        end
        remove_obsolete_db_entries(file_names,
                                  input_pipeline:, output_pipeline:)
      end

      def sync_pipelines(pipelines, remote_files)
        pipelines.each do |pipeline|
          sync_dir(remote_files, pipeline.inputs_dir, input_pipeline: pipeline)
          sync_dir(remote_files, pipeline.outputs_dir, output_pipeline: pipeline)
        end
      end

      def validate_only_one_pipeline!(input_pipeline, output_pipeline)
        if input_pipeline && output_pipeline
          raise ArgumentError(
            "Arguments input_pipeline and output_pipeline should be mutually exclusive"
          )
        end
      end

      def recognize_data_type(name)
        data_file_types.detect { |dft| dft.match?(name) }
      end

      def data_file_types
        @data_file_types ||= DataFileType.where(organization: @runnable.organization)
      end

      def names(remote_files, prefix)
        remote_files.filter_map do |rf|
          rf[:path].split(prefix)[1] if rf[:path].split(prefix).size > 1
        end
      end

      def sync_file(remote_name, input_pipeline: nil, output_pipeline: nil)
        data_type = recognize_data_type(remote_name)
        unless current_names(input_pipeline, output_pipeline).include?(remote_name)
          create_db_entry(data_type, remote_name, input_pipeline, output_pipeline)
        end
      end

      def current_names(input_pipeline, output_pipeline)
        @runnable.data_files.where(input_of: input_pipeline,
                                  output_of: output_pipeline).pluck(:name)
      end

      def create_db_entry(data_type, remote_name, input_pipeline, output_pipeline)
        DataFile.create(name: remote_name, data_type:, fileable: @runnable,
                        input_of: input_pipeline, output_of: output_pipeline)
      end

      def remove_obsolete_db_entries(remote_names, input_pipeline: nil, output_pipeline: nil)
        @runnable.data_files.where(input_of: input_pipeline,
                                  output_of: output_pipeline).each do |data_file|
          next if remote_names.include? data_file.name

          data_file.destroy!
          pipeline = input_pipeline || output_pipeline
          Rails.logger.info(
            I18n.t("data_file_synchronizer.file_removed",
                  name: data_file.name, runnable: @runnable.slug, pipeline:)
          )
        end
      end
  end
end
