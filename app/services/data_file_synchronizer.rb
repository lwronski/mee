# frozen_string_literal: true

require "net/dav"

class DataFileSynchronizer
  def initialize(runnable, user)
    @organization = runnable&.organization
    @runnable = runnable

    @client = @organization&.storage_for(user)
    @user = user
  end

  def call
    if invalid_runnable?
      report_problem(:no_slug)
    elsif invalid_client?
      report_problem(:no_fs_client)
    else
      call_storage
    end
  end

  private
    def invalid_runnable?
      @runnable.nil? || @runnable.slug.blank?
    end

    def invalid_client?
      @client.blank?
    end

    # Contacts organization storage and updates the list of DataFiles
    # related to a patient.
    def call_storage
      remote_files = @client.list(@runnable.working_dir)

      DataFiles::Synchronize.new(@runnable, remote_files).call
    rescue StorageError => e
      Rails.logger.tagged(self.class.name) { Rails.logger.warn e.message }
    end

    def report_problem(problem, details = {})
      details.merge!(extra_details(details))

      # TODO: FIXME Add Raven Sentry notification; issue #32

      Rails.logger.tagged(self.class.name) do
        Rails.logger.warn I18n.t("data_file_synchronizer.#{problem}", **details)
        Rails.logger.info(details[:response].body) if details[:response]
      end
    end

    def extra_details(details = {})
      {
        runnable: @runnable.try(:slug),
        user: @user.try(:name),
        code: details[:response].try(:code)
      }
    end
end
