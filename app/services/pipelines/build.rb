# frozen_string_literal: true

class Pipelines::Build
  def initialize(user, runnable, runnable_params, parameters_values_params, campaign: nil)
    @user = user
    @runnable = runnable
    @runnable_params = runnable_params || {}
    @parameters_values_params = parameters_values_params || {}
    @campaign = campaign
  end

  def call
    params = @runnable_params.merge(user: @user, runnable: @runnable, campaign: @campaign)
    params[:mode] ||= :automatic

    pipeline = Pipeline.new(params)

    if pipeline.flow
      pipeline.steps.includes(:parameters).each do |step|
        pipeline.computations.build(
          step:,
          pipeline:,
          type: step.computation_class,
          parameter_values_attributes: step_params(step.slug)
        )
      end
    end

    pipeline
  end

  private
    def step_params(step_name)
      @parameters_values_params.fetch(step_name) { {} }.to_h
    end
end
