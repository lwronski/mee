# frozen_string_literal: true

class Pipelines::Base
  def initialize(user, pipeline)
    @pipeline = pipeline
    @user = user
  end

  def call
    Pipeline.transaction { internal_call }
    @pipeline
  end

  delegate :mkdir, :remove, to: :storage

  def storage
    @storage ||= organization.storage_for(@user)
  end

  def organization
    @pipeline.runnable.organization
  end
end
