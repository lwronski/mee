# frozen_string_literal: true

class Pipelines::Create < Pipelines::Base
  def initialize(user, pipeline)
    super(user, pipeline)
  end

  protected
    def internal_call
      if @pipeline.save
        unless mkdir(@pipeline.root_dir, @pipeline.inputs_dir, @pipeline.outputs_dir)
          @pipeline.errors.
            add(:name,
                I18n.t("activerecord.errors.models.pipeline.create_dav403"))

          raise ActiveRecord::Rollback
        end
      end
    end
end
