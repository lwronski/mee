# frozen_string_literal: true

class ComputationUpdater
  attr_reader :computation, :computations, :pipeline, :runnable

  def initialize(computation)
    @computation = computation
    @computations = Computation.
                    includes(:pipeline).
                    where(pipeline_id: computation.pipeline_id).
                    order(:created_at)
    @pipeline = computation.pipeline
    @runnable = pipeline.runnable
  end

  def call
    computations.each { |c| broadcast_to_computation(c) }
    broadcast_to_runnable(runnable)
  end

  private
    def broadcast_to_computation(to)
      ComputationChannel.broadcast_to(to,
                                      menu: menu(to),
                                      reload_step: to.id == computation.id,
                                      reload_files: reload?)
    end

    def menu(to)
      ApplicationController.
        render(partial: "computations/menu",
               locals: { pipeline:,
                         computation: to, computations: })
    end

    def broadcast_to_runnable(to)
      RunnableChannel.broadcast_to(to, reload: true)
    end

    def pipelines
      runnable.pipelines.includes(:computations).
        order(:iid).order("computations.created_at")
    end

    def reload?
      computation.status == "finished"
    end
end
