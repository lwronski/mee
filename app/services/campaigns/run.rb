# frozen_string_literal: true

class Campaigns::Run
  def initialize(campaign)
    @campaign = campaign
  end

  def call
    start_pipelines
    @campaign.running!
  end

  private
    def start_pipelines
      @campaign.pipelines.each { |pipeline| ::Pipelines::StartRunnableJob.perform_later(pipeline) }
    end
end
