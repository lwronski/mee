# frozen_string_literal: true

class Campaigns::CreatePipelines
  def initialize(campaign, user, permitted_pipeline_attributes, step_attributes)
    @patients = campaign.cohort.patients
    @campaign = campaign
    @user = user
    @step_attributes = step_attributes
    @permitted_pipeline_attributes = permitted_pipeline_attributes
  end

  def call
    @patients.each do |patient|
      Campaigns::CreatePipelineJob.perform_later(patient:, user: @user, campaign: @campaign,
                                              permitted_attributes: @permitted_pipeline_attributes.merge({ name: "#{@campaign.cohort.name}_#{@campaign.name}_#{patient.slug}" }),
                                              steps_parameters_values: @step_attributes)
    end
  end
end
