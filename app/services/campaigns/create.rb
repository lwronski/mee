# frozen_string_literal: true

class Campaigns::Create
  def initialize(step_attributes, campaign_params, pipeline_params, user)
    @step_attributes = step_attributes
    @campaign_params = campaign_params
    @pipeline_params = pipeline_params
    @user = user
  end

  def call
    @campaign = Campaign.new(@campaign_params.merge(flow: @pipeline_params[:flow_id]))
    if @campaign.valid? && validate_step_attributes(@step_attributes)
      @campaign.save(validate: false)
      Campaigns::CreatePipelines.new(@campaign, @user, @pipeline_params, @step_attributes).call
    end

    @campaign
  end

  private
    def validate_step_attributes(step_attributes)
      invalid = false
      step_attributes.each_value { |step| step.each_value { |attributes| attributes.each_value { |attribute| invalid = true if attribute.empty? } } }
      if invalid
        @campaign.errors.add(:base, :invalid, message: I18n.t("campaigns.errors.steps_attributes"))
        return false
      end
      true
    end
end
