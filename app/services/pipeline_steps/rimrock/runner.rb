# frozen_string_literal: true

module PipelineSteps
  module Rimrock
    class Runner < PipelineSteps::RunnerBase
      def self.tag_or_branch(params)
        params.fetch(:tag_or_branch) { nil }
      end

      private
        delegate :tag_or_branch, :step, to: :computation

        def pre_internal_run
          init_internal_errors
          template_and_revision
          clear_previous_execution_data
        end

        def internal_run
          ::Rimrock::StartJob.perform_later computation if computation.valid?
        end

        def init_internal_errors
          @errors = ActiveModel::Errors.new(computation)
          computation.internal_errors = @errors
        end

        def template_and_revision
          if tag_or_branch.present?
            template, computation.revision = step.template_and_revision_for(tag_or_branch)
            computation.script = ScriptGenerator.new(computation, template, @errors).call
          end
        rescue ComputationError => e
          @errors.add(:script, e.message)
          @errors.add(:tag_or_branch, "choose another or fix this one")
        end

        def clear_previous_execution_data
          computation.job_id = nil
          computation.stdout_path = nil
          computation.stdout.purge
          computation.stderr_path = nil
          computation.stderr.purge
        end

        def repo
          step.git_repository
        end
    end
  end
end
