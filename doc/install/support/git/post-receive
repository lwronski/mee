#!/bin/bash

. $HOME/.asdf/asdf.sh

function log_shell {
  printf "$1... "
  output=`$2`
  if [[ $? == 0 ]]; then
    echo 'done'
    #printf $output
  else
    printf "failed!\n\n$output\n"
    if [[ $3 == true ]]; then
      printf "Ignoring error"
    else
      exit 1
    fi
  fi
}


read oldrev newrev refname
echo "Old revision: $oldrev"
echo "New revision: $newrev"
echo "Reference name: $refname"

new_branch=${refname#refs/heads/}

# Otherwise operations in sub-gits fail
unset GIT_DIR

cd ..

env_git="env -i `which git`"
is_not_new=`$env_git branch | grep -w $new_branch | tr '*' ' '`
current_branch=`$env_git branch | grep '*'`

if [ -f db/schema.rb ]; then
  log_shell "Reseting db/schema.rb" "$env_git checkout -- db/schema.rb"
fi

if [[ "$is_not_new" != *$new_branch* ]]; then
  log_shell "Creating the $new_branch branch" "$env_git checkout -b $new_branch"
fi

if [[ "$current_branch" != *$new_branch* ]]; then
  log_shell "Switching to the $new_branch branch" "$env_git checkout $new_branch"
fi

log_shell "Updating to $newrev" "$env_git reset --hard $newrev"
log_shell "Updating submodules" "$env_git submodule update --init"
log_shell "Bundle configure deployment" "bundle config set --local deployment 'true'"
log_shell "Bundle without development test" "bundle config set --local without 'development test'"
log_shell "Bundling" "bundle install"
log_shell "Yarning" "yarn"
log_shell "Pre-compiling assets" "foreman run bundle exec rake assets:clean assets:precompile"
log_shell "Migrating" "foreman run bundle exec rake db:migrate"
log_shell "Disabling systemd units" "systemctl --user disable dev-vapor.target" true
log_shell "Regenerating systemd configuration" "foreman export -t /home/portal/systemd-templates -a dev-vapor systemd /home/portal/.config/systemd/user/"
log_shell "Reloading systemd" "systemctl --user daemon-reload"
log_shell "Enabling systemd units" "systemctl --user enable dev-vapor.target"

log_shell "Restarting" "systemctl --user restart dev-vapor.target"
