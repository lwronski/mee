# Overview

MEE installation consists of setting up the following components:

1. Packages / Dependencies
2. System user account
3. Ruby and nodejs
4. Database configuration
5. MEE application
6. Nginx web server
7. MEE administrator account
8. Logrotate

## 1. Packages / Dependencies

Install the required packages (reqired to compile Ruby and native extensions to Ruby gems):

```
sudo apt-get update

sudo apt pinstall git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev
```

Make sure you have the right version of Git installed

```
# Install Git
sudo add-apt-repository ppa:git-core/ppa
sudo apt install git
```

```
# Install yarn for js packages management
# and imagemagic for managing active storage variants
sudo apt install imagemagick yarn
```

## 2. System User

Create a `mee` user for MEE:

```
sudo adduser --gecos 'MEE' mee
```

## 3. Ruby and nodejs

You can use ruby installed by ruby version managers such as
[asdf](https://asdf-vm.com/), or install it globally from sources.

```
sudo -u mee -H git clone https://github.com/asdf-vm/asdf.git ~/.asdf
cd /home/mee/.asdf
sudo -u mee -H git checkout "$(git describe --abbrev=0 --tags)"
sudo -u mee -H plugin-add ruby
sudo -u mee -H plugin-add nodejs
sudo -u mee -H echo ". $HOME/.asdf/asdf.sh" >> /home/mee/.bashrc

# Install ruby and nodejs in the version specified by `.tool-versions`
sudo -u mee -H asdf install ruby 3.0.2
sudo -u mee -H asdf global ruby 3.0.2
sudo -u mee -H asdf install nodejs 13.8.0
```

Install the Bundler and Foreman Gems:

```
sudo gem install bundler --no-ri --no-rdoc
sudo gem install foreman --no-ri --no-rdoc
```

## 4. Database setup

Install PostgreSQL database:

```
# Install the database packages
sudo apt install -y postgresql libpq-dev

# Log in to PostgreSQL
sudo -u postgres psql -d template1

# Create a user for MEE
template1=# CREATE USER mee CREATEDB;

# Create the MEE production database & grant all privileges to user mee
template1=# CREATE DATABASE mee_production OWNER mee;

# Quit the database session
template1=# \q

# Try connecting to the new database with the new user
sudo -u mee -H psql -d mee_production
```

Install Redis database:

```
sudo apt install redis-server
```

## 5. MEE rails application

We use Git `hooks` to automatically deploy new releases of MEE.

Prepare a clean git repository:

```
# We'll install MEE into the home directory of the user "mee"
cd /home/mee

# Create the MEE home directory
sudo -u mee -H mkdir prod

# Init empty git repository...
sudo -u prod -H git init /home/mee/prod

# ...and enable pushing to this repository.
cd /home/mee/prod
sudo -u mee -H git config receive.denyCurrentBranch ignore
```

Install the post hook which will be triggered every time new code is pushed into the
repository

```
# Download post-receive hook...
sudo -u mee -H wget --no-check-certificate https://gitlab.com/cyfronet/mee/-/raw/master/doc/install/support/git/post-receive -O /home/mee/prod/.git/hooks/post-receive

# ...make it executable
sudo -u mee -H chmod +x /home/mee/prod/.git/hooks/*

# ...and align it to your needs
sudo -u mee editor /home/mee/prod/.git/hooks/post-receive
```

Install modified templates for foreman upstart script generation.

```
# Create directory for upstart templates
sudo -u mee -H mkdir /home/mee/upstart-templates

# Download templates
sudo -u mee -H wget --no-check-certificate https://gitlab.com/cyfronet/mee/-/raw/master/doc/administration/support/systemd/master.target.erb -O /home/mee/systemd-templates/master.target.erb

sudo -u mee -H wget --no-check-certificate https://gitlab.com/cyfronet/mee/-/raw/master/doc/administration/support/systemd/process.service.erb -O /home/mee/systemd-templates/process.service.erb

# Create directory for generated upstart scripts
sudo -u mee -H mkdir -p /home/mee/.config/systemd

# Automatic start-up of systemd user instances
# see https://wiki.archlinux.org/title/Systemd/User#Automatic_start-up_of_systemd_user_instances
# for details
loginctl enable-linger mee
```

Create MEE configuration files

```
sudo -u mee -H mkdir /home/mee/prod/config

# Download required configuration files
sudo -u mee -H wget --no-check-certificate https://gitlab.com/cyfronet/mee/-/raw/master/config/puma.rb.example -O /home/mee/prod/config/puma.rb

# Customize configuration files
sudo -u mee -H editor /home/mee/prod/config/puma.rb

# Generate certificate for signing JWT tokens
sudo -u mee -H mkdir /home/mee/prod/config/jwt
cd /home/mee/prod/config/jwt
sudo -u mee -H openssl ecparam -name prime256v1 -genkey -noout -out prod.pem
```

Clone MEE code locally (e.g. on your laptop):

```
git clone git@gitlab.com:cyfronet/mee.git
```

Generate random secret locally:

```
cd mee
./bin/rails secret
```
Expose generated secrets as environmental variables on your server

```
# Download environment configuration template
sudo -u mee -H wget --no-check-certificate https://gitlab.com/cyfronet/mee/-/raw/master/doc/install/support/.env.example -O /home/mee/prod/.env

# Use generated secreat and fill other missing ENV variables values
sudo -u mee -H editor /home/mee/prod/.env
```

Add MEE remote to your local MEE copy

```
cd cloned_mee_path
git remote add production portal@production.server.ip:prod
```

Push MEE code into production

```
git push production master
```

As a result, code from the `master` branch will be pushed into the remote server and
the `post-receive` hook will be invoked. It will:
- update remote code to requested version
- install all required dependencies (gems)
- perform database migration
- regenerate upstart scripts
- restart the application.

## 6. Nginx

```
# Install nginx
sudo apt install -y nginx

# Download MEE nginx configuration file
sudo -u mee -H wget --no-check-certificate https://gitlab.com/cyfronet/mee/-/raw/master/doc/install/support/nginx/mee-prod -O /etc/nginx/sites-available/mee-prod

# customize nginx configuration file
# Expecially take a look at domains and certificates. You need 2 domains (e.g.
# mee.cyfronet.pl and *.mee.cyfronet.pl). Domain with "*" are used for MEE
# organization URLs (e.g. primage.mee.cyfronet.pl).
sudo editor /etc/nginx/sites-available/mee-prod

# ...enable it...
sudo ln -s /etc/nginx/sites-available/mee-prod /etc/nginx/sites-enabled/mee-prod

# ...and reload nginx
sudo service nginx reload
```

As a conclusion MEE should be up and running under the selected URL.

## 7. MEE administrator

```
sudo su - mee
cd /home/mee/prod
bundle exec rake db:seed
exit
```

## 8. Logrotate

```
sudo cp /home/mee/prod/doc/install/support/logrotate/mee-prod /etc/logrotate.d/mee-prod
```
