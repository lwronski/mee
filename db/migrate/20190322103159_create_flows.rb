# frozen_string_literal: true

class CreateFlows < ActiveRecord::Migration[5.2]
  def change
    create_table :flows do |t|
      t.string :name, null: false, unique: true, index: true
      t.integer :order, null: false, unique: true, index: true
      t.string :step_names, array: true, default: []

      t.timestamps
    end
  end
end
