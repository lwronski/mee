# frozen_string_literal: true

class CreateCohort < ActiveRecord::Migration[7.0]
  def change
    create_table :cohorts do |t|
      t.string :name
      t.references :organization, null: false, foreign_key: true

      t.timestamps
    end
    create_table :cohort_patients do |t|
      t.references :cohort, null: false, foreign_key: true
      t.references :patient, null: false, foreign_key: true

      t.timestamps
    end

    add_index :cohort_patients, [:patient_id, :cohort_id], unique: true
  end
end
