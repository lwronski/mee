# frozen_string_literal: true

class RemoveRolesFromOrganization < ActiveRecord::Migration[7.0]
  def change
    remove_column :organizations, :roles_mask, :integer
  end
end
