# frozen_string_literal: true

class RemovePdp < ActiveRecord::Migration[6.1]
  def change
    drop_table :access_methods
    drop_table :access_policies
    drop_table :group_relationships
    drop_table :groups
    drop_table :resource_managers
    drop_table :resources
    drop_table :service_ownerships
    drop_table :services
    drop_table :user_groups
  end
end
