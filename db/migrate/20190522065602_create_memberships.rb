# frozen_string_literal: true

class CreateMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :memberships do |t|
      t.integer :roles_mask
      t.integer :state, default: 0, null: false
      t.belongs_to :user, null: false
      t.belongs_to :organization, null: false

      t.timestamps
    end

    add_index :memberships, [:user_id, :organization_id], unique: true
  end
end
