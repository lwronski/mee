# frozen_string_literal: true

class ChangeOrganizationPlgridTeamIdMandatory < ActiveRecord::Migration[7.0]
  def change
    change_column_null :organizations, :plgrid_team_id, false
  end
end
