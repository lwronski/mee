# frozen_string_literal: true

class AddFlowIdToPipelines < ActiveRecord::Migration[5.2]
  def up
    rename_column :pipelines, :flow, :flow_name
    add_reference :pipelines, :flow, index: true

    Pipeline.all.each do |pipeline|
      query = <<-SQL.strip_heredoc
        UPDATE pipelines
        SET flow_id = #{Flow.find_by(name: pipeline.flow_name).id}
        WHERE id = #{pipeline.id}
      SQL

      Pipeline.connection.execute(query)
    end

    change_column_null :pipelines, :flow_id, false
    remove_column :pipelines, :flow_name
  end

  def down
    add_column :pipelines, :flow, :string, default: "full_body_scan", null: false

    Pipeline.all.each do |pipeline|
      query = <<-SQL.strip_heredoc
        UPDATE pipelines
        SET flow = '#{pipeline.flow.name}'
        WHERE id = #{pipeline.id}
      SQL

      Pipeline.connection.execute(query)
    end

    remove_reference :pipelines, :flow
  end
end
