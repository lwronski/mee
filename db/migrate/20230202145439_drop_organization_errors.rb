# frozen_string_literal: true

class DropOrganizationErrors < ActiveRecord::Migration[7.0]
  def change
    drop_table :organization_errors do |t|
      t.string :message, null: false
      t.string :key, index: true, null: false
      t.string :child, null: false
      t.references :organization, foreign_key: true, index: true, null: false
      t.timestamps
    end
  end
end
