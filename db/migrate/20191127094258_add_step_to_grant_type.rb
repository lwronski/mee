# frozen_string_literal: true

class AddStepToGrantType < ActiveRecord::Migration[5.2]
  def change
    add_reference :steps, :grant_type, index: true, foreign_key: true, null: true
  end
end
