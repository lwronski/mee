# frozen_string_literal: true

class AddDiscardedAtToFlows < ActiveRecord::Migration[6.1]
  def change
    add_column :flows, :discarded_at, :datetime
    add_index :flows, :discarded_at
  end
end
