# frozen_string_literal: true

class AddSlugToPatient < ActiveRecord::Migration[6.0]
  def change
    add_column :patients, :slug, :string
    add_index :patients, [:slug, :organization_id], unique: true
    remove_index :patients, :case_number
  end
end
