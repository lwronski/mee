# frozen_string_literal: true

class RemoveSiteFromOrganization < ActiveRecord::Migration[7.0]
  def change
    remove_reference :organizations, :site, foreign_key: true
  end
end
