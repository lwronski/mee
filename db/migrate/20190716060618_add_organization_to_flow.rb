# frozen_string_literal: true

class AddOrganizationToFlow < ActiveRecord::Migration[5.2]
  def change
    add_reference :flows, :organization, foreign_key: true, index: true
  end
end
