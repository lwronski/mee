# frozen_string_literal: true

class AddPlgridTeamNameToOrganization < ActiveRecord::Migration[6.1]
  def up
    add_column :organizations, :plgrid_team_name, :string
    ActiveRecord::Base.connection.execute(
      "UPDATE organizations SET plgrid_team_name = concat('plgg', lower(name))"
    )
  end
  def down
    remove_column :organizations, :plgrid_team_name
  end
end
