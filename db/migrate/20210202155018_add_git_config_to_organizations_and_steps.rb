# frozen_string_literal: true

class AddGitConfigToOrganizationsAndSteps < ActiveRecord::Migration[6.1]
  def change
    add_column :organizations, :git_config, :jsonb
    add_column :steps, :git_config, :jsonb
  end
end
