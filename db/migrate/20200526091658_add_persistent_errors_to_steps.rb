# frozen_string_literal: true

class AddPersistentErrorsToSteps < ActiveRecord::Migration[6.0]
  def change
    add_column :steps, :persistent_errors_count, :integer
  end
end
