# frozen_string_literal: true

class ChangeDataFileTypeOrganizationMandatory < ActiveRecord::Migration[5.2]
  def change
    change_column :data_file_types, :organization_id, :bigint, null: false
  end
end
