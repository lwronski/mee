# frozen_string_literal: true

class AddOrganizationToService < ActiveRecord::Migration[5.2]
  def change
    add_reference :services, :organization, foreign_key: true, index: true
  end
end
