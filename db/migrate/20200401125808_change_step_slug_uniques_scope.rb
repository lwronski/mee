# frozen_string_literal: true

class ChangeStepSlugUniquesScope < ActiveRecord::Migration[6.0]
  def change
    remove_index :steps, :slug
    add_index :steps, [:slug, :organization_id], unique: true
  end
end
