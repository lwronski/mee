# frozen_string_literal: true

class AddNameToDataFileType < ActiveRecord::Migration[5.2]
  def change
    add_column :data_file_types, :name, :string
  end
end
