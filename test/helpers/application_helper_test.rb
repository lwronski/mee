# frozen_string_literal: true

require "test_helper"

class ApplicationHelperTest < ActionView::TestCase
  setup do
    def controller.controller_name = "foo"
    def controller.action_name = "bar"
  end

  test "#current_controller" do
    assert current_controller?(:foo)
    assert_not current_controller?(:bar)

    assert current_controller?(:baz, :fzf, :foo), "Should be true when one match"
    assert_not current_controller?(:baz, :fzf)
  end

  test "#current_action?" do
    assert current_action?(:bar)
    assert_not current_action?(:foo)

    assert current_action?(:baz, :fzf, :bar), "Should be true when one match"
    assert_not current_action?(:baz, :fzf)
  end
end
