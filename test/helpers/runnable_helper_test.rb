# frozen_string_literal: true

require "test_helper"

class RunnableHelperTest < ActionView::TestCase
  test "patient runnable paths" do
    runnable = create(:patient)

    assert_equal patient_path(runnable), runnable_path(runnable)
    assert_equal new_patient_pipeline_path(runnable), new_runnable_pipeline_path(runnable)
  end

  test "organization runnable paths" do
    runnable = organizations(:main)

    assert_equal pipelines_path, runnable_path(runnable)
    assert_equal new_pipeline_path, new_runnable_pipeline_path(runnable)
  end
end
