# frozen_string_literal: true

require "test_helper"

class NavHelperTest < ActionView::TestCase
  include ApplicationHelper

  setup do
    def controller.controller_name = "foo"
    def controller.action_name = "foo"
  end

  test "#nav_link captures block output" do
    assert_match "Testing Blocks", nav_link { "Testing Blocks" }
  end

  test "#nav_link adds active class when current controller" do
    assert_match "<li class=\"active\">", nav_link(controller: :foo)
    assert_match "active", nav_link(controller: [:foo, :bar])
    assert_no_match "active", nav_link(controller: :bar)
  end

  test "#nav_link adds active class when current action" do
    assert_match "<li class=\"active\">", nav_link(action: :foo)
    assert_match "active", nav_link(action: [:foo, :bar])
    assert_no_match "active", nav_link(action: :bar)
  end

  test "#nav_link adds active class when current controller and action" do
    assert_no_match "active", nav_link(controller: :bar, action: :foo)
    assert_no_match "active", nav_link(controller: :foo, action: :bar)
    assert_match "active", nav_link(controller: :foo, action: :foo)
  end

  test "#nav_link accepts a path shorthand" do
    assert_no_match "active", nav_link(path: "foo#bar")
    assert_match "active", nav_link(path: "foo#foo")
  end

  test "#nav_link respect custom active class" do
    assert_match "<li class=\"current-page\">",
      nav_link(controller: :foo, active_class: "current-page")
  end

  test "#nav_link respect extra html options" do
    assert_match "<li class=\"home active\">",
      nav_link(action: :foo, html_options: { class: "home" })

    assert_match "<li class=\"active\">",
      nav_link(html_options: { class: "active" })
  end

  test "#nav_link accept params key and value" do
    def controller.params = { foo: "bar" }

    assert_match "Testing Block",
      nav_link(key: :key, value: "value") { "Testing Block" }

    assert_match "active", nav_link(key: :foo, value: "bar")
    assert_no_match "active", nav_link(key: :foo, value: "other")
    assert_no_match "current-page", nav_link(key: :foo, value: "other", active_class: "current-page")
  end
end
