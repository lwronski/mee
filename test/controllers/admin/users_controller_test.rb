# frozen_string_literal: true

require "test_helper"

class Admin::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization = organizations("main")
    in_organization! @organization
  end

  test "supervisor can block user" do
    membership = Membership.create!(user: create(:user),
                                    organization: @organization,
                                    state: "approved")

    login_as(users("supervisor"))

    assert_changes "membership.reload.state", from: "approved", to: "blocked" do
      put admin_user_path(membership, state: :blocked)
    end
  end

  test "supervisor can approve user" do
    john = create(:user)
    membership = Membership.create!(user: john, organization: @organization)

    login_as(users("supervisor"))
    assert_changes "membership.reload.state", from: "new_account", to: "approved" do
      put admin_user_path(membership, state: :approved)
    end

    assert_equal I18n.t("admin.users.update.success", user: john.name, state: "approved"),
                 flash[:notice]
  end

  test "supervisor cannot block himself" do
    membership = memberships("supervisor")

    login_as(users("supervisor"))
    assert_no_changes "membership.reload.state" do
      put admin_user_path(membership, state: :blocked)
    end
    assert_equal I18n.t("admin.users.update.self"), flash[:alert]
  end

  test "supervisor cannot remove user from organization" do
    login_as(users("supervisor"))
    assert_no_changes "Membership.count" do
      delete admin_user_path(users("user"))
    end
  end

  test "admin can remove user from organization" do
    john = create(:user)
    membership = Membership.create!(user: john,
                                    organization: @organization,
                                    state: "approved")

    login_as(users("admin"))
    assert_changes "Membership.count", -1 do
      delete admin_user_path(membership)
    end
    assert_equal I18n.t("admin.users.destroy.success", user: john.name), flash[:notice]
  end

  test "admin cannot remove himself" do
    login_as(users("admin"))
    assert_no_changes "Membership.count" do
      delete admin_user_path(memberships("admin"))
    end
    assert_equal I18n.t("admin.users.destroy.self"), flash[:alert]
  end

  test "user cannot enter users management page" do
    login_as(users("user"))
    get admin_users_path

    assert_response :redirect
  end

  test "user cannot change users state" do
    login_as(users("user"))
    put admin_user_path(memberships("user"), state: :blocked)

    assert_response :redirect
  end
end
