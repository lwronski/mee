# frozen_string_literal: true

require "test_helper"

class Patients::PipelinesControllerTest < ActionDispatch::IntegrationTest
  include StorageHelper

  setup do
    @organization = organizations("main")
    @patient = create(:patient, organization: @organization)

    in_organization! @organization
  end

  test "Only organization members can see patient page" do
    get patient_pipelines_path(@patient)
    assert_redirected_to root_path
  end

  test "Pipelines view redirects to patient view" do
    login_as users("user")
    get patient_pipelines_path(@patient)
    assert_redirected_to patient_path(@patient)
  end

  test "Organization member can create pipeline for patient" do
    user = users("user")
    login_as user

    assert_changes "@patient.pipelines.count" do
      stub_storage_with_defaults

      post patient_pipelines_path(@patient),
           params: { pipeline: { name: "my pipeline",
                                 flow_id: flows("first").id,
                                 mode: "manual" } }
    end
    assert_equal user, Pipeline.last.user
  end

  test "Pipeline owner can remove it" do
    stub_storage_with_defaults
    owner = users("user")
    pipeline = create(:pipeline, runnable: @patient, user: owner)

    assert_no_changes "Pipeline.count" do
      login_as users("supervisor")
      delete pipeline_path(pipeline)
      assert_response :redirect
    end

    assert_changes "Pipeline.count", -1 do
      login_as owner
      delete pipeline_path(pipeline)
    end
  end

  test "Pipeline owner can update it" do
    owner = users("user")
    pipeline = create(:pipeline, runnable: @patient, user: owner, name: "owned")

    login_as users("supervisor")
    put pipeline_path(pipeline), params: { pipeline: { name: "not my" } }
    assert_redirected_to root_path
    assert_equal "owned", pipeline.reload.name


    login_as owner
    put pipeline_path(pipeline), params: { pipeline: { name: "new name" } }
    assert_redirected_to pipeline_path
    assert_equal "new name", pipeline.reload.name
  end
end
