# frozen_string_literal: true

require "test_helper"

OmniAuth.config.test_mode = true

ActiveSupport.on_load(:action_dispatch_system_test_case) do
  ActionDispatch::SystemTesting::Server.silence_puma = true
end

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400] do |opts|
    opts.add_argument("--no-sandbox")
    opts.add_argument("--disable-dev-shm-usage")
  end

  def login_as(user, teams: "#{organizations("main").plgrid_team_id}(#{organizations("main").name})")
    stub_plgrid_oauth(user, teams)

    visit "/auth/open_id"
  end

  def in_organization!(organization)
    Capybara.app_host = "http://#{organization.slug}.mee.lvh.me"
  end

  def in_root!
    Capybara.app_host = "http://mee.lvh.me"
    Capybara.default_host = Capybara.app_host
  end
end

Capybara.configure do |config|
  config.always_include_port = true
  config.default_max_wait_time = 3
end
