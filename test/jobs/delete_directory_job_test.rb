# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class DeleteDirectoryJobTest < ActiveJob::TestCase
  include ActiveJob::TestHelper

  test "calls 'remove from storage' operation with correct arguments" do
    dir = "some_directory"
    stub_request(:any, "https://rimrock.plgrid.pl/api/process").to_return(status: 200, body: { status: "OK", exit_code: 0 }.to_json)
    perform_enqueued_jobs do
      DeleteDirectoryJob.perform_later(users("user"), organizations("main"), dir)
      assert_requested :post, "https://rimrock.plgrid.pl/api/process",
                       body: { "host": "#{organizations("main").storage_config.host}", "command": "rm -rf #{dir}" },
                       times: 1
    end
  end
end
