# frozen_string_literal: true

require "test_helper"

class ProxyTest < ActiveSupport::TestCase
  include CertHelper

  test "checks for proxy validity" do
    proxy = outdated_proxy

    assert_not proxy.valid?

    travel_to Time.zone.local(2017, 1, 17, 18, 0, 0) do
      assert proxy.valid?
    end
  end

  test "can check validity even for corrupted proxy" do
    proxy = Proxy.for("a b c")

    assert_not proxy.valid?
  end
end
