# frozen_string_literal: true

require "test_helper"

class SiteTest < ActiveSupport::TestCase
  include CertHelper

  test "downloads log content from site" do
    user = users(:user)

    stub_request(:get, "https://data.plgrid.pl/download/ares/my/file").
      with(headers: { "PROXY" => user.proxy.encode }).
      to_return(status: 200, body: "file payload")

    assert_equal "file payload", sites("ares").fetch_logs("my/file", user).read
  end
end
