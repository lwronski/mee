# frozen_string_literal: true

require "test_helper"

class PatientTest < ActiveSupport::TestCase
  ["", "new"].each do |case_number|
    test "'#{case_number}' should not be a valid case number" do
      assert_not build(:patient, case_number:).valid?
    end
  end

  test "#status returns last pipeline status" do
    patient = create(:patient)
    create(:pipeline, runnable: patient, status: :error)
    create(:pipeline, runnable:  patient, status: :success)

    assert_equal "success", patient.status
  end
end
