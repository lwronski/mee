# frozen_string_literal: true

require "test_helper"

class User::JwtTest < ActiveSupport::TestCase
  test "generates and find users using jwt" do
    user = users(:user)

    assert_equal user.id, User.from_token(user.token).id
  end
end
