# frozen_string_literal: true

require "test_helper"

class StepTest < ActiveSupport::TestCase
  setup do
    @pipeline = create(:pipeline)
  end

  test "is runnable when no prerequisites" do
    step = build(:step)

    assert step.input_present_for?(@pipeline)
  end

  test "is runnable when all prerequisites are in place" do
    image, segmentation = data_file_types("image", "segmentation")
    step = build(:step, required_file_types: [image, segmentation])

    assert_not step.input_present_for?(@pipeline)

    create(:data_file,
           input_of: @pipeline, data_type: image,
           fileable: @pipeline.runnable)

    assert_not step.input_present_for?(@pipeline)

    create(:data_file,
           input_of: @pipeline, data_type: segmentation,
           fileable: @pipeline.runnable)

    assert step.input_present_for?(@pipeline)
  end

  test "#slug is organization scoped" do
    first = rimrock_steps("first")
    step = create(:step, name: " #{first.name}  ",
                  organization: organizations("other"))

    assert_equal first.slug, step.slug
  end

  test "#name is organization scoped" do
    first = rimrock_steps("first")
    step = build(:step, name: " #{first.name}  ")

    assert_not step.valid?
    assert step.errors["name"].present?, "Name error should be present"

    step.organization = organizations("other")
    assert step.valid?
  end

  test "Shouldn't discard or destroy when active flows" do
    step = rimrock_steps("second")

    assert_no_difference "Step.count" do
      assert_not step.discard_or_destroy
    end
  end
end
