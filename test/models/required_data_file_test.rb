# frozen_string_literal: true

require "test_helper"

class DataFileTest < ActiveSupport::TestCase
  test "returns path for patient input" do
    input = build(:data_file,
                  fileable: build(:patient, slug: "123"), name: "foo")

    assert_equal "#{organizations("main").working_dir}patients/123/inputs/foo", input.path
  end

  test "returns path for patient pipeline output" do
    patient = create(:patient, case_number: "123")
    pipeline = create(:pipeline, iid: "1", runnable: patient)
    output = build(:data_file, fileable: patient, output_of: pipeline, name: "foo")

    assert_equal "#{organizations("main").working_dir}patients/123/pipelines/1/outputs/foo", output.path
  end

  test "returns path for patient pipeline input" do
    patient = create(:patient, case_number: "123")
    pipeline = create(:pipeline, iid: "1", runnable: patient)
    input = build(:data_file, fileable: patient, input_of: pipeline, name: "foo")

    assert_equal "#{organizations("main").working_dir}patients/123/pipelines/1/inputs/foo", input.path
  end
end
