# frozen_string_literal: true

require "test_helper"

class OrganizationTest < ActiveSupport::TestCase
  test "has many supervisors" do
    assert_equal users("admin", "supervisor").sort,
                 organizations("main").supervisors.sort
  end

  test "has many approved users" do
    org = organizations("main")
    Membership.create!(user: build(:user), organization: org, state: :new_account)
    Membership.create!(user: build(:user), organization: org, state: :blocked)

    assert_equal users("admin", "supervisor", "user").sort,
                 org.approved_users.sort
  end
end
