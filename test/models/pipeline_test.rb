# frozen_string_literal: true

require "test_helper"

class PipelineTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "generates relative pipeline id" do
    patient = create(:patient)

    p1 = create(:pipeline, runnable: patient)
    p2 = create(:pipeline, runnable: patient)

    assert_equal 1, p1.iid
    assert_equal 2, p2.iid
  end

  test "returns pipeline working dir" do
    organization = organizations("main")
    pipeline = build(:pipeline,
                     iid: 123,
                     runnable: build(:patient, slug: "abc"))

    assert_equal "#{organization.working_dir}patients/abc/pipelines/123/inputs/", pipeline.inputs_dir
    assert_equal "#{organization.working_dir}patients/abc/pipelines/123/outputs/", pipeline.outputs_dir
  end

  test "returns data file scoped into pipeline" do
    patient = create(:patient)
    p1, p2 = create_list(:pipeline, 2, runnable: patient)

    image = data_file_types("image")
    off_mesh = create(:data_file_type)
    graphics = create(:data_file_type)
    estimated_parameters = create(:data_file_type)

    create(:data_file,
           fileable: patient, output_of: p1,
           data_type: image, name: "p1 image output")
    create(:data_file,
           fileable: patient, output_of: p2,
           data_type: image, name: "p2 image output")
    create(:data_file,
           fileable: patient, input_of: p1,
           data_type: off_mesh, name: "p1 off mesh output")
    create(:data_file,
           fileable: patient, input_of: p2,
           data_type: graphics, name: "p2 graphics output")
    create(:data_file,
           fileable: patient,
           data_type: estimated_parameters, name: "input")

    assert_equal "p1 image output", p1.data_file(image).name
    assert_equal "p2 image output", p2.data_file(image).name
    assert_nil p2.data_file(off_mesh)
    assert_equal "p2 graphics output", p2.data_file(graphics).name
    assert_equal "input", p1.data_file(estimated_parameters).name
    assert_equal "input", p2.data_file(estimated_parameters).name
  end

  test "calls StatusCalculator when asked for status" do
    Pipeline::StatusCalculator.any_instance.expects(:calculate)
    pipeline = create(:pipeline)
    pipeline.set_status
  end

  test "returns creator name" do
    user = users("user")
    pipeline = create(:pipeline, user:)

    assert_equal user.name, pipeline.owner_name
  end

  test "returns information about deleted user when owner is nil" do
    user = users("user")
    pipeline = create(:pipeline, user:)

    user.destroy!
    pipeline.reload

    assert_equal "(deleted user)", pipeline.owner_name
  end

  test "destroys discarded flow when it has only one pipelines" do
    flow = create(:flow, discarded_at: Time.current)
    pipeline = create(:pipeline, flow:)

    assert_changes "Flow.count", -1 do
      pipeline.destroy
    end
  end

  test "doesn't destroy the flow when it has more pipelines" do
    flow = create(:flow, discarded_at: Time.current)
    pipeline = create(:pipeline, flow:)
    create(:pipeline, flow:)

    assert_no_changes "Flow.count" do
      pipeline.destroy
    end
  end

  test "enqueues DeleteDirectoryJob when removed" do
    record = create(:pipeline)
    assert_enqueued_with(job: DeleteDirectoryJob) do
      record.destroy
    end
  end
end
