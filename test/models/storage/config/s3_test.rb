# frozen_string_literal: true

require "test_helper"

class Storage::Config::S3Test < ActiveSupport::TestCase
  test "validates storage configuration correctness" do
    config = Storage::Config::S3
      .new(endpoint: "https://some-cloud.com",
           bucket: "some_bucket",
           secret_key: "some secret key", access_key: "some access key")

    Current.set(user: users("admin")) do
      Storage::S3.any_instance.stubs(:valid?).returns(false)
      assert_not config.valid?
      assert_equal 1, config.errors[:type].size

      Storage::S3.any_instance.stubs(:valid?).returns(true)
      assert config.valid?
    end
  end

  test "does not test connection when fields validations failed" do
    config = Storage::Config::S3
      .new(endpoint: "wrong-url",
           bucket: "some_bucket",
           secret_key: "some secret key", access_key: "some access key")

    Storage::S3.any_instance.stubs(:valid?).never

    assert_not config.valid?
  end
end
