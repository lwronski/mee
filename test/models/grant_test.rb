# frozen_string_literal: true

require "test_helper"

class GrantTest < ActiveSupport::TestCase
  test "#active returns only active grants" do
    create(:expired_grant)
    create(:future_grant)

    assert_equal grants("cpu", "gpu").sort, Grant.active.sort
  end
end
