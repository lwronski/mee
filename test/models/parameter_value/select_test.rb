# frozen_string_literal: true

require "test_helper"

class ParameterValue::SelectTest < ActiveSupport::TestCase
  setup do
    @pv = build(:select_parameter_value)
  end

  test "allows only values defined in parameter" do
    @pv.value = "nonexisting"
    assert_not @pv.valid?
    assert_match "is not included in the list", @pv.errors[:value].first

    @pv.value = @pv.values.first.value
    assert @pv.valid?
  end

  test "has default value" do
    @pv.parameter.default_value = @pv.parameter.values.first.value

    assert_equal @pv.parameter.default_value, @pv.value

    @pv.value = ""
    assert_equal "", @pv.value

    @pv.value = @pv.parameter.values.second.value
    assert_equal @pv.parameter.values.second.value, @pv.value
  end
end
