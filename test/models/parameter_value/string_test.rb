# frozen_string_literal: true

require "test_helper"

class ParameterValue::StringTest < ActiveSupport::TestCase
  setup do
    @pv = build(:string_parameter_value)
  end

  test "has default value" do
    @pv.parameter.default_value = "default"

    assert_equal "default", @pv.value

    @pv.value = ""
    assert_equal "", @pv.value

    @pv.value = "value123"
    assert_equal "value123", @pv.value
  end
end
