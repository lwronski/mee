# frozen_string_literal: true

require "test_helper"

class Parameter::RequiredDataFileTest < ActiveSupport::TestCase
  setup do
    @data_file_type = data_file_types("image")
    @parameter = Parameter::RequiredDataFile.new(data_file_type: @data_file_type, data_files: [])
  end

  test "generates correct name based on data_file_type" do
    assert_equal @data_file_type.name, @parameter.name
  end

  test "generates correct key based on data_file_type" do
    assert_equal "#{Parameter::RequiredDataFile::PREREQUISITE}#{@data_file_type.data_type}", @parameter.key
  end
end
