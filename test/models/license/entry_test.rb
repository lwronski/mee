# frozen_string_literal: true

require "test_helper"

class License::EntryTest < ActiveSupport::TestCase
  test "correct key values" do
    entry = License::Entry.new(key: "a", value: "test1&*&%*")
    assert entry.valid?

    entry = License::Entry.new(key: "correct_name123", value: "test1&*&%*")
    assert entry.valid?

    entry = License::Entry.new(key: "_correct1name2", value: "test1&*&%*")
    assert entry.valid?

    entry = License::Entry.new(key: "CORRECTNAME", value: "test1&*&%*")
    assert entry.valid?
  end

  test "incorrect key values" do
    entry = License::Entry.new(key: "", value: "test1&*&%*")
    assert_not entry.valid?

    entry = License::Entry.new(key: "1", value: "1")
    assert_not entry.valid?

    entry = License::Entry.new(key: "1_incorrect", value: "abc")
    assert_not entry.valid?
  end
end
