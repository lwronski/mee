# frozen_string_literal: true

require "test_helper"

class ErrorableTest < ActiveSupport::TestCase
  include GitlabHelper

  setup do
    @organization = organizations("main")
  end

  test "is created by organization token validation job when token is wrong" do
    stub_gitlab_branches(repository: GitlabTokenValidator::TEST_REPOSITORY,
                        status: 401, body: { error: "wrong token" })

    Current.set(user: users("admin")) do
      assert_changes "@organization.persistent_errors.count" do
        Organization::ValidateTokenJob.perform_now(@organization)
      end
    end
  end

  test "are cleared when organization is saved" do
    @organization.persistent_errors.create(child: "git_config", key: "private_token", message: "test invalid")
    @organization.persistent_errors.create(child: "git_config", key: "download_key", message: "another error")

    Current.set(user: users("admin")) do
      assert_changes "@organization.persistent_errors.count", -2 do
        @organization.save
      end
    end
  end
end
