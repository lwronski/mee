# frozen_string_literal: true

require "test_helper"

class RimrockComputationTest < ActiveSupport::TestCase
  test "Computation is configured only if tag or branch is set" do
    assert_not build(:computation, tag_or_branch: nil).configured?
    assert_not build(:computation, tag_or_branch: "").configured?
    assert build(:computation, tag_or_branch: "x").configured?
  end
end
