# frozen_string_literal: true

require "test_helper"

class JwtTokenTest < ActiveSupport::TestCase
  setup do
    @token = JwtToken.new(users("user"))
  end

  test "includes issuer in token" do
    assert_equal Mee::Application.config.jwt.issuer,
                 key_from_token(@token.generate, "iss")
  end

  test "includes expiration time in token" do
    freeze_time do
      assert_equal Time.now.to_i + Mee::Application.config.jwt.expiration_time,
                   key_from_token(@token.generate, "exp")
    end
  end

  test "includes sub in token" do
    assert_equal users("user").id.to_s, key_from_token(@token.generate, "sub")
  end

  test "allows to create long tokens" do
    freeze_time do
      assert_equal Time.now.to_i + long_expiration_time,
                   key_from_token(@token.generate(long_expiration_time), "exp")
    end
  end

  test "expired token fails with error" do
    expired_token = @token.generate

    travel((Mee::Application.config.jwt.expiration_time + 1).seconds)

    assert_raise JWT::ExpiredSignature do
      User.from_token(expired_token)
    end
  end

  private
    def key_from_token(enc_token, key)
      decode_token(enc_token).detect { |el| el.key? key }.try(:[], key)
    end

    def decode_token(enc_token)
      JWT.decode(
        enc_token, Mee::Application.config.jwt.key, true,
        algorithm: Mee::Application.config.jwt.key_algorithm
      )
    end

    def long_expiration_time
      2 * Mee::Application.config.jwt.expiration_time
    end
end
