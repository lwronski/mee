# frozen_string_literal: true

require "test_helper"

class ComputationTest < ActiveSupport::TestCase
  test "#active returns only new, queued or running computations" do
    computation = create(:computation, status: "new")
    assert_equal [computation], Computation.active

    computation.update(status: "queued")
    assert_equal [computation], Computation.active

    computation.update(status: "running")
    assert_equal [computation], Computation.active

    computation.update(status: "finished")
    assert_equal [], Computation.active
  end

  test "#submitted returns only queued and running computations" do
    create(:computation, status: "new")
    queued = create(:computation, status: "queued")
    running = create(:computation, status: "running")

    assert_equal [queued, running].sort, Computation.submitted.sort
  end

  test "#unsubmitted returns only created and new computations" do
    new = create(:computation, status: "new")
    create(:computation, status: "queued")
    created = create(:computation, status: "created")
    create(:computation, status: "running")
    create(:computation, status: "error")
    create(:computation, status: "finished")
    create(:computation, status: "aborted")

    assert_equal 7, Computation.all.count
    assert_equal [new, created].sort, Computation.unsubmitted.sort
  end

  test "#submitted_rimrock returns only queued and running rimrock-based computations" do
    create(:computation, status: "new")
    queued = create(:computation, status: "queued")
    running = create(:computation, status: "running")

    assert_equal [queued, running].sort, Computation.submitted_rimrock.sort
  end
end
