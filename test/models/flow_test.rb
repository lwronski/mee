# frozen_string_literal: true

require "test_helper"

class FlowTest < ActiveSupport::TestCase
  test "#steps should be ordered" do
    s1, s2, s3 = create_list(:step, 3)
    flow = create(:flow, flow_steps_attributes: [
                        { step: s1, position: 2 },
                        { step: s2, position: 3 },
                        { step: s3, position: 1 }])

    steps = flow.steps.to_a

    assert_equal s3, steps[0]
    assert_equal s1, steps[1]
    assert_equal s2, steps[2]
  end

  test "#cleanup destroys discarded and not used flows" do
    create(:flow, discarded_at:  Time.current)

    assert_changes "Flow.count", -1 do
      Flow.cleanup
    end
  end

  test "discarding deletes useless steps" do
    rimrock_steps("first").discard

    assert_changes "Step.count", -1 do
      flows("first").destroy
    end
  end

  test "discarding doesn't delete used steps" do
    assert_no_changes "Step.count" do
      flows("first").destroy
    end
  end
end
