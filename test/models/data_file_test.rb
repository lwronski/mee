# frozen_string_literal: true

require "test_helper"

class DataFileTest < ActiveSupport::TestCase
  test "Should validate pattern" do
    assert_not build(:data_file_type, pattern: "?").valid?
    assert_not build(:data_file_type, pattern: "").valid?
    assert_not build(:data_file_type, pattern: nil).valid?

    assert build(:data_file_type, pattern:  /^dft.txt$/).valid?
  end

  test "Should match according to pattern" do
    dft = build(:data_file_type, pattern: /^dft.txt$/)

    assert dft.match?("dft.txt")
    assert_not dft.match?("abc.1")
  end
end
