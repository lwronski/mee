# frozen_string_literal: true

require "test_helper"

class ArtifactTest < ActiveSupport::TestCase
  test "should properly add extension" do
    artifact = create(:artifact,
                      name: "foo",
                      file_id: create(:data_file, name: "bar.txt").id)

    assert_equal "foo.txt", artifact.name
  end
end
