# frozen_string_literal: true

require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "returns users with submitted Rimrock computations" do
    u1, u2, u3 = create_list(:user, 3)

    create(:computation, status: "new", user: u1)
    create(:computation, status: "finished", user: u1)
    create(:computation, status: "queued", user: u2)
    create(:computation, status: "running", user: u3)

    assert_equal [u2, u3].sort,
                 User.with_submitted_computations("RimrockComputation").sort
  end
end
