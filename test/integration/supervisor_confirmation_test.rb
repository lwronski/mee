# frozen_string_literal: true

require "test_helper"

class SerpervisorConfirmationTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
  end

  test "non supervisor users should not see the confirmation page" do
    login_as(users("user"))
    follow_redirect!
    follow_redirect!

    assert_no_match I18n.t("layouts.organization.side_menu.administration.users"), response.body
  end

  test "supervisor users should see the confirmation page" do
    login_as(users("supervisor"))
    follow_redirect!
    follow_redirect!

    assert_match I18n.t("layouts.organization.side_menu.administration.users"), response.body
  end
end
