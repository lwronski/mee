# frozen_string_literal: true

module PipelineBrowsingHelper
  def mock_rimrock_computation_ready_to_run
    Computation.any_instance.stubs(:runnable?).returns(true)
    RimrockStep.any_instance.stubs(:input_present_for?).returns(true)
    Proxy.any_instance.stubs(:valid?).returns(true)
  end

  def computation_run_text(computation)
    I18n.t("computations.start_#{computation.mode}", step: computation.name)
  end
end
