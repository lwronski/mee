# frozen_string_literal: true

module CertHelper
  def private_key
    SSHKey.generate.private_key
  end

  def valid_proxy
    key = OpenSSL::PKey::RSA.new(1024)
    public_key = key.public_key

    subject = "/C=BE/O=Test/OU=Test/CN=Test"

    cert = OpenSSL::X509::Certificate.new
    cert.subject = cert.issuer = OpenSSL::X509::Name.parse(subject)
    cert.not_before = Time.now
    cert.not_after = Time.now + 24 * 60 * 60
    cert.public_key = public_key
    cert.serial = 0x0
    cert.version = 2

    cert.sign key, OpenSSL::Digest::SHA1.new

    cert.to_pem
  end

  def outdated_proxy
    Proxy.new(file_fixture("outdated_proxy").read)
  end
end
