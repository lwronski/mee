# frozen_string_literal: true

module OauthHelper
  def stub_oauth(provider, options = {})
    OmniAuth.config.add_mock(
      provider,
      {
        info: {
          nickname: nil, name: nil,
          first_name: nil, last_name: nil,
          email: nil
        },
        provider:,
        uid: "123"
      }.deep_merge(options)
    )
  end

  def stub_plgrid_oauth(user, teams = "")
    stub_oauth(
      :open_id,
      info: {
        name: "#{user.first_name} #{user.last_name}",
        nickname: user.plgrid_login,
        email: user.email,
        proxy: user.proxy.proxy_cert,
        userCert: user.proxy.user_cert,
        proxyPrivKey: user.proxy.proxy_priv_key,
        userteams: teams
      }
    )
  end
end
