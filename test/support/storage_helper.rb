# frozen_string_literal: true

require "minitest/autorun"

module StorageHelper
  def stub_storage(&block)
    mock = mock()
    Organization.any_instance.stubs(:storage_for).returns(mock)
    mock
  end

  def stub_storage_with_defaults(&block)
    mock = stub_storage

    def mock.valid? = true
    def mock.require_proxy? = false
    def mock.remove(...) = true
    def mock.mkdir(...) = true
    def mock.list(...) = {}
    def mock.copy(...) = true
    def mock.move(...) = true

    mock
  end

  def stub_storage_failure
    mock = stub_storage

    def mock.valid? = true
    def mock.require_proxy? = false
    def mock.remove(...) = false
    def mock.mkdir(...) = false
    def mock.list(...) = {}
    def mock.copy(...) = false
    def mock.move(...) = false

    mock
  end
end
