# frozen_string_literal: true

require "test_helper"

class PipelinePolicyTest < ActiveSupport::TestCase
  include PunditHelper

  test "All organization members can create and view pipeline" do
    pipeline = build(:pipeline)

    assert_permit(pundit_user, pipeline, :new)
    assert_permit(pundit_user, pipeline, :create)
    assert_permit(pundit_user, pipeline, :show)
  end

  test "Cannot create pipeline from other organization flow" do
    stranger_flow = build(:flow, organization: organizations("other"))

    assert_no_permit(pundit_user, build(:pipeline, flow: stranger_flow), :create)
  end

  test "Owner and admin can manage pipeline" do
    pipeline = build(:pipeline)

    # Owner
    assert_permit(pundit_user, pipeline, :update)
    assert_permit(pundit_user, pipeline, :destroy)
    assert_permit(pundit_user, pipeline, :edit)

    # Admin
    assert_permit(pundit_user, pipeline, :update)
    assert_permit(pundit_user, pipeline, :destroy)
    assert_permit(pundit_user, pipeline, :edit)

    # Other user
    assert_no_permit(supervisor_pundit_user, pipeline, :update)
    assert_no_permit(supervisor_pundit_user, pipeline, :destroy)
    assert_no_permit(supervisor_pundit_user, pipeline, :edit)
  end
end
