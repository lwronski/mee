# frozen_string_literal: true

require "test_helper"

class StepPolicyTest < ActiveSupport::TestCase
  include PunditHelper

  test "Step can be edited only bo owner and admin" do
    step = rimrock_steps("first")

    # Owner
    assert_permit(pundit_user, step, :edit)
    assert_permit(pundit_user, step, :update)

    # Admin
    assert_permit(admin_pundit_user, step, :edit)
    assert_permit(admin_pundit_user, step, :update)

    # Other organization user
    assert_no_permit(supervisor_pundit_user, step, :edit)
    assert_no_permit(supervisor_pundit_user, step, :update)
  end

  test "Step cannot be edited when has active computations" do
    step = rimrock_steps("first")
    create(:computation, step:, status: :running)

    assert_no_permit(admin_pundit_user, step, :edit)
    assert_no_permit(admin_pundit_user, step, :update)
  end

  test "Returns only organization steps" do
    create(:step, organization: organizations("other"))
    scope = StepPolicy::Scope.new(pundit_user, Step.all)

    assert_equal rimrock_steps("first", "second").sort, scope.resolve.sort
  end
end
