# frozen_string_literal: true

require "test_helper"

class FlowPolicyTest < ActiveSupport::TestCase
  include PunditHelper

  test "Flow can be edited only by owner and admin" do
    flow = flows("first")

    # Owner
    assert_permit(pundit_user, flow, :edit)
    assert_permit(pundit_user, flow, :update)

    # Admin
    assert_permit(admin_pundit_user, flow, :edit)
    assert_permit(admin_pundit_user, flow, :update)

    # Other organization user
    assert_no_permit(supervisor_pundit_user, flow, :edit)
    assert_no_permit(supervisor_pundit_user, flow, :update)
  end

  test "Returns only organization flows" do
    create(:flow, organization: organizations("other"))
    scope = FlowPolicy::Scope.new(pundit_user, Flow.all)

    assert_equal [flows("first")], scope.resolve
  end
end
