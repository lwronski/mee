# frozen_string_literal: true

require "test_helper"

class Membership::UpdateTest < ActiveSupport::TestCase
  test "updates user status" do
    membership = memberships("user")

    result = Membership::Update.new(users("admin"), membership, { state: "blocked" }).call

    assert_equal :ok, result
    assert_equal "blocked", membership.reload.state
  end

  test "returns :self when trying to block self" do
    result = Membership::Update.new(users("admin"), memberships("admin"), { state: "blocked" }).call

    assert_equal :self, result
  end

  test "is possible to remove user roles" do
    membership = memberships("user")
    membership.update(roles: [:admin, :supervisor])

    result = Membership::Update.new(users("admin"), membership, { roles: [] }).call

    assert_equal :ok, result
    assert_empty membership.reload.roles
  end

  test "is forbidden to block last admin" do
    result = Membership::Update.new(users("admin"), memberships("admin"), { roles: [] }).call

    assert_equal :last_admin, result
  end

  test "updates user roles" do
    membership = memberships("user")
    result = Membership::Update.new(users("admin"), membership, { roles: ["admin", "supervisor"] }).call
    membership.reload

    assert_equal :ok, result
    assert membership.admin?
    assert membership.supervisor?
  end
end
