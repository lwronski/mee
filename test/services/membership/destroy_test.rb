# frozen_string_literal: true

require "test_helper"

class Membership::DestroyTest < ActiveSupport::TestCase
  include StorageHelper
  include RimrockHelper

  test "removes user membership" do
    assert_changes "Membership.count", -1 do
      result = Membership::Destroy.new(users("admin"), memberships("user")).call
      assert_equal :ok, result
    end
  end

  test "abort all active user computation" do
    stub_storage_with_defaults

    computation = create(:computation, user: users("user"), status: :running)
    stub_job_abort(computation)

    Membership::Destroy.new(users("admin"), memberships("user")).call

    assert_equal "aborted", computation.reload.status
  end

  test "is fobidden to remove self" do
    assert_no_changes "Membership.count" do
      result = Membership::Destroy.new(users("admin"), memberships("admin")).call
      assert_equal :self, result
    end
  end
end
