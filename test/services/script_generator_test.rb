# frozen_string_literal: true

require "test_helper"

class ScriptGeneratorTest < ActiveSupport::TestCase
  include GitlabHelper
  test "add error when active grant cannot be found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    travel_to(1.year.from_now) do
      ScriptGenerator.new(computation, "{{ grant_id }}", errors).call
    end


    assert_includes errors[:script], "active grant cannot be found"
  end

  test "inserts upload file curl for file type" do
    image_dft = data_file_types("image")
    patient = create(:patient,
      case_number: "Case number",
      data_files: [build(
        :data_file,
        data_type: image_dft,
        name: "foo.txt"
      )]
    )

    pipeline = create(:pipeline, runnable: patient, iid: 1)
    computation = create(:computation, pipeline:, revision: "rev")
    data_file = pipeline.data_files(image_dft).first

    script = ScriptGenerator.new(
      computation,
      "{% stage_in #{image_dft.data_type} out.txt %}"
    ).call

    assert_includes script, data_file.path
    assert_includes script, "$SCRATCHDIR/out.txt"
  end

  test "adds error when input file is not found" do
    computation = build(:computation)
    create(:data_file_type, data_type: "provenance")
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% stage_in provenance %}", errors).call

    assert_includes errors[:script],
                   "cannot find provenance data file in patient or pipeline directories"
  end

  test "inserts upload file for file type" do
    patient = build(:patient)
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)
    dft = create(:data_file_type,
                  name: "TestDataFileType",
                  viewer: :text,
                  data_type: "test_data_file_type", pattern: /^test_data_file.*\.txt$/)
    computation.step.required_file_types << dft
    df = create(:data_file, name: "test_data_file1.txt", data_type: dft, fileable: patient, input_of: pipeline)

    script = ScriptGenerator.new(
      computation,
      "{% stage_in #{dft.data_type} %}"
    ).call

    assert_includes script, "$SCRATCHDIR/#{df.name}"
  end

  test "inserts download file curl" do
    computation = create(:computation)
    script = ScriptGenerator.new(computation,
                                 "{% stage_out foo.txt %}").call

    assert_includes script, "cp foo.txt #{computation.pipeline.outputs_dir}"
  end

  test "inserts download file curl with default target file name" do
    computation = create(:computation)
    script = ScriptGenerator.new(computation,
                                 "{% stage_out dir/foo.txt %}").call

    assert_includes script, "cp dir/foo.txt #{computation.pipeline.outputs_dir}"
  end

  test "inserts repository sha to clone" do
    script = ScriptGenerator.new(create(:computation, revision: "rev"),
                                 "{{ revision }}").call

    assert_includes script, "rev"
  end

  test "inserts clone repo command" do
    organization = organizations("main")
    computation = build(:computation, revision: "rev")
    script = ScriptGenerator.new(computation, "{% clone_repo %}").call

    assert_includes script, "export SSH_DOWNLOAD_KEY=\"#{organization.git_config.download_key}"
    assert_includes script, "git clone git@#{organization.git_config.host}:#{computation.repository}"
    assert_match(/cd.*repo.*/, script)
    assert_includes script, "git reset --hard rev"
  end

  test "inserts license_for ansys script" do
    create(:license,
          name: "ansys",
          config: [License::Entry.new(key: "ansysli_servers",
                                      value: "ansys-servers"),
                    License::Entry.new(key: "ansyslmd_license_file",
                                      value: "ansys-license-file")
                    ])

    script = ScriptGenerator.new(create(:computation, revision: "rev"),
                                "{% license_for ansys %}").call

    assert_includes script, "export ANSYSLI_SERVERS=ansys-servers"
    assert_includes script, "export ANSYSLMD_LICENSE_FILE=ansys-license-file"
  end

  test "adds error when requested license is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% license_for ansys %}", errors).call

    assert_includes errors[:script], "cannot find requested ansys license"
  end


  test "inserts value_of tag-or-branch and grant" do
    step = rimrock_steps("first")
    stub_repo_versions(step,
                      { branches: ["main"], tags: ["t1"] })

    computation = create(:computation,
                         step:,
                          parameter_values_attributes: {
                            "tag-or-branch" => { "version" => "main" },
                            "grant" => { "value" => grants("cpu").name }
                          })

    script = ScriptGenerator.new(computation,
                                "{% value_of tag-or-branch %}").call

    assert_includes "main", script
  end

  test "adds error when requested value is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% value_of tag-or-branch %}", errors).call

    assert_includes errors[:script], "parameter value tag-or-branch was not found"
  end

  test "inserts pipeline identifier" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ pipeline_identifier }}").call

    assert_equal "case-312-#{pipeline.iid}", script
  end

  test "inserts patient case_number" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ case_number }}").call

    assert_equal "Case 312", script
  end

  test "generates user token" do
    user = users("user")
    pipeline = build(:pipeline, user:)
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ token }}").call

    assert script.size > 0
  end

  test "inserts user email" do
    user = users("user")
    pipeline = build(:pipeline, user:)
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ email }}").call

    assert_equal script, user.email
  end

  test "inserts pipeline mode" do
    pipeline = build(:pipeline, mode: "automatic")
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ mode }}").call

    assert_equal script, "automatic"
  end

  test "inserts user proxy certificate" do
    user = users("user")
    computation = build(:computation, user:)

    script = ScriptGenerator.new(computation, "{{ proxy }}").call
    assert_includes script, user.proxy.to_s
  end
end
