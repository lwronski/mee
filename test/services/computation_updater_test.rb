# frozen_string_literal: true

require "test_helper"

class ComputationUpdaterTest < ActiveSupport::TestCase
  setup do
    @patient = create(:patient)
    @pipeline = create(:pipeline, runnable: @patient)
    @c1 = create(:computation, pipeline: @pipeline, status: "new", step: rimrock_steps("first"))
    @c2 = create(:computation, pipeline: @pipeline, status: "finished", step: rimrock_steps("second"))
  end

  test "broadcast computation change" do
    ComputationChannel.expects(:broadcast_to)
      .with(@c1, has_entries(reload_step: true, reload_files: false))
    ComputationChannel.expects(:broadcast_to)
      .with(@c2, has_entries(reload_step: false, reload_files: false))

    ComputationUpdater.new(@c1).call
  end

  test "broadcast output reload after finish" do
    ComputationChannel.expects(:broadcast_to)
      .with(@c1, has_entries(reload_step: false, reload_files: true))
    ComputationChannel.expects(:broadcast_to)
      .with(@c2, has_entries(reload_step: true, reload_files: true))

    ComputationUpdater.new(@c2).call
  end

  test "broadcast reload patient pipelines statuses" do
    RunnableChannel.expects(:broadcast_to).with(@patient, anything)

    ComputationUpdater.new(@c2).call
  end
end
