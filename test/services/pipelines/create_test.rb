# frozen_string_literal: true

require "test_helper"

class Pipelines::CreateTest < ActiveSupport::TestCase
  include StorageHelper

  setup do
    @user = users("user")
  end

  test "creates new pipeline in db" do
    stub_storage_with_defaults

    assert_changes "Pipeline.count" do
      Pipelines::Create.new(@user, build(:pipeline, user: @user)).call
    end
  end

  test "creates pipeline directory" do
    organization = organizations("main")
    patient = create(:patient)
    new_pipeline = build(:pipeline, runnable: patient)

    storage = stub_storage_with_defaults
    storage.stubs(:mkdir).
      with("#{organization.working_dir}patients/#{patient.slug}/pipelines/1/",
           "#{organization.working_dir}patients/#{patient.slug}/pipelines/1/inputs/",
           "#{organization.working_dir}patients/#{patient.slug}/pipelines/1/outputs/")

    Pipelines::Create.new(@user, new_pipeline).call
  end

  test "don't create db pipeline when dirs cannot be created" do
    stub_storage_failure

    assert_no_changes "Pipeline.count" do
      Pipelines::Create.new(@user, build(:pipeline)).call
    end
  end

  test 'don\t create pipeline dirs structure when pipeline cannot be created' do
    bad_pipeline = build(:pipeline, name: nil)

    storage = stub_storage_with_defaults
    storage.expects(:mkdir).never

    Pipelines::Create.new(@user, bad_pipeline).call
  end

  test "set error message when web pipeline dirs cannot be created" do
    stub_storage_failure
    new_pipeline = build(:pipeline)

    Pipelines::Create.new(@user, new_pipeline).call

    assert_includes new_pipeline.errors[:name],
                    I18n.t("activerecord.errors.models.pipeline.create_dav403")
  end
end
