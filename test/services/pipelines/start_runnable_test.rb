# frozen_string_literal: true

require "test_helper"

class Pipelines::StartRunnableTest < ActiveSupport::TestCase
  setup do
    @dft = data_file_types("image")
    @pipeline = create(:pipeline, mode: :automatic, user: users("user"))
    @step = create(:step, required_file_types: [@dft])
  end

  test "starts created runnable pipeline step when proxy is valid and inputs are in place" do
    create(:computation, status: "created", step: @step, pipeline: @pipeline)
    create(:data_file, fileable: @pipeline.runnable, data_type: @dft)

    runner = mock
    runner.expects(:call)
    PipelineSteps::Rimrock::Runner.expects(:new).returns(runner)

    Pipelines::StartRunnable.new(@pipeline).call
  end

  test "does not start when tag or branch is not chosen" do
    create(:computation, status: "created", step: @step, tag_or_branch: nil, pipeline: @pipeline)
    create(:data_file, fileable: @pipeline.runnable, data_type: @dft)

    runner = mock
    runner.expects(:call).never
    PipelineSteps::Rimrock::Runner.stubs(:new).returns(runner)

    Pipelines::StartRunnable.new(@pipeline).call
  end

  test "does not start already started pipeline step" do
    create(:computation, status: "running", step: @step, pipeline: @pipeline)
    create(:data_file, fileable: @pipeline.runnable, data_type: @dft)

    PipelineSteps::Rimrock::Runner.expects(:new).never

    Pipelines::StartRunnable.new(@pipeline).call
  end

  test "does not start not runnable pipeline step" do
    create(:computation, status: "created", step: @step, pipeline: @pipeline)

    PipelineSteps::Rimrock::Runner.expects(:new).never

    Pipelines::StartRunnable.new(@pipeline).call
  end

  test "runnable rimrock computations are not started when proxy is not valid" do
    create(:computation, status: "created", step: @step, pipeline: @pipeline)
    create(:data_file, fileable: @pipeline.runnable, data_type: @dft)

    runner = mock
    runner.expects(:call).never
    PipelineSteps::Rimrock::Runner.stubs(:new).returns(runner)

    travel 2.days do
      Pipelines::StartRunnable.new(@pipeline).call
    end
  end
end
