# frozen_string_literal: true

require "test_helper"

class Patients::DestroyTest < ActiveSupport::TestCase
  include StorageHelper

  def setup
    @storage = stub_storage_with_defaults
    @patient = create(:patient)
    @user = users("user")
  end

  test "remove patient from db" do
    assert_changes "Patient.count", -1 do
      Patients::Destroy.new(@user, @patient).call
    end
  end

  test "calls storage#remove with patient directory" do
    @storage.expects(:remove).with("#{@patient.organization.working_dir}patients/#{@patient.slug}/")
    Patients::Destroy.new(@user, @patient).call
  end

  test "returns true when patient is removed" do
    result = Patients::Destroy.new(@user, @patient).call

    assert result
  end

  test "don't remove patient when cannot remove patient directory" do
    stub_storage_failure

    assert_no_changes "Patient.count" do
      Patients::Destroy.new(@user, @patient).call
    end
  end

  test "returns false when patient cannot be removed" do
    stub_storage_failure

    result = Patients::Destroy.new(@user, @patient).call

    assert_not result
  end
end
