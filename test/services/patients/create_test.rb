# frozen_string_literal: true

require "test_helper"

class Patients::CreateTest < ActiveSupport::TestCase
  include StorageHelper

  def setup
    @storage = stub_storage_with_defaults
    @user = users("user")
  end

  test "creates new patient in db" do
    assert_changes "Patient.count" do
      Patients::Create.new(@user, build(:patient)).call
    end
  end

  test "calls storage#mkdir with patient director" do
    new_patient = build(:patient, case_number: "Test", slug: "test")
    organization = organizations("main")
    @storage.expects(:mkdir)
      .with("#{organization.working_dir}patients/#{new_patient.slug}/",
              "#{organization.working_dir}patients/#{new_patient.slug}/inputs/",
              "#{organization.working_dir}patients/#{new_patient.slug}/pipelines/")

    Patients::Create.new(@user, new_patient).call
  end

  test "does not create db patient when dir cannot be created" do
    stub_storage_failure

    assert_no_changes "Patient.count" do
      Patients::Create.new(@user, build(:patient)).call
    end
  end

  test "does not create dir if db patient is invalid" do
    invalid_patient = build(:patient, case_number: nil)
    @storage.expects(:mkdir).never
    Patients::Create.new(@user, invalid_patient).call

    assert_not invalid_patient.valid?
  end

  test "set error message when dir cannot be created" do
    stub_storage_failure
    new_patient = build(:patient)
    Patients::Create.new(@user, new_patient).call

    assert_includes new_patient.errors[:case_number], I18n.t("activerecord.errors.models.patient.create_dav403")
  end
end
