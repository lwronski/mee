# frozen_string_literal: true

require "test_helper"

class Storage::DestroyFileTest < ActiveSupport::TestCase
  include StorageHelper
  test "removes file from directory" do
    user = create(:user)
    data_file = create(:data_file)

    stub_storage_with_defaults

    assert_changes "DataFile.count", -1 do
      Storage::DestroyFile.new(user, data_file).call
    end
  end
end
