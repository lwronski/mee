# frozen_string_literal: true

require "test_helper"

class Rimrock::StartTest < ActiveSupport::TestCase
  test "starts computation" do
    computation = create(:computation)

    stub_request(:post, "https://rimrock.plgrid.pl/api/jobs")
      .with(body: {
        host: "ares.cyfronet.pl",
        script: computation.script,
        tag: "mee",
        working_directory: computation.working_directory,
      })
      .to_return(status: 201, headers: {},
                 body: '{"job_id":"id", "stdout_path":"out", ' \
        '"stderr_path":"err", "status":"QUEUED"}')


    Rimrock::Start.new(computation).call

    assert_equal "out", computation.stdout_path
    assert_equal "err", computation.stderr_path
    assert_equal "queued", computation.status
  end

  test "fails to start computation" do
    computation = create(:computation)

    stub_request(:post, "https://rimrock.plgrid.pl/api/jobs")
      .to_return(status: 422, headers: {},
                 body: '{"status":"error", "exit_code": -1, ' \
        '"standard_output":"stdout", "error_output":"stderr", ' \
        '"error_message": "error_msg"}')

    Rimrock::Start.new(computation).call

    assert_equal "error", computation.status
    assert_equal(-1, computation.exit_code)
    assert_equal "stdout", computation.standard_output
    assert_equal "stderr", computation.error_output
    assert_equal "error_msg", computation.error_message
  end

  test "cannot start already started computation" do
    computation = create(:computation, status: "running")

    stub_request(:post, "https://rimrock.plgrid.pl/api/jobs")
      .to_raise("Job should not be started twice")

    Rimrock::Start.new(computation).call
  end
end
