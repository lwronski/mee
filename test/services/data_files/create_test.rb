# frozen_string_literal: true

require "test_helper"

class DataFiles::CreateTest < ActiveSupport::TestCase
  def setup
    @organization = organizations("main")
  end

  test "creates patient input data files" do
    patient = create(:patient)
    patient_path = patient.working_dir.delete_prefix(@organization.storage_config.path)

    assert_changes "DataFile.count" do
      DataFiles::Create.
      new(@organization, [File.join(patient_path, "inputs", "file.zip")]).call
    end

    data_file = DataFile.first

    assert_equal data_file_types("image"), data_file.data_type
    assert_equal patient, data_file.fileable
    assert_nil data_file.input_of
    assert_nil data_file.output_of
  end

  test "create input data file without data type when type is not found" do
    patient = create(:patient)
    patient_path = patient.working_dir.delete_prefix(@organization.storage_config.path)
    unknown_file_path = File.join(patient_path, "inputs", "file.unknown")

    assert_changes "DataFile.count" do
      DataFiles::Create.new(@organization, [unknown_file_path]).call
    end

    data_file = DataFile.first
    assert_nil data_file.data_type
  end

  test "does not create duplicates" do
    patient = create(:patient)
    DataFile.create(name: "file.zip", data_type: data_file_types("image"), fileable: patient)

    assert_no_changes "DataFile.count" do
      DataFiles::Create.
        new(@organization,
            [File.join("/", patient.working_dir, "inputs", "file.zip")]).call
    end
  end

  test "creates pipeline input file" do
    patient = create(:patient)
    pipeline = create(:pipeline, runnable: patient)
    pipeline_inputs_dir = pipeline.inputs_dir.delete_prefix(@organization.storage_config.path)

    assert_changes "DataFile.count" do
      DataFiles::Create.
        new(@organization, [File.join(pipeline_inputs_dir, "file.zip")]).call
    end

    data_file = DataFile.first

    assert_equal data_file_types("image"), data_file.data_type
    assert_equal patient, data_file.fileable
    assert_equal pipeline, data_file.input_of
    assert_nil data_file.output_of
  end

  test "creates pipeline output file" do
    patient = create(:patient)
    pipeline = create(:pipeline, runnable: patient)
    pipeline_outputs_dir = pipeline.outputs_dir.delete_prefix(@organization.storage_config.path)


    assert_changes "DataFile.count" do
      DataFiles::Create.
        new(@organization, [File.join(pipeline_outputs_dir, "file.zip")]).call
    end

    data_file = DataFile.first

    assert_equal data_file_types("image"), data_file.data_type
    assert_equal patient, data_file.fileable
    assert_equal pipeline, data_file.output_of
    assert_nil data_file.input_of
  end

  test "uses organization data types" do
    other_organization = organizations("other")
    other_org_patient = create(:patient, organization: other_organization)
    other_org_pipeline = create(:pipeline, runnable: other_org_patient)
    other_org_dft = data_file_types("image").dup.tap do |dft|
      dft.organization = other_organization
      dft.save!
    end
    pipeline_outputs_dir = other_org_pipeline.outputs_dir.delete_prefix(other_organization.storage_config.path)

    assert_changes "DataFile.count" do
      DataFiles::Create.
        new(other_organization, [File.join(pipeline_outputs_dir, "file.zip")]).call
    end


    data_file = DataFile.first

    assert_equal other_org_dft, data_file.data_type
    assert_equal other_org_patient, data_file.fileable
    assert_equal other_org_pipeline, data_file.output_of
    assert_nil data_file.input_of
  end

  test "ignores files from outsite patients folder" do
    assert_no_changes "DataFile.count" do
      DataFiles::Create.new(@organization, ["/a/file.zip", "/a/b/file.zip"]).call
    end
  end

  test "ignores files from other patient directory" do
    patient = create(:patient)

    assert_no_changes "DataFile.count" do
      DataFiles::Create.new(@organization, [File.join("/", patient.working_dir, "a", "file.zip")]).call
    end
  end

  test "returns empty when no data file created" do
    data_files = DataFiles::Create.new(@organization, ["/a/file.zip", "/a/b/file.zip"]).call

    assert_empty data_files
  end
end
