# frozen_string_literal: true

require "test_helper"

class DataFiles::SynchronizeTest < ActiveSupport::TestCase
  def setup
    @fluid_virtual_model = create(:data_file_type, data_type: "fluid_virtual_model", pattern: /^fluidFlow\.cas$/)
    @patient = create(:patient)
  end

  test "synchronizes patient inputs" do
    remote_files = [
      file(@patient, "inputs/fluidFlow.cas"),
      file(@patient, "inputs/README.md"),
      file(@patient, "ignored.txt")
    ]

    DataFiles::Synchronize.new(@patient, remote_files).call

    assert_equal 2, @patient.inputs.size
    assert_equal ["fluidFlow.cas", "README.md"], @patient.inputs.map(&:name)
    assert_equal @fluid_virtual_model, data_file(@patient.inputs, "fluidFlow.cas").data_type
    assert_nil data_file(@patient.inputs, "README.md").data_type
  end

  test "synchronizes pipeline inputs" do
    pipeline = create(:pipeline, runnable: @patient)

    remote_files = [
      file(@patient, "pipelines/#{pipeline.iid}/inputs/fluidFlow.cas"),
      file(@patient, "pipelines/#{pipeline.iid}/inputs/README.md"),
      file(@patient, "pipelines/#{pipeline.iid}/ignore.txt")
    ]

    DataFiles::Synchronize.new(@patient, remote_files).call

    assert_equal 2, pipeline.inputs.size
    assert_equal ["fluidFlow.cas", "README.md"], pipeline.inputs.map(&:name)
    assert_equal @fluid_virtual_model, data_file(pipeline.inputs, "fluidFlow.cas").data_type
    assert_nil data_file(pipeline.inputs, "README.md").data_type
  end

  test "synchronizes pipeline outputs" do
    pipeline = create(:pipeline, runnable: @patient)

    remote_files = [
      file(@patient, "pipelines/#{pipeline.iid}/outputs/fluidFlow.cas"),
      file(@patient, "pipelines/#{pipeline.iid}/outputs/README.md"),
      file(@patient, "pipelines/#{pipeline.iid}/ignore.txt")
    ]

    DataFiles::Synchronize.new(@patient, remote_files).call

    assert_equal 2, pipeline.outputs.size
    assert_equal ["fluidFlow.cas", "README.md"], pipeline.outputs.map(&:name)
    assert_equal @fluid_virtual_model, data_file(pipeline.outputs, "fluidFlow.cas").data_type
    assert_nil data_file(pipeline.outputs, "README.md").data_type
  end

  test "removes not found files" do
    pipeline = create(:pipeline, runnable: @patient)

    create(:data_file, fileable: @patient)
    create(:data_file, fileable: @patient, input_of: pipeline)
    create(:data_file, fileable: @patient, output_of: pipeline)

    DataFiles::Synchronize.new(@patient, []).call

    assert_equal 0, @patient.inputs.size
    assert_equal 0, pipeline.inputs.size
    assert_equal 0, pipeline.outputs.size
  end

  private
    def file(patient, relative_path)
      {
        path: File.join(patient.working_dir, relative_path),
        size: 10,
        modification_date: Time.now
      }
    end

    def data_file(rel, name)
      rel.find { |df| df.name == name }
    end
end
