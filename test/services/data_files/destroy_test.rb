# frozen_string_literal: true

require "test_helper"

class DataFiles::DestroyTest < ActiveSupport::TestCase
  test "destroys deleted file" do
    df = create(:data_file)

    assert_changes "DataFile.count", -1 do
      DataFiles::Destroy.new([File.join("/", df.path)]).call
    end

    assert_not DataFile.exists?(df.id)
  end

  test "destroys all files in directory and subdirectories"  do
    patient = create(:patient)
    pipeline = create(:pipeline, runnable: patient)
    _input = create(:data_file, fileable: patient)
    _pipeline_input = create(:data_file, fileable: patient, input_of: pipeline)
    _pipeline_output = create(:data_file, fileable: patient, output_of: pipeline)

    assert_changes "DataFile.count", -3 do
      DataFiles::Destroy.new([File.join("/", patient.working_dir)]).call
    end
  end
end
