# frozen_string_literal: true

require "application_system_test_case"

class StepsTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "Organization member can see organization steps" do
    other_org_step = create(:step, organization: organizations("other"))

    visit steps_path

    assert_text rimrock_steps("first").name
    assert_text rimrock_steps("second").name
    assert_no_text other_org_step.name
  end

  # takes > 2 seconds
  test "Organization member can create new step" do
    visit steps_path

    click_on "New pipeline step"

    fill_in "Name", with: "My new step"
    fill_in "Repository", with: "cyfronet/test"
    fill_in "File", with: "run.sh.liquid"
    select "cpu", from: "Grant type"
    select "Ares", from: "Site"

    assert_difference "RimrockStep.count" do
      click_on "Create new pipeline step"
      assert_text "My new step"
    end
  end

  # takes > 2 seconds
  test "Owner can edit step" do
    step = rimrock_steps("first")

    visit edit_step_path(step)

    fill_in "Name", with: "updated name", match: :first
    version_parent_scope = find_field("Key", with: "tag-or-branch", disabled: :all).find(:xpath, "../../..")
    version_parent_scope.fill_in "Name", with: "Model version updated"

    grant_parent_scope = find_field("Key", with: "grant", disabled: :all).find(:xpath, "../../..")
    grant_parent_scope.fill_in "Name", with: "Grant updated"

    select "gpu", from: "Grant type"
    select "Athena", from: "Site"
    click_on "Save changes"

    assert_text "updated name"
    step.reload

    assert_equal "updated name", step.name
  end

  test "Can switch to default organization git config" do
    # FIXME: check how this can be moved to global mock
    GitRepository::GitlabClient.any_instance.stubs(:key_valid?).returns(false)

    step = rimrock_steps("second")

    visit edit_step_path(step)

    uncheck("Override organization git configuration")
    click_on "Save changes"

    assert_text "Registered pipeline steps"
    assert_nil step.reload.git_config
  end

  test "Should show step configuration errors" do
    step = rimrock_steps("first")
    step.persistent_errors.create(key: "repository", message: "test invalid")

    visit edit_step_path(step)

    assert_text "test invalid"
  end

  test "Should delete step when no depending flows" do
    step = create(:step)

    visit steps_path

    assert_difference "Step.count", -1 do
      accept_confirm do
        within :xpath, "//form[@action='#{step_path step}']" do
          click_on "Remove this pipeline step"
        end
      end
      assert_text "Registered pipeline steps"
    end
  end
end
