# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
module OrganizationPipelines
  class AddPipelineTest < ApplicationSystemTestCase
    include StorageHelper

    def setup
      @organization = organizations("main")
      in_organization! @organization
      login_as users("user")
      create(:flow)
      stub_storage_with_defaults
      @runnable = mock
      Pipelines::StartRunnable.stubs(:new).returns(@runnable)
    end

    # takes > 2 seconds
    test "adds manual pipeline" do
      @runnable.expects(:call).never
      visit new_pipeline_path

      fill_in "Name", with: "my new manual pipeline"
      first("#pipeline_mode").select "manual"
      assert_difference "Pipeline.count" do
        click_on "Set up new pipeline"
        assert_text "Waiting for execution"
      end

      pipeline = Pipeline.last
      assert_current_path computation_path(pipeline.computations.first)
      assert_equal @organization, pipeline.runnable
      assert_equal "my new manual pipeline", pipeline.name
      assert pipeline.manual?
    end

    # takes > 2 seconds
    test "adds automatic pipeline" do
      @runnable.expects(:call)
      visit new_pipeline_path

      fill_in "Name", with: "my new automatic pipeline"
      assert_difference "Pipeline.count" do
        click_on "Set up new pipeline"
        assert_text "Waiting for execution"
      end

      pipeline = Pipeline.last
      assert_current_path computation_path(pipeline.computations.first)
      assert_equal @organization, pipeline.runnable
      assert_equal "my new automatic pipeline", pipeline.name
      assert pipeline.automatic?
    end

    test "is unable to create pipeline without name" do
      visit new_pipeline_path
      click_on "Set up new pipeline"
      assert_text "Name can't be blank"
    end
  end
end
