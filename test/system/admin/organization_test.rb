# frozen_string_literal: true

require "application_system_test_case"

class Admin::OrganizationTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
  end

  test "can not be managed by a regular user" do
    login_as users("user")
    visit admin_organization_path

    assert_text I18n.t("pundit.default")
  end

  test "admin can manage organization" do
    login_as users("admin")

    visit admin_organization_path
    fill_in "Name", with: "Test Org"
    click_button I18n.t("organizations.form.submit_button")

    assert_text "Organization details updated"
  end

  test "shows persistent errors" do
    error_message = "invalid token value"
    organizations("main").persistent_errors
      .create(child: "git_config", key: "private_token", message: error_message)

    login_as users("admin")
    visit admin_organization_path

    assert_text error_message
  end
end
