# frozen_string_literal: true

require "application_system_test_case"
require "minitest/autorun"

class Campaigns::RunTest < ApplicationSystemTestCase
  include RimrockHelper
  include ActiveJob::TestHelper

  setup do
    @cohort = create(:cohort, patients: create_list(:patient, 3))
    @campaign = create(:campaign, cohort: @cohort, desired_pipelines: 3, status: 1, pipelines:
      build_list(:pipeline, 3))

    stub_rimrock_process

    in_organization! organizations("main")
    login_as users("admin")
  end

  test "can see computation statuses" do
    @campaign.pipelines.each { |pipeline| create(:computation, status: "queued", pipeline:) }
    visit campaign_path(@campaign)
    assert_css ".fa-spin", count: 3
  end

  test "updates status as running" do
    visit campaign_path(@campaign)
    perform_enqueued_jobs do
      click_on I18n.t("campaigns.view.run.created")
    end
    assert_text I18n.t("campaigns.view.run.running")
  end

  test "updates status as finished" do
    computations = @campaign.pipelines.map { |pipeline| create(:computation, status: "queued", pipeline:) }
    stub_plgrid_api_jobs_get(computations)
    stub_computation_logs_request(*computations)
    @campaign.running!

    visit campaign_path(@campaign)
    perform_enqueued_jobs do
      TriggerUpdateJob.perform_now
    end
    assert_text I18n.t("campaigns.view.run.finished")
  end
end
