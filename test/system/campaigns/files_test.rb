# frozen_string_literal: true

require "application_system_test_case"

class Campaigns::FilesTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
    login_as users("admin")

    campaign = create :campaign
    pipeline = create(:pipeline, campaign:)
    @data_file = create :data_file, output_of: pipeline

    visit campaign_path(campaign)
  end

  test "Campaign files are collapsed by default" do
    assert_no_text DataFile.first.name
  end

  test "Can see campaign files after expanding files tab" do
    click_on I18n.t("campaigns.view.output_files.title")
    assert_text I18n.t("campaigns.view.output_files.patient.title", case_number:
      Pipeline.first.runnable.case_number)
    assert_text @data_file.name
  end
end
