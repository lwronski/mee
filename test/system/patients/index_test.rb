# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
class Patients::IndexTest < ApplicationSystemTestCase
  include StorageHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
  end

  test "shows notification when no patients are available" do
    visit patients_path

    assert_text I18n.t("patients.empty.title")
    assert_text I18n.t("patients.empty.new")
  end

  test "allows to add a new patient" do
    create(:patient)
    visit patients_path

    assert_text I18n.t("patients.index.add")
    click_link I18n.t("patients.index.add")

    assert_current_path new_patient_path
  end

  test "lets navigate to a given patient case" do
    patient = create(:patient)
    visit patients_path

    assert_text patient.case_number
    click_link patient.case_number

    assert_current_path patient_path(patient)
  end

  test "deletes patient" do
    patient = create(:patient)
    visit patients_path
    stub_storage_with_defaults

    assert_button I18n.t("patients.summary_component.destroy")
    assert_difference "Patient.count", -1 do
      accept_confirm do
        click_button I18n.t("patients.summary_component.destroy")
      end
      assert_text I18n.t("patients.destroy.success", case_number: patient.case_number)
    end
  end
end
