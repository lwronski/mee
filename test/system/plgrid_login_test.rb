# frozen_string_literal: true

require "minitest/autorun"
require "application_system_test_case"

class PlgridLoginTest < ApplicationSystemTestCase
  test "new user is granted access when in organizations team" do
    in_organization! organizations("main")
    login_as(users("user"))

    assert_content "#{users("user").first_name} #{users("user").last_name}"
  end

  test "new user is not granted access when not in organizations team" do
    in_organization! organizations("main")
    login_as(users("user"), teams: "")

    assert_content "no access"
  end

  test "denies access to user when no longer in organizations team" do
    plgrid_user = users("user")
    in_organization! organizations("main")
    login_as(plgrid_user)

    assert_equal plgrid_user.memberships.first.state, "approved"
    find("a[class='nav-link dropdown-toggle user-profile']").click
    click_on("Logout")

    login_as(plgrid_user, teams: "")
    assert_equal plgrid_user.memberships.first.state, "blocked"
  end

  test "grants access to user that was added to team in the organization" do
    plgrid_user = users("user")
    in_organization! organizations("main")
    login_as(plgrid_user, teams: "")
    assert_equal plgrid_user.memberships.first.state, "blocked"

    click_on("Logout")

    login_as(plgrid_user)
    assert_equal plgrid_user.memberships.first.state, "approved"
  end

  test "after plgrid login proxy expired notification date is reseted" do
    john = create(:user, proxy_expired_notification_time: Time.zone.now)
    in_organization! organizations("main")

    login_as(john)
    john.reload

    assert_nil john.proxy_expired_notification_time
  end
end
