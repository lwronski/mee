# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
class Artifacts::DestroyTest < ApplicationSystemTestCase
  include StorageHelper
  def setup
    in_organization! organizations("main")
    login_as users("user")
    stub_storage_with_defaults
    create(:artifact)
    visit artifacts_path
  end

  test "it destroys artifact" do
    accept_confirm do
      click_button I18n.t("artifacts.artifact.destroy")
    end
    assert_no_text Artifact.first.name
    assert_text "Artifact successfully destroyed"
  end

  test "with failing storage shows failure error" do
    stub_storage_failure
    accept_confirm do
      click_button I18n.t("artifacts.artifact.destroy")
    end
    assert_text "Unable to delete artifact"
  end
end
