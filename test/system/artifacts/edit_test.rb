# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
class Artifacts::EditTest < ApplicationSystemTestCase
  include StorageHelper
  def setup
    in_organization! organizations("main")
    login_as users("user")
    stub_storage_with_defaults
    create(:artifact)
  end

  test "it updates artifact and shows notification" do
    visit artifacts_path
    find_link(href: "/artifacts/#{Artifact.first.id}/edit").click
    find("#artifact_name").set("Updated name")
    click_on("Update Artifact")

    assert_text "Artifact successfully updated"
    assert_text "Updated name"
  end

  test "doesn't allow blank name" do
    visit artifacts_path
    find_link(href: "/artifacts/#{Artifact.first.id}/edit").click
    find("#artifact_name").set("")
    click_on("Update Artifact")

    assert_text "Name can't be blank"
  end

  test "doesn't allow taken name" do
    create(:artifact, name: "taken")
    visit artifacts_path
    find_link(href: "/artifacts/#{Artifact.first.id}/edit").click
    find("#artifact_name").set("taken.txt")
    click_on("Update Artifact")

    assert_text "Name has already been taken"
  end

  test "with failing storage shows failure error" do
    stub_storage_failure
    visit artifacts_path
    find_link(href: "/artifacts/#{Artifact.first.id}/edit").click
    find("#artifact_name").set("error")
    click_on("Update Artifact")

    assert_text "Unable to rename"
  end
end
