# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
class Artifacts::IndexTest < ApplicationSystemTestCase
  include StorageHelper
  def setup
    in_organization! organizations("main")
    login_as users("user")
    stub_storage_with_defaults
    create(:artifact)
    visit artifacts_path
  end

  test "it shows general artifact information" do
    assert_text Artifact.first.name
    assert_link I18n.t("artifacts.artifact.edit")
    assert_button I18n.t("artifacts.artifact.destroy")
  end
end
