# frozen_string_literal: true

require "application_system_test_case"

class Pipelines::NotesTest < ApplicationSystemTestCase
  def setup
    @organization = organizations("main")
    in_organization! @organization
    @pipeline = create(:pipeline, runnable: @organization,
                user: users("admin"), notes: "some notes")
  end

  test "owner sees and updates notes" do
    login_as users("admin")
    visit pipeline_path(@pipeline)

    assert_text "Notes"
    assert_text @pipeline.notes.to_plain_text

    find(".trix-content").set("Some content")
    click_on "Update notes"

    assert_text "Some content"
    assert_equal "Some content", @pipeline.reload.notes.to_plain_text
  end

  test "non-owner doesn't see notes" do
    login_as users("user")

    visit pipeline_path(@pipeline)
    assert_no_text @pipeline.notes.to_plain_text
  end
end
