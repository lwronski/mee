# frozen_string_literal: true

require "application_system_test_case"

class Pipelines::DataFilesTest < ApplicationSystemTestCase
  include GitlabHelper
  include StorageHelper
  include PipelineBrowsingHelper
  include ActionView::RecordIdentifier

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @patient = create(:patient, case_number: "1234")
    @pipeline = create(:pipeline, runnable: @patient, name: "p1")
    @dft = create(:data_file_type,
      name: "TestDataFileType",
      viewer: :text,
      data_type: "test_data_file_type", pattern: /^test_data_file.*\.txt$/)
    @step = create(:step, required_file_types: [@dft])
    @computation = create(:computation, pipeline: @pipeline, step: @step)
    mock_rimrock_computation_ready_to_run
  end

  test "allows to choose Data File to use when there is more than 1" do
    stub_storage_with_defaults
    stub_repo_versions(@step, { branches: ["master"], tags: ["t1"] })
    stub_get_repo_file(@step, "master", Base64.encode64("script"))
    grant = create(:grant)
    Rimrock::StartJob.expects(:perform_later)

    df1 = create(:data_file, name: "test_data_file1.txt", data_type: @dft, fileable: @patient, input_of: @pipeline)
    df2 = create(:data_file, name: "test_data_file2.txt", data_type: @dft, fileable: @patient, input_of: @pipeline)

    visit computation_path(@computation)

    assert_text @dft.name
    assert_text df1.name
    assert_text df2.name

    select("master")
    select(grant.name)
    select(df2.name)
    click_button computation_run_text(@computation)
    assert_text "is being submitted"

    @computation.reload
    assert_equal df2, @computation.data_file(@dft)
  end

  test "does not allow to choose Data File when there is only 1" do
    df1 = create(:data_file, name: "test_data_file1.txt", data_type: @dft, fileable: @patient, input_of: @pipeline)
    visit computation_path(@computation)

    within(:xpath, ".//form[@action='#{computation_path(@computation)}']") do
      assert_no_text @dft.name
      assert_no_text df1.name
    end
  end
end
