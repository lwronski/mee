# frozen_string_literal: true

require "application_system_test_case"

class Pipelines::ComputationsTest < ApplicationSystemTestCase
  include GitlabHelper
  include StorageHelper
  include PipelineBrowsingHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @step = create(:step)
    @computation = create(:computation, step: @step)
    stub_storage_with_defaults
  end

  test "user can set computation tag_or_branch for automatic and start runnable computations" do
    grant = grants("cpu")
    stub_repo_versions(@step, { branches: ["master"], tags: ["t1"] })
    start_runnable = mock
    Pipelines::StartRunnable.expects(:new).returns(start_runnable)
    start_runnable.expects(:call)
    Computation.any_instance.expects(:run).never

    visit computation_path(@computation)
    select("t1")
    select(grant.name)
    click_button computation_run_text(@computation)

    assert_text "t1"
    assert_equal "t1", @computation.reload.tag_or_branch
  end

  test "show started computation source link for started step" do
    @computation.update(revision: "my-revision", started_at: Time.zone.now)

    visit computation_path(@computation)

    assert_link "my-revision", href: "https://gitlab.com/#{@step.repository}/tree/my-revision"
  end

  test "computation source link is not shown when no revision" do
    visit computation_path(@computation)
    assert_no_link href: "https://gitlab.com/#{@step.repository}/tree"
  end
end
