# frozen_string_literal: true

require "application_system_test_case"

class CustomDomainTest < ApplicationSystemTestCase
  test "Custom organization domain is chosen instead of subdomain" do
    organization = organizations("main")
    user = users("admin")
    Current.set(user:) do
      organization.update(domain: "valve.lvh.me")
    end

    Capybara.app_host = "http://valve.lvh.me"
    login_as user

    visit root_path

    assert_content organization.name
    assert_content user.name
  end
end
