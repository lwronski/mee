# frozen_string_literal: true

class Cohorts::AddTest < ApplicationSystemTestCase
  include CohortHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
  end

  test "can add cohort" do
    navigate_to_cohorts
    add_cohort "CohortName"
    navigate_to_cohorts
    assert_text "CohortName"
  end

  test "can't add a cohort with a blank name" do
    navigate_to_cohorts
    add_cohort ""
    assert_text "Name can't be blank"
  end

  test "can't add a cohort with a wrong name" do
    navigate_to_cohorts
    add_cohort "@CohortName"
    assert_text I18n.t("cohorts.name_error")
  end
end
