# frozen_string_literal: true

require "application_system_test_case"

class Cohorts::AddPatientsTest < ApplicationSystemTestCase
  include CohortHelper

  def setup
    in_organization! organizations("main")
    login_as users("user")
  end

  # takes > 2 seconds
  test "can add patients to cohort" do
    cohort = create(:cohort, patients: [])
    create(:patient)
    visit cohort_path(cohort)
    page.all(:css, ".btn.btn-success").each { |el| el.click }

    assert_no_text I18n.t("cohorts.patients.add")
    assert_text I18n.t("cohorts.patients.remove")
  end

  test "can view non cohort patients" do
    cohort = create(:cohort, patients: [])
    create(:patient)
    visit cohort_path(cohort)
    assert_no_match Patient.first.case_number, patients_text(page, "members", cohort)
    assert_match Patient.first.case_number, patients_text(page, "candidates", cohort)
  end

  test "can remove patients from cohort" do
    cohort = create(:cohort, patients: create_list(:patient, 1))
    visit cohort_path(cohort)
    click_on I18n.t("cohorts.patients.remove")
    assert_text I18n.t("cohorts.patients.add")
    assert_no_text I18n.t("cohorts.patients.remove")
  end

  test "can see patients added to cohort" do
    cohort = create(:cohort, patients: create_list(:patient, 1))
    non_cohort_patient = create(:patient)
    visit cohort_path(cohort)
    assert_match cohort.patients.first.case_number, patients_text(page, "members", cohort)
    assert_match non_cohort_patient.case_number, patients_text(page, "candidates", cohort)
  end

  test "can see non cohort patients" do
    cohort = create(:cohort, patients: [])
    create(:patient)
    visit cohort_path(cohort)
    assert_no_match Patient.first.case_number, patients_text(page, "members", cohort)
    assert_match Patient.first.case_number, patients_text(page, "candidates", cohort)
  end
end
