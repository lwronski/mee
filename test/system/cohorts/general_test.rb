# frozen_string_literal: true

require "application_system_test_case"

class Cohorts::GeneralTest < ApplicationSystemTestCase
  def setup
    in_organization! organizations("main")
    login_as users("user")
    create :cohort
  end

  test "doesn't see cohorts from other organization" do
    owned_cohort = Cohort.where(organization: organizations("main"))
    not_owned_cohort = create(:cohort, organization: organizations("other"))
    visit cohort_path(owned_cohort)
    assert_no_text I18n.t("cohorts.empty.title")
    assert_text owned_cohort.name
    assert_no_text not_owned_cohort.name
  end

  test "can delete it" do
    visit cohorts_path
    click_on "Delete cohort"
    page.driver.browser.switch_to.alert.accept
    assert_text I18n.t("cohorts.empty.title")
  end

  test "can visit the cohort page" do
    visit cohort_path(Cohort.first)
    assert_text Cohort.first.name
    assert_text I18n.t("cohorts.show.campaigns.link_to_campaigns")
    assert_text I18n.t("cohorts.show.link_to_patients")
  end

  test "Organization member views that no campaigns were created yet" do
    visit cohort_path(Cohort.first)
    find("span", text: I18n.t("cohorts.show.campaigns.link_to_campaigns")).click
    assert_text I18n.t("cohorts.show.campaigns.empty.title")
    assert_text I18n.t("cohorts.show.campaigns.empty.description")
  end

  test "doesn't see campaigns from other organization" do
    cohort = create(:cohort, organization: organizations("other"))
    other_campaign = create(:campaign, cohort:)

    visit cohort_path(Cohort.first)
    find('span[aria-controls="cohortCampaigns"]').click
    assert_no_text other_campaign.name
    assert_text I18n.t("cohorts.show.campaigns.empty.title")
    assert_text I18n.t("cohorts.show.campaigns.empty.description")
  end
end
