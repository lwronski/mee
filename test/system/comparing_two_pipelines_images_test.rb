# left to rewrite later, because uses net/dav which was temporarily removed

=begin
# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Comparing two pipelines images", files: true do
  before do
    create(:data_file_type,
           pattern: /^.*\.\b(png|bmp|jpg)\b$/,
           data_type: "graphics", viewer: :graphics)
  end

  it "shows image diffs for comparable image files", js: true do
    user = create(:user)
    create(:approved_membership, user:, organization:)
    sign_in_as(user)

    patient = create(:patient, case_number: "7900")
    create_list(:pipeline, 2, runnable: patient)
    patient.execute_data_sync(user)

    visit comparisons_path(pipeline_ids: [1, 2])

    expect(page).to_not have_content "portal6.png"
    expect(page).to have_content "heart.bmp"
  end
end
=end
