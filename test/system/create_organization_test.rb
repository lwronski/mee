# frozen_string_literal: true

require "application_system_test_case"

class CreateOrganizationTest < ApplicationSystemTestCase
  setup do
    in_root!
  end

  test "user creates organization" do
    create_organization_for(users("user"))
  end

  test "admin creates organization" do
    create_organization_for(users("admin"))
  end

  def create_organization_for(actor)
    login_as actor
    visit new_organization_path
    fill_registration_form
    after_create_count = Organization.count + 1
    click_button I18n.t("organizations.form.submit_button")
    assert_content("Test Org")
    assert_equal Organization.count, after_create_count
    assert_includes(Organization.last.supervisors, actor)
  end

  test "user is redirected to sign in page if not logged in" do
    visit new_organization_path

    assert_current_path(root_path)
  end

  private
    def fill_registration_form
      fill_in "Name", with: "Test Org"
      fill_in "Host", with: "gitlab.com"
      fill_in "Private token", with: "veryprivatetoken"
      attach_file "Download key file", file_fixture("id_rsa")
      fill_in "Storage host", with: "prometheus.cyfronet.pl"
      fill_in "PLGData host key", with: "prometheus"
      fill_in "Storage path", with: "/wrong/path/on/on/plgrid"
      fill_in "Description", with: "Some test description"
      fill_in "Plgrid team", with: "plggteam"
    end
end
