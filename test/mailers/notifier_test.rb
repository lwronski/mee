# frozen_string_literal: true

require "test_helper"

class NotifierTest < ActionMailer::TestCase
  test "new user: don't send email when no supervisors" do
    assert_no_difference "ActionMailer::Base.deliveries.count" do
      Notifier.user_registered(user: users("admin"),
                               organization: organizations("other")).deliver_now
    end
  end

  test "new user: send email to all supervisors" do
    new_user = users("stranger")

    email =
      assert_difference "ActionMailer::Base.deliveries.count" do
        Notifier.user_registered(user: new_user,
                                organization: organizations("main")).deliver_now
      end

    assert_equal [users("admin").email, users("supervisor").email].sort, email.to.sort
    assert_match new_user.name, email.body.encoded
  end

  test "account approved: notify user" do
    email = Notifier.account_approved(user: users("user"),
                                      organization: organizations("main")).deliver_now

    assert_match "has been approved", email.body.encoded
    assert_equal [users("user").email], email.to
  end

  test "proxy expired: notify user" do
    email = Notifier.proxy_expired(users("user")).deliver_now

    assert_match "proxy certificate has expired", email.body.encoded
    assert_equal [users("user").email], email.to
  end
end
