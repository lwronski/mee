# frozen_string_literal: true

require "test_helper"

class Pipelines::AffectedTest < ActiveSupport::TestCase
  setup do
    @patient = create(:patient)
  end

  test "returns all pipelines for patient input" do
    all_pipelines = create_list(:pipeline, 2, runnable: @patient)
    data_file = create(:data_file, fileable: @patient)

    assert_equal all_pipelines.sort, Pipelines::Affected.new([data_file]).call.sort
  end

  test "returns input pipeline" do
    pipeline, = create_list(:pipeline, 2, runnable: @patient)
    data_file = create(:data_file, fileable: @patient, input_of: pipeline)

    assert_equal [pipeline], Pipelines::Affected.new([data_file]).call
  end

  test "returns output pipeline" do
    pipeline, = create_list(:pipeline, 2, runnable: @patient)
    data_file = create(:data_file, fileable: @patient, output_of: pipeline)

    assert_equal [pipeline], Pipelines::Affected.new([data_file]).call
  end

  test "does not duplicate records" do
    p1, p2 = create_list(:pipeline, 2, runnable: @patient)

    patient_input = create(:data_file, fileable: @patient)
    p1_input = create(:data_file, fileable: @patient, input_of: p1)
    p2_output = create(:data_file, fileable: @patient, output_of: p2)

    assert_equal 2, Pipelines::Affected.new([patient_input, p1_input, p2_output]).call.size
  end
end
